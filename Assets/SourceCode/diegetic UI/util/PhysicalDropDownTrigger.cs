using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

[RequireComponent(typeof(TMP_Dropdown))]
public class PhysicalDropDownTrigger : MonoBehaviour
{
    private TMP_Dropdown dropDown;

    private void Start()
    {
        dropDown = GetComponent<TMP_Dropdown>();
    }

    void OnTriggerEnter()
    {
        if(dropDown != null)
        {
            dropDown.OnPointerClick(null);
        }
    }
}
