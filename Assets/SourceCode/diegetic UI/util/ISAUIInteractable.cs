using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;

//[RequireComponent(typeof(Collider))]
public class ISAUIInteractable : MonoBehaviour
{
    [Serializable]
    public class ISAUIEvent : UnityEvent
    {
        internal void AddListener(object v)
        {
            throw new NotImplementedException();
        }
    }



    [SerializeField]
    private ISAUIEvent m_OnHoverStart = new ISAUIEvent();

    public ISAUIEvent onHoverStart
    {
        get { return m_OnHoverStart; }
        set { m_OnHoverStart = value; }
    }



    [SerializeField]
    private ISAUIEvent m_OnHoverEnd = new ISAUIEvent();

    public ISAUIEvent onHoverEnd
    {
        get { return m_OnHoverEnd; }
        set { m_OnHoverEnd = value; }
    }



    [SerializeField]
    private ISAUIEvent m_OnTriggerPressed = new ISAUIEvent();

    public ISAUIEvent onTriggerPressed
    {
        get { return m_OnTriggerPressed; }
        set { m_OnTriggerPressed = value; }
    }



    [SerializeField]
    private ISAUIEvent m_OnTriggerReleased = new ISAUIEvent();

    public ISAUIEvent onTriggerReleased
    {
        get { return m_OnTriggerReleased; }
        set { m_OnTriggerReleased = value; }
    }

}
