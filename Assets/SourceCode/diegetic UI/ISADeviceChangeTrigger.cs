using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using ISA.util;

namespace ISA.devices
{

    [RequireComponent(typeof(Hand))]
    public class ISADeviceChangeTrigger : MonoBehaviour
    {

        public SteamVR_Input_Sources TargetInputSource;
        public SteamVR_Action_Boolean ClickAction;

        public GameObject DeviceSelectionCircle;
        public DeviceSelectionCircleItem[] items;

        private Hand hand;

        private bool circleVisible;

        void Start()
        {
            hand = GetComponent<Hand>();

            ChangeSelectionCircleVisibility(false);
        }

        void Update()
        {
            if (ClickAction.GetStateDown(TargetInputSource))
            {
                ChangeSelectionCircleVisibility(true);
            }
            if (ClickAction.GetStateUp(TargetInputSource))
            {
                DeviceSelectionCircleItem selectedTool = GetSelectedTool();
                foreach (DeviceSelectionCircleItem item in items)
                {
                    if (!item.Equals(selectedTool))
                        item.UnEquip(hand);
                }
                if (selectedTool != null)
                    selectedTool.Equip(hand);

                ChangeSelectionCircleVisibility(false);
            }

            if(circleVisible)
            {
                foreach (DeviceSelectionCircleItem item in items)
                {
                    item.UnHighlight();
                }
                DeviceSelectionCircleItem selectedTool = GetSelectedTool();
                if (selectedTool != null)
                {
                    selectedTool.Highlight();
                }
            }
        }

        private DeviceSelectionCircleItem GetSelectedTool()
        {
            float bestDistance = float.MaxValue;
            DeviceSelectionCircleItem selectedTool = null;

            foreach (DeviceSelectionCircleItem item in items)
            {
                float distance = Vector3.Distance(item.gameObject.transform.position, hand.hoverSphereTransform.position);
                if (distance > 0.2)
                    continue;

                if (distance < bestDistance)
                {
                    bestDistance = distance;
                    selectedTool = item;
                }
            }

            return selectedTool;
        }

        private void ChangeSelectionCircleVisibility(bool visible)
        {
            //VisualDebugger.DrawPoint(hand.objectAttachmentPoint.position, Vector3.one * 0.5f, new Color(1, 1, 0));
            circleVisible = visible;
            DeviceSelectionCircle.SetActive(visible);
            DeviceSelectionCircle.transform.position = hand.hoverSphereTransform.position;
        }
    }

}
