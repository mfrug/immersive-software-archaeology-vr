using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDisposalBinManager : MonoBehaviour
{

    public static UIDisposalBinManager INSTANCE;

    public Transform PlayerBodyTransform;
    public Transform HandTransform;

    public Transform ArrowTransform;



    public Vector3 center
    {
        get
        {
            return transform.position + Vector3.down * radius * 0.5f;
        }
    }
    public float radius
    {
        get
        {
            return transform.lossyScale.x;
        }
    }



    void Start()
    {
        INSTANCE = this;
        gameObject.SetActive(false);
    }

    void Update()
    {
        if (!gameObject.activeInHierarchy)
            return;

        //transform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);

        Vector3 bodyToHandXZ = HandTransform.position - PlayerBodyTransform.position;
        bodyToHandXZ.y = 0;
        bodyToHandXZ.Normalize();
        Vector3 bodyToHandXZNormal = Vector3.Cross(bodyToHandXZ, Vector3.up).normalized;

        //Vector3 targetPosition = PlayerBodyTransform.position + 0.7f * bodyToHandXZ - 0.2f * bodyToHandXZNormal * bodyToHandXZ.magnitude;
        //transform.position = new Vector3(targetPosition.x, HandTransform.position.y - 0.3f, targetPosition.z);

        transform.position = HandTransform.position + Vector3.down * 0.5f - bodyToHandXZ * 0.2f - bodyToHandXZNormal * 0.3f;
        
        ArrowTransform.localPosition = new Vector3(0, Mathf.Sin(Time.time), 0);
    }


    public void Show()
    {
        gameObject.SetActive(true);
        Update();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
