﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ISA.Modelling;
using TMPro;
using UnityEngine.UI;
using System.Threading;
using System.Threading.Tasks;
using ISA.SolarOverview;
using System;
using Valve.VR;

namespace ISA.UI
{

    public class ModelSelectionUICanvasHandler : MonoBehaviour
    {
        public List<ActiveToggleable> ShowOnLoadingError;

        public GameObject ModelSelectionDropDownObject;
        private TMP_Dropdown ModelSelectionDropDown;

        public TMP_Text ModelSelectionText;

        public GameObject ModelLoadButtonObject;

        public Slider GenerationProgressSlider;

        private bool modelStorageLoaded = false;

        public GameObject loadTextObject;
        private TMP_Text loadTextText;

        public GameObject loadIconObject;
        private RectTransform loadIconTransform;
        public float LoadAnimationRotationSpeed;

        private CancellationToken token;





        // Start is called before the first frame update
        async void Start()
        {
            ModelSelectionDropDown = ModelSelectionDropDownObject.GetComponent<TMP_Dropdown>();

            ModelSelectionText.text = "";

            loadIconTransform = loadIconObject.GetComponent<RectTransform>();
            loadTextText = loadTextObject.GetComponent<TMP_Text>();

            ModelSelectionDropDownObject.SetActive(false);
            ModelLoadButtonObject.SetActive(false);
            GenerationProgressSlider.gameObject.SetActive(false);

            token = ThreadingUtility.QuitToken;
            await ModelLoader.instance.TryToLoadModelInformationFromEclipse(SetUpUI, SetUpModelLoadErrorMessage);
        }

        public void SetUpUI()
        {
            modelStorageLoaded = true;

            ModelSelectionText.text = "Select a Software Universe to load:";
            foreach (ActiveToggleable toShow in ShowOnLoadingError)
                toShow.Hide();

            ModelSelectionDropDownObject.SetActive(true);
            ModelLoadButtonObject.SetActive(true);
            loadIconObject.SetActive(false);
            loadTextObject.SetActive(false);

            ModelSelectionDropDown.Select();
            ModelSelectionDropDown.options.Clear();
            foreach (ModelStorageEntry entry in ModelLoader.instance.ModelStorage.entries)
            {
                ModelSelectionDropDown.options.Add(new TMP_Dropdown.OptionData() { text = entry.systemName });
            }
            ModelSelectionDropDown.value = -1;

            // DEV TRICK: makes the first found system load instantly
            LoadButtonClicked();
        }

        public async void SetUpModelLoadErrorMessage(string error)
        {
            ModelSelectionText.text = error;
            foreach (ActiveToggleable toShow in ShowOnLoadingError)
                toShow.Show();

            try
            {
                token.ThrowIfCancellationRequested();
            } catch(System.OperationCanceledException)
            {
                Debug.Log("Stopped trying to connect to Eclipse...");
                return;
            }

            await Task.Delay(500);
            await ModelLoader.instance.TryToLoadModelInformationFromEclipse(SetUpUI, SetUpModelLoadErrorMessage);
        }





        // Update is called once per frame
        void Update()
        {
            if(!modelStorageLoaded)
            {
                Vector3 loadIconRotation = loadIconTransform.localEulerAngles;
                loadIconRotation.z += Time.deltaTime * LoadAnimationRotationSpeed;
                loadIconTransform.localEulerAngles = loadIconRotation;
            }
        }





        public void LoadButtonClicked()
        {
            ModelStorageEntry selectedEntry = ModelLoader.instance.ModelStorage.entries[ModelSelectionDropDown.value];
            string baseFolderLocation = ModelLoader.instance.ModelStorage.baseFolder;
            ModelLoader.instance.LoadVisualizationModel(baseFolderLocation + selectedEntry.visualizationModelFileName);
            ModelLoader.instance.LoadQualityModel(baseFolderLocation + selectedEntry.qualityModelFileName);

            ModelSelectionText.text = "";
            if (ModelLoader.instance.VisualizationModel == null)
            {
                ModelSelectionText.text += "\nCould not load visualization model - is the model corrupted?";
            }
            if (ModelLoader.instance.QualityModel == null)
            {
                ModelSelectionText.text += "\nCould not load quality model - is the model corrupted?";
            }
            
            if (ModelLoader.instance.VisualizationModel != null && ModelLoader.instance.QualityModel != null)
            {
                StartCoroutine(StartGenerationAndUpdateUI(ModelLoader.instance.VisualizationModel));
            }
        }

        private IEnumerator StartGenerationAndUpdateUI(ISAVisualizationModel model)
        {
            yield return StartCoroutine(ModelStatistics.Initialize(model));

            ModelSelectionText.gameObject.SetActive(false);
            ModelSelectionDropDownObject.SetActive(false);
            ModelLoadButtonObject.SetActive(false);

            GenerationProgressSlider.gameObject.SetActive(true);
            loadTextObject.SetActive(true);
            UpdateGenerationProgress("", 0);

            while (SolarSystemManager.INSTANCE == null || SolarSystemManager.INSTANCE.SolarSystemGenerator == null)
            {
                UpdateGenerationProgress("Loading Solar System Scene", 0);
                yield return null;
            }

            Debug.Log("Successfully loaded visualization model. Generating sun system visualization...");
            yield return StartCoroutine(SolarSystemManager.INSTANCE.SolarSystemGenerator.GenerateSolarSystem(this, model));

            gameObject.SetActive(false);
        }





        public void UpdateGenerationProgress(string taskDescription, float newValue)
        {
            loadTextText.text = taskDescription;
            GenerationProgressSlider.value = newValue;
        }
    }

}