using ISA.SolarOverview;
using ISA.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ISAUIInteractionSynchronizer
{

    public static ISAUIInteractionSynchronizer INSTANCE = new ISAUIInteractionSynchronizer();

    private Dictionary<ISASynchronizedUIElementListener, ISANamedVisualElement> listenersToAddSafely;
    private Dictionary<ISANamedVisualElement, List<ISASynchronizedUIElementListener>> listenersMap;

    private List<ISANamedVisualElement> hoveredElements = new List<ISANamedVisualElement>();
    private List<ISANamedVisualElement> activeElements = new List<ISANamedVisualElement>();

    private ISAUIInteractionSynchronizer()
    {
        listenersMap = new Dictionary<ISANamedVisualElement, List<ISASynchronizedUIElementListener>>();
        listenersToAddSafely = new Dictionary<ISASynchronizedUIElementListener, ISANamedVisualElement>();
    }



    public void RegisterListenerAfterNextNotifyEvent(ISANamedVisualElement ecoreElement, AbstractInfoCanvasManager abstractInfoCanvasManager)
    {
        if(!listenersToAddSafely.ContainsKey(abstractInfoCanvasManager))
            listenersToAddSafely.Add(abstractInfoCanvasManager, ecoreElement);
    }

    public void RegisterListener(ISANamedVisualElement elementToReceiveUpdatesFor, ISASynchronizedUIElementListener newListener)
    {
        if (listenersMap.TryGetValue(elementToReceiveUpdatesFor, out List<ISASynchronizedUIElementListener> existingListeners))
        {
            existingListeners.Add(newListener);
        }
        else
        {
            List<ISASynchronizedUIElementListener> newListenerList = new List<ISASynchronizedUIElementListener>();
            newListenerList.Add(newListener);
            listenersMap.Add(elementToReceiveUpdatesFor, newListenerList);
        }
    }

    public void RemoveListener(ISASynchronizedUIElementListener listener)
    {
        foreach (ISANamedVisualElement key in listenersMap.Keys)
        {
            if (listenersMap.TryGetValue(key, out List<ISASynchronizedUIElementListener> existingListeners))
            {
                existingListeners.Remove(listener);
            }
        }

        listenersToAddSafely.Remove(listener);
    }



    internal void NotifyHoverStart(ISANamedVisualElement e)
    {
        NotifyHovered(e, true);
    }

    internal void NotifyHoverEnd(ISANamedVisualElement e)
    {
        NotifyHovered(e, false);
    }

    internal void NotifyHovered(ISANamedVisualElement e, bool nowHovered)
    {
        if (nowHovered)
        {
            if (!hoveredElements.Contains(e))
                hoveredElements.Add(e);
        }
        else
        {
            hoveredElements.Remove(e);
        }
        Notify(e);
    }

    internal void NotifyToggleActive(ISANamedVisualElement e)
    {
        if (!activeElements.Contains(e))
            activeElements.Add(e);
        else
            activeElements.Remove(e);
        Notify(e);
    }

    internal void NotifyActiveState(ISANamedVisualElement e, bool nowActive)
    {
        if (nowActive)
        {
            if (!activeElements.Contains(e))
                activeElements.Add(e);
        }
        else
        {
            activeElements.Remove(e);
        }
        Notify(e);
    }

    internal void ResetAndNotifyAll()
    {
        List<ISANamedVisualElement> elementsToUpdate = new List<ISANamedVisualElement>();
        elementsToUpdate.AddRange(activeElements);
        elementsToUpdate.AddRange(hoveredElements);

        activeElements.Clear();
        hoveredElements.Clear();

        foreach(ISANamedVisualElement e in elementsToUpdate)
            Notify(e);
    }

    private void Notify(ISANamedVisualElement e)
    {
        if (listenersMap.TryGetValue(e, out List<ISASynchronizedUIElementListener> listeners))
        {
            foreach (ISASynchronizedUIElementListener listener in listeners)
            {
                listener.NotifyUpdateOnElement(e);
            }
        }

        foreach (ISASynchronizedUIElementListener newListener in listenersToAddSafely.Keys)
        {
            RegisterListener(listenersToAddSafely[newListener], newListener);
            //newListener.NotifyUpdateOnElement(e);
        }
        listenersToAddSafely.Clear();
    }



    public bool isHovered(ISANamedVisualElement e)
    {
        return hoveredElements.Contains(e);
    }

    public bool isActive(ISANamedVisualElement e)
    {
        return activeElements.Contains(e);
    }

}


public interface ISASynchronizedUIElementListener
{
    public void NotifyUpdateOnElement(ISANamedVisualElement e);
}