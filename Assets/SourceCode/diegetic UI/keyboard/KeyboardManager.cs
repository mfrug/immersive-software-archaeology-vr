using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
public class KeyboardManager : MonoBehaviour
{

    public static KeyboardManager INSTANCE;

    public Transform KeyParent;
    public TMP_Text TextField;

    private List<KeyboardListener> listeners = new List<KeyboardListener>();

    public void addListener(KeyboardListener newListener)
    {
        listeners.Add(newListener);
    }



    void Start()
    {
        INSTANCE = this;
        foreach (Transform keyObject in KeyParent)
        {
            Button keyButton = keyObject.GetComponent<Button>();
            keyButton.onClick.AddListener(delegate { keyPressed(keyObject.name); });
        }
        setText("");

        GetComponent<Canvas>().worldCamera = SceneHandler.INSTANCE.VRPointerCamera;
    }



    private void keyPressed(string key)
    {
        if (key.Equals("Shift"))
        {
        }
        else if (key.Equals("Enter"))
        {
            foreach (KeyboardListener listener in listeners)
                listener.enterPressed(getText());
        }
        else if (key.Equals("Remove"))
        {
            if (getText().Length == 0)
                return;

            setText(getText().Substring(0, getText().Length - 1));
            foreach (KeyboardListener listener in listeners)
                listener.keyPressed();
        }
        else if (key.Equals("Clear"))
        {
            setText("");
            foreach (KeyboardListener listener in listeners)
                listener.cleared();
        }
        else
        {
            setText(getText() + key);
            foreach (KeyboardListener listener in listeners)
                listener.keyPressed();
        }
    }

    public void setText(string newText)
    {
        TextField.text = newText;
    }
    public string getText()
    {
        return TextField.text;
    }
}

public interface KeyboardListener
{
    public void keyPressed();
    public void enterPressed(string text);
    public void cleared();
}