using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;

public class DeviceSelectionCircleItem : MonoBehaviour
{
    public Interactable interactableTool;
    public Renderer backgroundPlaneRenderer;
    public Canvas descriptionCanvas;

    public UnityEvent onToolEquip;
    public UnityEvent onToolUnEquip;

    public Transform attachmentOffset;



    public void Highlight()
    {
        backgroundPlaneRenderer.material.SetColor("_BaseColor", new Color(1, 1, 1, 1));
        backgroundPlaneRenderer.material.SetColor("_EmissionColor", new Color(50, 50, 50, 1));
    }

    public void UnHighlight()
    {
        backgroundPlaneRenderer.material.SetColor("_BaseColor", new Color(1, 1, 1, 0.5f));
        backgroundPlaneRenderer.material.SetColor("_EmissionColor", new Color(0, 0, 0, 0));
    }

    // Is called once a user decides to equip this tool
    public void Equip(Hand hand)
    {
        //backgroundPlaneRenderer.gameObject.SetActive(false);
        //descriptionCanvas.gameObject.SetActive(false);

        interactableTool.gameObject.transform.localPosition = Vector3.zero;
        interactableTool.gameObject.transform.localRotation = Quaternion.identity;

        hand.AttachObject(interactableTool.gameObject, GrabTypes.None, Hand.defaultAttachmentFlags, attachmentOffset);
        hand.HoverLock(interactableTool);

        if (onToolEquip != null)
            onToolEquip.Invoke();
    }

    // Is called once a user decides to equip another tool
    public void UnEquip(Hand hand)
    {
        //backgroundPlaneRenderer.gameObject.SetActive(true);
        //descriptionCanvas.gameObject.SetActive(true);

        hand.DetachObject(interactableTool.gameObject);
        hand.HoverUnlock(interactableTool);

        interactableTool.gameObject.transform.localPosition = Vector3.zero;

        if (onToolUnEquip != null)
            onToolUnEquip.Invoke();
    }

}
