using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StickyPostManager : MonoBehaviour
{

    public TMP_Text text;
    private float StartTime = 0;



    void Update()
    {
        if(StartTime != 0)
        {
            float time = Time.time - StartTime;
            if (time < 10)
                text.text = "00:0" + (int)Mathf.Floor(time);
            else
                text.text = "00:" + (int)Mathf.Floor(time);
        }
    }



    public void StartTimer()
    {
        StartTime = Time.time;
    }
}
