using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExecutionSpeedGadgetManager : MonoBehaviour
{

    public Animator animator;
    public Slider progressSlider;

    private bool animationPlaying = false;
    private float animationProgressValue = 0f;
    private float animationSpeed = 1f;


    public void Start()
    {
        StartCoroutine(FindAnimationComponent());
    }

    private IEnumerator FindAnimationComponent()
    {
        while(true)
        {
            yield return null;

            GameObject obj = GameObject.Find("inhabitants");
            if (obj == null)
            {
                continue;
            }

            animator = obj.GetComponent<Animator>();
        }
    }

    public void Update()
    {
        if (animator != null)
        {
            if (animationPlaying)
            {
                animator.enabled = true;

                float animationProgressDiff = animationSpeed * Time.deltaTime / 60f;
                while (progressSlider.value + animationProgressDiff > 1f)
                    animationProgressDiff -= 1f;

                progressSlider.value += animationProgressDiff;
            }
            else
            {
                animator.enabled = false;
            }
        }
    }


    public void Play()
    {
        if(animator != null)
            animationPlaying = true;
    }

    public void Pause()
    {
        if (animator != null)
            animationPlaying = false;
    }

    public void Faster()
    {
        animationSpeed *= 2f;
        Debug.Log("New Speed: " + animationSpeed);
    }

    public void Slower()
    {
        animationSpeed /= 2f;
        Debug.Log("New Speed: " + animationSpeed);
    }

    public void PlayAnimationAt(float timePoint)
    {
        if (animator != null)
        {
            animationProgressValue = Mathf.Max(0f, Mathf.Min(timePoint, 1f));
            animator.Play(0, 0, animationProgressValue);
        }
    }

}
