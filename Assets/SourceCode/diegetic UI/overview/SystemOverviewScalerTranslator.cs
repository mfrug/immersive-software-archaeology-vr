using ISA.SolarOverview;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.InteractionSystem;

namespace ISA.devices
{
    public class SystemOverviewScalerTranslator : MonoBehaviour
    {

        public List<Transform> ToShowWhenInScaleRotate;

        public SteamVR_Input_Sources PrimaryHandInputSource;
        public SteamVR_Input_Sources SecondaryHandInputSource;
        public SteamVR_Action_Boolean TargetAction;

        public Button Button;
        private Color ButtonDefaultColor;
        public Color ButtonHighlightColor;

        public ScalingPivotGizmo PrimaryHandCursor;
        public ScalingPivotGizmo SecondaryHandCursor;
        public LineRenderer ScaleLineRenderer;
        public TMP_Text ScaleText;

        public Hand SecondaryHand;
        public bool RelativeScaling = true;
        public bool CopyRotationFromHand = false;
        public float minScaleValue = 0.25f;
        public float maxScaleValue = 3f;

        private bool translateMode = false;

        private Vector3 oldSecondaryHandCursorPosition;

        private Quaternion previousRotation;
        private Vector3 previousLookDirection;

        private bool moveActive = false;
        private bool scaleRotateActive = false;
        private float startScale;
        private float pivotHandDistance;

        private bool showHints = true;



        public void Start()
        {
            ButtonDefaultColor = Button.GetComponent<Image>().color;
            foreach (Transform t in ToShowWhenInScaleRotate)
                t.gameObject.SetActive(false);
        }



        public void ToggleMoveScaleMode()
        {
            if (!SolarSystemManager.INSTANCE.gameObject.activeInHierarchy)
                return;

            if (!this.translateMode)
            {
                translateMode = true;
                //SceneHandler.INSTANCE.VRPointer.Disable();

                PrimaryHandCursor.gameObject.SetActive(true);
                SecondaryHandCursor.gameObject.SetActive(true);

                Button.gameObject.GetComponent<Image>().color = ButtonHighlightColor;
                if(showHints)
                {
                    ControllerButtonHints.ShowTextHint(SecondaryHand, TargetAction, "You are in transform mode now.\nUse the trigger on this hand to translate the planet overview!\nUse the trigger on the other hand to enter scale/rotate mode.", true);
                    ControllerButtonHints.ShowTextHint(SecondaryHand.otherHand, TargetAction, "Use the trigger on this hand to scale and rotate the planet overview!\nThe gizmo attached to the other hand serves as pivot center for the scaling and rotation.", true);
                }
            }
            else
            {
                translateMode = false;
                //SceneHandler.INSTANCE.VRPointer.Enable();

                PrimaryHandCursor.gameObject.SetActive(false);
                SecondaryHandCursor.gameObject.SetActive(false);

                ControllerButtonHints.HideTextHint(SecondaryHand, TargetAction);
                ControllerButtonHints.HideTextHint(SecondaryHand.otherHand, TargetAction);
                Button.gameObject.GetComponent<Image>().color = ButtonDefaultColor;
            }
        }



        public void Update()
        {
            if (!this.translateMode)
                return;

            if (TargetAction.GetStateDown(SecondaryHandInputSource))
                registerMoveActive();
            if (TargetAction.GetStateUp(SecondaryHandInputSource))
                unregisterMoveActive();

            if (TargetAction.GetStateDown(PrimaryHandInputSource))
                registerScaleRotateActive();
            if (TargetAction.GetStateUp(PrimaryHandInputSource))
                unregisterScaleRotateActive();

            if (moveActive)
            {
                handleMove();
            }
            if (scaleRotateActive)
            {
                handleRotation();
                handleScaling();
                UpdateScaleLine();
            }
        }

        private void registerMoveActive()
        {
            if (moveActive)
                return;
            moveActive = true;
            ControllerButtonHints.HideTextHint(SecondaryHand, TargetAction);

            oldSecondaryHandCursorPosition = SecondaryHandCursor.transform.position;
            previousLookDirection = SecondaryHandCursor.transform.position - PrimaryHandCursor.transform.position;
        }

        private void unregisterMoveActive()
        {
            if (!moveActive)
                return;
            moveActive = false;
            unregisterScaleRotateActive();
        }

        private void registerScaleRotateActive()
        {
            if (scaleRotateActive /*|| !moveActive*/)
                return;
            scaleRotateActive = true;
            ControllerButtonHints.HideTextHint(SecondaryHand.otherHand, TargetAction);
            showHints = false;

            foreach (Transform t in ToShowWhenInScaleRotate)
                t.gameObject.SetActive(true);

            {
                // Position the overview within its parent sothat the rotation will follow the secondary hand's pivot point
                Vector3 originalSystemPosition = SolarSystemManager.INSTANCE.transform.position;
                SolarSystemManager.INSTANCE.transform.position = SecondaryHandCursor.transform.position;
                SolarSystemManager.INSTANCE.SolarSystemGenerator.transform.position = originalSystemPosition;
            }

            ScaleLineRenderer.gameObject.SetActive(true);
            ScaleText.gameObject.SetActive(true);

            startScale = SolarSystemManager.INSTANCE.transform.localScale.x;
            pivotHandDistance = Vector3.Distance(SecondaryHandCursor.transform.position, PrimaryHandCursor.transform.position);
            previousRotation = Quaternion.identity;
        }

        private void unregisterScaleRotateActive()
        {
            if (!scaleRotateActive)
                return;
            scaleRotateActive = false;

            foreach (Transform t in ToShowWhenInScaleRotate)
                t.gameObject.SetActive(false);

            {
                // Reposition the overview to its original position, with local position 0
                SolarSystemManager.INSTANCE.transform.position += SolarSystemManager.INSTANCE.SolarSystemGenerator.transform.position - SolarSystemManager.INSTANCE.transform.position;
                SolarSystemManager.INSTANCE.SolarSystemGenerator.transform.localPosition = Vector3.zero;
            }

            ScaleLineRenderer.gameObject.SetActive(false);
            ScaleText.gameObject.SetActive(false);
        }

        private void handleMove()
        {
            float distanceGizmoToSun = Vector3.Distance(SecondaryHandCursor.transform.position, SolarSystemManager.INSTANCE.transform.position);
            float distanceGizmoToOrbitPlusBuffer = distanceGizmoToSun - SolarSystemManager.INSTANCE.transform.lossyScale.x * 2;
            SolarSystemManager.INSTANCE.transform.position +=
                Mathf.Max(1f, distanceGizmoToOrbitPlusBuffer) *
                (SecondaryHandCursor.transform.position - oldSecondaryHandCursorPosition);

            oldSecondaryHandCursorPosition = SecondaryHandCursor.transform.position;
        }

        private void handleRotation()
        {
            if(!CopyRotationFromHand)
            {
                Vector3 currentLookDirection = SecondaryHandCursor.transform.position - PrimaryHandCursor.transform.position;
                Quaternion someRotation = Quaternion.FromToRotation(previousLookDirection, currentLookDirection);

                if (previousRotation != Quaternion.identity)
                {
                    Quaternion thisRotation = Quaternion.identity * Quaternion.Inverse(someRotation);
                    Quaternion oldRotation = Quaternion.identity * Quaternion.Inverse(previousRotation);
                    Quaternion deltaRotation = Quaternion.Inverse(thisRotation) * (oldRotation);
                    SolarSystemManager.INSTANCE.transform.rotation = deltaRotation * SolarSystemManager.INSTANCE.transform.rotation;
                }

                previousRotation = someRotation;
            }
            else
            {
                if (previousRotation != Quaternion.identity)
                {
                    Quaternion thisRotation = Quaternion.identity * Quaternion.Inverse(SecondaryHand.otherHand.transform.rotation);
                    Quaternion oldRotation = Quaternion.identity * Quaternion.Inverse(previousRotation);
                    Quaternion deltaRotation = Quaternion.Inverse(thisRotation) * (oldRotation);
                    SolarSystemManager.INSTANCE.transform.rotation = deltaRotation * SolarSystemManager.INSTANCE.transform.rotation;
                }

                previousRotation = SecondaryHand.otherHand.transform.rotation;
            }
        }

        private void handleScaling()
        {
            float currentHandDistance = Vector3.Distance(SecondaryHandCursor.transform.position, PrimaryHandCursor.transform.position);
            float scale = startScale * currentHandDistance / pivotHandDistance;
            if (float.IsNaN(scale) || float.IsInfinity(scale))
                return;
            else if (scale < minScaleValue)
                scale = minScaleValue;
            else if (scale > maxScaleValue)
                scale = maxScaleValue;

            if (RelativeScaling)
            {
                // Get the pivot point position relative to the solar system at the time point where scaling is triggered
                Vector3 scalingPivot = SecondaryHandCursor.transform.position;//PivotHand.transform.position;// - SolarSystemObjectReferencesManager.INSTANCE.transform.position;
                                                                              // Set the y value to the value of the solar system,
                                                                              // we do not want to do the scaling relative to the y coordinate!
                                                                              //scalingPivot.y = SolarSystemObjectReferencesManager.INSTANCE.transform.position.y;

                // diff between object pivot to desired pivot/origin
                Vector3 diff = SolarSystemManager.INSTANCE.transform.position - scalingPivot;

                float relativeScaleFactor = scale / SolarSystemManager.INSTANCE.transform.localScale.x;

                // calc final position
                Vector3 finalPosition = scalingPivot + diff * relativeScaleFactor;

                // perform the translation
                SolarSystemManager.INSTANCE.transform.position = finalPosition;
            }

            // perform the scale
            SolarSystemManager.INSTANCE.transform.localScale = scale * Vector3.one;
        }



        private void UpdateScaleLine()
        {
            Vector3 position1 = ScaleLineRenderer.transform.InverseTransformPoint(PrimaryHandCursor.transform.position);
            Vector3 position2 = ScaleLineRenderer.transform.InverseTransformPoint(SecondaryHandCursor.transform.position);
            ScaleLineRenderer.SetPosition(0, position1);
            ScaleLineRenderer.SetPosition(1, position2);

            ScaleText.SetText("" + Mathf.Round(100f * SolarSystemManager.INSTANCE.transform.localScale.x) / 100);
            ScaleText.transform.position = PrimaryHandCursor.transform.position + 0.5f * (SecondaryHandCursor.transform.position - PrimaryHandCursor.transform.position);

            Vector3 ScaleTextToPrimaryHandDirection = (PrimaryHandCursor.transform.position - ScaleText.transform.position).normalized;
            Vector3 HeadToScaleTextDirection = (SceneHandler.INSTANCE.VRHeadCollider.transform.position - ScaleText.transform.position).normalized;
            Vector3 ScaleTextUpDirection = Vector3.Cross(HeadToScaleTextDirection, ScaleTextToPrimaryHandDirection);
            ScaleText.transform.rotation = Quaternion.LookRotation(-HeadToScaleTextDirection, -ScaleTextUpDirection);

            //ScaleLineRenderer.material.SetFloat("_Tiling", 10f * Vector3.Distance(SecondaryHandCursor.transform.position, PrimaryHandCursor.transform.position));
        }
    }
}
