using ISA.SolarOverview;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemOverviewVisibilityHandler : MonoBehaviour
{

    public SpriteToggler ToggleableButtonImage;
    public Transform[] SubordinateElements;

    private bool visible = true;



    public void ToggleVisibility()
    {
        visible = !visible;
        if (visible)
        {
            ToggleableButtonImage.ChangeSprite(0);
        }
        else
        {
            ToggleableButtonImage.ChangeSprite(1);
        }

        SolarSystemManager.INSTANCE.gameObject.SetActive(visible);
        foreach (Transform element in SubordinateElements)
        {
            element.gameObject.SetActive(visible);
        }
    }

}
