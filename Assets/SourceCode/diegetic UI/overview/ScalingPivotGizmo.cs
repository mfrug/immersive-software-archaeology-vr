using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ISA.devices
{

    public class ScalingPivotGizmo : MonoBehaviour
    {
        public Transform RotationToFollow;

        /*
        private bool lockPosition = false;
        private Vector3 lockedPosition;
        private Vector3 oldLocalPosition;
        */

        private void Start()
        {
            gameObject.SetActive(false);
        }

        void Update()
        {
            transform.rotation = RotationToFollow.rotation;
            /*if (lockPosition)
                transform.position = lockedPosition;*/
        }

        /*
        public void LockLocation()
        {
            oldLocalPosition = transform.localPosition;

            lockPosition = true;
            lockedPosition = transform.position;
        }

        public void ReleaseLocation()
        {
            lockPosition = false;
            transform.localPosition = oldLocalPosition;
        }
        */
    }

}