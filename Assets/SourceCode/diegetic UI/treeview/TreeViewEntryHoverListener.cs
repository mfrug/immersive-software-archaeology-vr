using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace ISA.UI
{
    public class TreeViewEntryHoverListener : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler, IPointerUpHandler, IPointerClickHandler
    {
        private ISAUIInteractable interactable;

        public void Awake()
        {
            interactable = GetComponent<ISAUIInteractable>();
        }



        public void OnPointerEnter(PointerEventData eventData)
        {
            interactable.onHoverStart.Invoke();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            interactable.onHoverEnd.Invoke();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            interactable.onTriggerPressed.Invoke();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            interactable.onTriggerReleased.Invoke();
        }

        public void OnPointerClick(PointerEventData eventData)
        {

        }
    }
}