﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using static UnityEngine.UI.Button;
using ISA.SolarOverview;
using Valve.VR.InteractionSystem;
using System.Collections;

namespace ISA.UI
{
    [RequireComponent(typeof(Canvas))]
    public abstract class AbstractTreeViewUICanvasHandler : MonoBehaviour, ISASynchronizedUIElementListener
    {

        public GameObject treeViewer;
        public GameObject treeViewerContentParent;

        //private static int TEXT_SIZE = 14;
        public int treeViewEntryTextSize = 20;
        public int treeViewEntryLeftMargin = 20;
        public ISATreeNode rootNode { private set; get; }
        private Dictionary<ISANamedVisualElement, ISATreeNode> ecoreElementToTreeNodeMap = new Dictionary<ISANamedVisualElement, ISATreeNode>();

        public Color defaultColor = new Color(1f, 1f, 1f, 1f);
        public Color hovorColor = new Color(1f, 1f, 1f, 1f);
        public Color activeColor = new Color(1f, 0f, 0f, 1f);
        public Sprite arrowSprite;
        public Sprite Mark3DElementDefaultSprite;
        public Sprite Mark3DElementActiveSprite;

        public Sprite sunSprite;
        public Sprite planetSprite;
        public Sprite continentSprite;
        public Sprite citySprite;
        public Sprite buildingSprite;

        public void AssignRootNode(ISATreeNode node)
        {
            rootNode = node;
        }

        public virtual void UpdateTreeView()
        {
            CorrectCanvasDimensions(rootNode.CalculateHeight());
            AlignNodesRecursively(rootNode, true);
        }

        private void CorrectCanvasDimensions(int height)
        {
            RectTransform transform = treeViewerContentParent.GetComponent<RectTransform>();

            int newHeight = (int) Mathf.Ceil((height + 1) * 1.5f * treeViewEntryTextSize);
            transform.sizeDelta = new Vector2(0, newHeight);
        }

        private void AlignNodesRecursively(ISATreeNode node, bool parentVisible)
        {
            bool visible = node.IsVisible() && parentVisible;

            GameObject textObject = node.treeViewEntryObject;
            if (!visible)
            {
                textObject.SetActive(false);
            }
            else
            {
                Vector2 position = node.CalculatePositionInTree();
                textObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(-treeViewEntryTextSize, (position.y + 1) * -1.5f * treeViewEntryTextSize);

                GameObject arrowObject = textObject.transform.GetChild(0).gameObject;
                {
                    RectTransform arrowTransform = arrowObject.GetComponent<RectTransform>();
                    arrowTransform.localRotation = new Quaternion();
                    if (node.expanded)
                        arrowTransform.Rotate(new Vector3(0, 0, -90));
                }

                if (node.IsLeaf())
                    arrowObject.SetActive(false);
                textObject.SetActive(true);
            }

            foreach (ISATreeNode child in node.GetChildren(1))
                AlignNodesRecursively(child, visible);
        }

        public ISATreeNode CreateEntry(ISANamedVisualElement ecoreElement, ISATreeNode parentNode)
        {
            GameObject textObject = new GameObject("TreeView-Entry: " + ecoreElement.name + "(order of children matters!)");
            textObject.transform.parent = treeViewerContentParent.transform;
            textObject.transform.localRotation = Quaternion.identity;
            textObject.transform.localScale = Vector3.one;

            ISATreeNode node = new ISATreeNode(ecoreElement, parentNode, textObject);
            Vector2 position = node.CalculatePositionInTree();

            TextMeshProUGUI text = textObject.AddComponent<TextMeshProUGUI>();
            text.text = ecoreElement.name;
            text.fontSize = treeViewEntryTextSize;
            text.alignment = TextAlignmentOptions.MidlineLeft;
            text.color = defaultColor;
            text.overflowMode = TextOverflowModes.Ellipsis;
            text.margin = new Vector4((position.x + 2) * treeViewEntryLeftMargin, 0, 0, 0);

            RectTransform textTransform = textObject.GetComponent<RectTransform>();
            textTransform.anchorMin = new Vector2(0, 1);
            textTransform.anchorMax = new Vector2(1, 1);
            textTransform.pivot = new Vector2(0.5f, 0.5f);
            textTransform.localPosition = Vector3.zero;
            textTransform.sizeDelta = new Vector2(-2f * treeViewEntryTextSize, 1.5f * treeViewEntryTextSize);

            // Create button that is responsible for handling clicks on the entire entry
            {
                Button button = textObject.AddComponent<Button>();
                button.onClick.AddListener(delegate {
                    //if(node.IsLeaf())
                    //    ISAUIInteractionSynchronizer.INSTANCE.NotifyClicked(ecoreElement);
                    if(!ecoreElementToTreeNodeMap[ecoreElement].IsLeaf())
                    {
                        ecoreElementToTreeNodeMap[ecoreElement].ToggleExpanded();
                        UpdateTreeView();
                    }
                });

                ColorBlock cb = ColorBlock.defaultColorBlock;
                cb.normalColor = new Color(1f, 1f, 1f, 0.5f);
                cb.highlightedColor = new Color(1f, 1f, 1f, 1f);
                button.colors = cb;

                //BoxCollider collider = textObject.AddComponent<BoxCollider>();
                //collider.isTrigger = true;
                //collider.size = new Vector3(105, treeViewEntryTextSize / 2, 1);
                ////collider.center = new Vector3(-treeViewEntryTextSize, 0, 0);
                //textObject.AddComponent<PhysicalButtonTrigger>();
            }

            {
                ISAUIInteractable interactable = textObject.AddComponent<ISAUIInteractable>();
                interactable.onHoverStart.AddListener(delegate { ISAUIInteractionSynchronizer.INSTANCE.NotifyHoverStart(ecoreElement); });
                interactable.onHoverEnd.AddListener(delegate { ISAUIInteractionSynchronizer.INSTANCE.NotifyHoverEnd(ecoreElement); });

                textObject.AddComponent<TreeViewEntryHoverListener>();
            }

            {
                GameObject arrowObject = new GameObject("TreeView-Arrow: " + ecoreElement.name);
                arrowObject.transform.parent = textObject.transform;
                arrowObject.transform.localPosition = Vector3.zero;
                arrowObject.transform.localRotation = Quaternion.identity;
                arrowObject.transform.localScale = Vector3.one;

                Image arrowImage = arrowObject.AddComponent<Image>();
                arrowImage.sprite = arrowSprite;
                arrowImage.color = defaultColor;

                RectTransform arrowTransform = arrowObject.GetComponent<RectTransform>();
                arrowTransform.anchorMin = new Vector2(0f, 0.5f);
                arrowTransform.anchorMax = new Vector2(0f, 0.5f);
                arrowTransform.pivot = new Vector2(0.5f, 0.5f);
                arrowTransform.sizeDelta = new Vector2(treeViewEntryTextSize / 2, treeViewEntryTextSize / 2);
                arrowTransform.localPosition = Vector3.zero;
                arrowTransform.anchoredPosition = new Vector2(text.margin.x - treeViewEntryTextSize * 2, 0);
            }

            {
                GameObject iconObject = new GameObject("TreeView-Icon: " + ecoreElement.name);
                iconObject.transform.parent = textObject.transform;
                iconObject.transform.localPosition = Vector3.zero;
                iconObject.transform.localRotation = Quaternion.identity;
                iconObject.transform.localScale = Vector3.one;

                Image iconImage = iconObject.AddComponent<Image>();
                if (ecoreElement is ISASolarSystem)
                    iconImage.sprite = sunSprite;
                else if (ecoreElement is ISAPlanet)
                    iconImage.sprite = planetSprite;
                else if (ecoreElement is ISAContinent)
                    iconImage.sprite = continentSprite;
                else if (ecoreElement is ISACity)
                    iconImage.sprite = citySprite;
                else if (ecoreElement is ISABuilding)
                    iconImage.sprite = buildingSprite;
                else
                    Debug.LogError("Unexpected type of element in tree view: " + ecoreElement.qualifiedName);
                iconImage.color = defaultColor;

                RectTransform iconTransform = iconObject.GetComponent<RectTransform>();
                iconTransform.anchorMin = new Vector2(0f, 0.5f);
                iconTransform.anchorMax = new Vector2(0f, 0.5f);
                iconTransform.pivot = new Vector2(0.5f, 0.5f);
                iconTransform.sizeDelta = new Vector2(treeViewEntryTextSize, treeViewEntryTextSize);
                iconTransform.localPosition = Vector3.zero;
                iconTransform.anchoredPosition = new Vector2(text.margin.x - treeViewEntryTextSize * 1, 0);
            }

            if(!(/*ecoreElement is ISABuilding || */ecoreElement is ISACompoundContinent))
            {
                GameObject mark3DElementbuttonObject = new GameObject("Mark3DElementButton");
                mark3DElementbuttonObject.transform.parent = textObject.transform;
                mark3DElementbuttonObject.transform.localScale = Vector3.one;

                RectTransform rectTrans = mark3DElementbuttonObject.AddComponent<RectTransform>();
                rectTrans.anchorMin = new Vector2(1f, 0.5f);
                rectTrans.anchorMax = new Vector2(1f, 0.5f);
                rectTrans.pivot = new Vector2(0.5f, 0.5f);
                rectTrans.sizeDelta = new Vector2(1.5f * treeViewEntryTextSize, 1.5f * treeViewEntryTextSize);
                rectTrans.localPosition = Vector3.zero;
                rectTrans.anchoredPosition = new Vector2(1f * treeViewEntryTextSize, 0);
                rectTrans.localRotation = Quaternion.identity;

                Image mark3DElementImage = mark3DElementbuttonObject.AddComponent<Image>();
                mark3DElementImage.sprite = Mark3DElementDefaultSprite;
                mark3DElementImage.color = defaultColor;

                Button mark3DElementbutton = mark3DElementbuttonObject.AddComponent<Button>();
                mark3DElementbutton.onClick.AddListener(delegate {
                    ISAUIInteractionSynchronizer.INSTANCE.NotifyToggleActive(ecoreElement);
                });

                ColorBlock cb = ColorBlock.defaultColorBlock;
                cb.normalColor = new Color(1f, 1f, 1f, 0.25f);
                cb.highlightedColor = new Color(1f, 1f, 1f, 1f);
                mark3DElementbutton.colors = cb;

                //BoxCollider collider = mark3DElementbuttonObject.AddComponent<BoxCollider>();
                //collider.isTrigger = true;
                //collider.size = new Vector3(treeViewEntryTextSize / 1f, treeViewEntryTextSize / 1f, 1);
                //mark3DElementbuttonObject.AddComponent<PhysicalButtonTrigger>();
            }

            textObject.SetActive(false);

            ISAUIInteractionSynchronizer.INSTANCE.RegisterListener(ecoreElement, this);
            ecoreElementToTreeNodeMap.Add(ecoreElement, node);
            return node;
        }

        public void NotifyUpdateOnElement(ISANamedVisualElement e)
        {
            ISATreeNode node = ecoreElementToTreeNodeMap[e];
            if (ISAUIInteractionSynchronizer.INSTANCE.isActive(e))
            {
                node.treeViewEntryObject.GetComponent<TextMeshProUGUI>().color = activeColor;
                if (node.treeViewEntryObject.transform.childCount >= 3)
                {
                    node.treeViewEntryObject.transform.GetChild(2).GetComponent<Image>().color = activeColor;
                    node.treeViewEntryObject.transform.GetChild(2).GetComponent<Image>().sprite = Mark3DElementActiveSprite;
                }
            }
            else if (ISAUIInteractionSynchronizer.INSTANCE.isHovered(e))
            {
                node.treeViewEntryObject.GetComponent<TextMeshProUGUI>().color = hovorColor;
                if (node.treeViewEntryObject.transform.childCount >= 3)
                {
                    node.treeViewEntryObject.transform.GetChild(2).GetComponent<Image>().color = hovorColor;
                    node.treeViewEntryObject.transform.GetChild(2).GetComponent<Image>().sprite = Mark3DElementDefaultSprite;
                }
            }
            else
            {
                node.treeViewEntryObject.GetComponent<TextMeshProUGUI>().color = defaultColor;
                if (node.treeViewEntryObject.transform.childCount >= 3)
                {
                    node.treeViewEntryObject.transform.GetChild(2).GetComponent<Image>().color = defaultColor;
                    node.treeViewEntryObject.transform.GetChild(2).GetComponent<Image>().sprite = Mark3DElementDefaultSprite;
                }
            }
        }
    }
}
