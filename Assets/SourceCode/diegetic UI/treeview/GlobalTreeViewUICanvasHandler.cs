﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using static UnityEngine.UI.Button;
using ISA.SolarOverview;
using Valve.VR.InteractionSystem;
using System.Collections;

namespace ISA.UI
{
    public class GlobalTreeViewUICanvasHandler : AbstractTreeViewUICanvasHandler, SolarSystemOverviewLoadingListener
    {
        public static GlobalTreeViewUICanvasHandler INSTANCE;



        public GameObject heading;



        public void Start()
        {
            INSTANCE = this;
            SolarSystemManager.INSTANCE.RegisterLoadingProcessObserver(this);
        }
        public override void UpdateTreeView()
        {
            heading.GetComponent<TextMeshProUGUI>().text = rootNode.label;
            base.UpdateTreeView();
        }



        public void NotifySolarSystemOverviewFinishedLoading()
        {
            UpdateTreeView();
        }

        public void NotifySolarSystemSwitchedInsideOutsideMode() {}
    }
}
