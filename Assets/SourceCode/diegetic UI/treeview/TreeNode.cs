﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ISA.UI
{

    public class ISATreeNode
    {

        public string label { get; private set; }
        public bool expanded { get; private set; }

        private ISATreeNode parent;
        private List<ISATreeNode> children = new List<ISATreeNode>();

        private ISANamedVisualElement ecoreElement;
        public GameObject treeViewEntryObject { get; private set; }



        public ISATreeNode(ISANamedVisualElement ecoreElement, ISATreeNode parentNode, GameObject treeViewEntryObject)
        {
            this.ecoreElement = ecoreElement;
            this.treeViewEntryObject = treeViewEntryObject;

            this.label = ecoreElement.name;
            this.expanded = false;

            if(parentNode != null)
                SetParent(parentNode);
        }

        internal bool ToggleExpanded()
        {
            this.expanded = !this.expanded;
            return !IsLeaf();
        }

        internal bool IsVisible()
        {
            if (parent == null)
                return true;

            if (!parent.expanded)
                return false;

            return parent.IsVisible();
        }

        public void AddChild(ISATreeNode newChild)
        {
            if(newChild.GetParent() != this)
                newChild.SetParent(this);

            this.children.Add(newChild);
        }

        public void RemoveChild(ISATreeNode existingChild)
        {
            this.children.Remove(existingChild);
        }

        public ISATreeNode GetParent()
        {
            return this.parent;
        }

        public void SetParent(ISATreeNode newParent)
        {
            if(this.parent != null)
                this.parent.RemoveChild(this);

            this.parent = newParent;
            newParent.AddChild(this);
        }

        public List<ISATreeNode> GetChildren(int depth)
        {
            List<ISATreeNode> result = new List<ISATreeNode>();

            if(depth == 0)
                return result;

            foreach (ISATreeNode child in children)
            {
                result.Add(child);
                result.AddRange(child.GetChildren(depth - 1));
            }
            return result;
        }

        public bool IsLeaf()
        {
            return this.children.Count == 0;
        }

        public ISATreeNode GetRootNode()
        {
            ISATreeNode root = this;
            while (root.parent != null)
                root = root.parent;
            return root;
        }

        public int CalculateHeight()
        {
            int height = 1;

            if (!this.expanded)
                return height;

            foreach(ISATreeNode child in children)
            {
                height += child.CalculateHeight();
            }
            return height;
        }

        public Vector2 CalculatePositionInTree()
        {
            int xPosition = 0;
            List<ISATreeNode> nodeHierarchie = new List<ISATreeNode>();
            nodeHierarchie.Add(this);
            while (nodeHierarchie[nodeHierarchie.Count-1].parent != null)
            {
                nodeHierarchie.Add(nodeHierarchie[nodeHierarchie.Count - 1].parent);
                xPosition++;
            }

            int yPosition = 0;
            for (int i = nodeHierarchie.Count-1; i > 0; i--)
            {
                yPosition += 1;
                ISATreeNode currentNode = nodeHierarchie[i];
                foreach (ISATreeNode currentNodeChild in currentNode.GetChildren(1))
                {
                    if (currentNodeChild == nodeHierarchie[i - 1])
                        break;
                    yPosition += currentNodeChild.CalculateHeight();
                }
            }
            // handle nodeHierarchie[0] ?

            //Debug.Log(xPosition +" ... "+ yPosition);
            return new Vector2(xPosition, yPosition);
        }

        public int GetTotalNumberOfEntries()
        {
            return this.GetChildren(-1).Count;
        }

        internal ISATreeNode FindNode(ISANamedVisualElement o)
        {
            if (this.ecoreElement.Equals(o))
            {
                return this;
            }
            else
            {
                foreach (ISATreeNode child in children)
                {
                    ISATreeNode result = child.FindNode(o);
                    if (result != null)
                        return result;
                }
            }
            return null;
        }
    }
}
