using ISA.VR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(AttachToInteractable))]
public class UIDisposableElement : MonoBehaviour
{

    public UnityEvent OnDispose;
    public Graphic[] ElementsToHueOnHighlight;
    public float radius = 0.2f;

    private AttachToInteractable interactable;

    private float originalLocalScale;

    private bool currentlyAttached = false;
    private bool currentlyHighlighted = false;
    private bool currentlyInDisposalBin = false;
    private Dictionary<Graphic, float> originalHueMapping = new Dictionary<Graphic, float>();



    void Start()
    {
        interactable = GetComponent<AttachToInteractable>();
        interactable.NotifyOnAttach.AddListener(OnAttach);
        interactable.NotifyOnDetach.AddListener(OnDetach);
    }

    private void OnAttach()
    {
        originalLocalScale = transform.localScale.x;
        currentlyAttached = true;
    }

    private void OnDetach()
    {
        currentlyAttached = false;
        transform.localScale = Vector3.one * originalLocalScale;
        if (currentlyInDisposalBin)
        {
            restoreColor();
            Dispose();
        }
    }

    public void Dispose()
    {
        OnDispose.Invoke();
    }



    void Update()
    {
        if (!currentlyAttached)
            return;

        Vector3 elementWorldPosition = interactable.attachmentOffset.position;
        float distance = Vector3.Distance(elementWorldPosition, UIDisposalBinManager.INSTANCE.center);
        currentlyInDisposalBin = distance < radius + UIDisposalBinManager.INSTANCE.radius;

        float scale = Mathf.Min(originalLocalScale, Mathf.Max(originalLocalScale/5f, 5f * (distance - (radius + UIDisposalBinManager.INSTANCE.radius))));
        //scaleRelatively(scale, elementWorldPosition);

        if (currentlyInDisposalBin)
        {
            if(!currentlyHighlighted)
            {
                colorRed();
            }
            currentlyHighlighted = true;
        }
        else
        {
            if(currentlyHighlighted)
            {
                restoreColor();
            }
            currentlyHighlighted = false;
        }
    }

    private void scaleRelatively(float scale, Vector3 pivotPoint)
    {
        Vector3 pivotToAnchor = transform.position - pivotPoint;

        float relativeScaleFactor = scale / transform.localScale.x;
        Vector3 finalPosition = pivotPoint + pivotToAnchor * relativeScaleFactor;

        transform.position = finalPosition;
        transform.localScale = scale * Vector3.one;
    }

    private void colorRed()
    {
        foreach (Graphic graphic in ElementsToHueOnHighlight)
        {
            // save original hue
            Color.RGBToHSV(graphic.color, out float H, out float S, out float V);
            originalHueMapping.Remove(graphic);
            originalHueMapping.Add(graphic, H);

            // tint to red hue
            graphic.color = Color.HSVToRGB(5f / 360f, S, V);
        }
    }

    private void restoreColor()
    {
        foreach (Graphic graphic in ElementsToHueOnHighlight)
        {
            // get original hue
            float H = originalHueMapping[graphic];

            // restore original hue
            Color.RGBToHSV(graphic.color, out _, out float S, out float V);
            graphic.color = Color.HSVToRGB(H, S, V);
        }
    }
    
}
