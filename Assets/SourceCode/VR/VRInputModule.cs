using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR;

namespace ISA.VR
{

    public class VRInputModule : BaseInputModule
    {

        public Camera Camera;
        public SteamVR_Input_Sources TargetInputSource;
        public SteamVR_Action_Boolean ClickAction;

        private GameObject CurrentObject = null;
        private PointerEventData PointerData = null;

        protected override void Awake()
        {
            base.Awake();

            PointerData = new PointerEventData(eventSystem);
            PointerData.Reset();
            PointerData.position = new Vector2(Camera.pixelWidth / 2f, Camera.pixelHeight / 2f);
        }

        public override void Process()
        {
            try
            {
                eventSystem.RaycastAll(PointerData, m_RaycastResultCache);
            }
            catch(Exception)
            {
                return;
            }
            PointerData.pointerCurrentRaycast = FindFirstRaycast(m_RaycastResultCache);
            CurrentObject = PointerData.pointerCurrentRaycast.gameObject;

            m_RaycastResultCache.Clear();

            HandlePointerExitAndEnter(PointerData, CurrentObject);

            if (ClickAction.GetStateDown(TargetInputSource))
                ProcessPress();

            if (ClickAction.GetStateUp(TargetInputSource))
                ProcessRelease();

            ExecuteEvents.ExecuteHierarchy(PointerData.pointerDrag, PointerData, ExecuteEvents.dragHandler);
        }

        public PointerEventData GetData()
        {
            return PointerData;
        }

        private void ProcessPress()
        {
            PointerData.pointerPressRaycast = PointerData.pointerCurrentRaycast;

            // Check for object hit -> get down handler and call
            GameObject newPointerPress = ExecuteEvents.ExecuteHierarchy(CurrentObject, PointerData, ExecuteEvents.pointerDownHandler);
            if (newPointerPress == null)
                newPointerPress = ExecuteEvents.GetEventHandler<IPointerClickHandler>(CurrentObject);

            GameObject newPointerDrag = ExecuteEvents.ExecuteHierarchy(CurrentObject, PointerData, ExecuteEvents.beginDragHandler);
            if (newPointerDrag == null)
                newPointerDrag = ExecuteEvents.GetEventHandler<IDragHandler>(CurrentObject);

            PointerData.pressPosition = PointerData.position;
            PointerData.pointerPress = newPointerPress;
            PointerData.pointerDrag = newPointerDrag;
            PointerData.rawPointerPress = CurrentObject;
        }

        private void ProcessRelease()
        {
            ExecuteEvents.Execute(PointerData.pointerPress, PointerData, ExecuteEvents.pointerUpHandler);

            GameObject pointerUpHandler = ExecuteEvents.GetEventHandler<IPointerClickHandler>(CurrentObject);
            if(PointerData.pointerPress == pointerUpHandler)
                ExecuteEvents.Execute(PointerData.pointerPress, PointerData, ExecuteEvents.pointerClickHandler);
            ExecuteEvents.Execute(PointerData.pointerDrag, PointerData, ExecuteEvents.endDragHandler);

            eventSystem.SetSelectedGameObject(null);

            PointerData.pressPosition = Vector2.zero;
            PointerData.pointerPress = null;
            PointerData.pointerDrag = null;
            PointerData.rawPointerPress = null;
        }
    }

}