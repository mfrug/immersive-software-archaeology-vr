using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Valve.VR.InteractionSystem;



namespace ISA.VR
{
	[RequireComponent(typeof(Interactable))]
	public class AttachToInteractable : MonoBehaviour
	{
		public Transform attachmentOffset;

		public UnityEvent NotifyOnAttach;
		public UnityEvent NotifyOnDetach;

		private Interactable interactableComponent;



		void Start()
		{
			interactableComponent = GetComponent<Interactable>();
		}

		void Update()
		{

		}



		private void OnHandHoverBegin(Hand hand)
		{
			//hand.ShowGrabHint();
		}

		private void OnHandHoverEnd(Hand hand)
		{
			//hand.HideGrabHint();
		}



		private void HandHoverUpdate(Hand hand)
		{
			//string side = (hand.handType == SteamVR_Input_Sources.RightHand) ? "right" : "left";

			// Grabbing
			GrabTypes grabType = hand.GetGrabStarting();
			if (grabType == GrabTypes.Grip)
			//if (hand.uiInteractAction.GetStateDown(hand.handType))
			{
				//Debug.Log("GRAB STARTED!");

				if (interactableComponent.attachedToHand == null)
				{
					hand.AttachObject(gameObject, GrabTypes.Grip, Hand.defaultAttachmentFlags, attachmentOffset);
					//hand.AttachObject(gameObject, grabType); //, Hand.AttachmentFlags.DetachFromOtherHand | Hand.AttachmentFlags.SnapOnAttach);
					hand.HoverLock(interactableComponent);
				}
			}

			// Releasing
			else if (hand.IsGrabEnding(gameObject))
			//else if (hand.uiInteractAction.GetStateUp(hand.handType))
			{
				//Debug.Log("GRAB ENDED!");

				hand.DetachObject(gameObject, true);
				hand.HoverUnlock(interactableComponent);
			}
		}



		private void OnAttachedToHand(Hand hand)
		{
			//Debug.Log("OnAttachedToHand");
			NotifyOnAttach.Invoke();
		}

		private void OnDetachedFromHand(Hand hand)
		{
			//Debug.Log("OnDetachedFromHand");
			NotifyOnDetach.Invoke();
		}
	}

}