using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR.InteractionSystem;

namespace ISA.VR
{

    public class Pointer : MonoBehaviour
    {
        public LayerMask IgnoredLayers;

        public float RayLength = 5f;
        public float FingerTipHoverDistance = 0.1f;

        public GameObject RayCastCrossObject;
        private bool showCross;

        public VRInputModule InputModule;

        private LineRenderer lineRenderer;

        private GameObject lastHitObject = null;



        void Awake()
        {
            lineRenderer = GetComponent<LineRenderer>();
            GetComponent<Camera>().farClipPlane = RayLength;
            showCross = RayCastCrossObject != null;
        }

        public void Enable()
        {
            this.enabled = true;
            lineRenderer.enabled = true;
            RayCastCrossObject.SetActive(true);
            InputModule.Camera.enabled = true;
        }

        public void Disable()
        {
            this.enabled = false;
            lineRenderer.enabled = false;
            RayCastCrossObject.SetActive(false);
            InputModule.Camera.enabled = false;
        }





        void Update()
        {
            Hand rightHand = Player.instance.rightHand;
            if (!rightHand.isActive || !rightHand.isPoseValid)
            {
                if (showCross)
                    RayCastCrossObject.SetActive(false);
                return;
            }

            //Vector3 fingerTipPosition = Player.instance.rightHand.mainRenderModel.GetBonePosition((int)Player.instance.rightHand.fingerJointHover);
            Vector3 fingerTipPosition = transform.position;
            Collider sphereCollider = PerformFingerTipSphereCast(fingerTipPosition, RayLength);
            if (sphereCollider != null)
            {
                Vector3 directionToColliderSurface = sphereCollider.ClosestPoint(fingerTipPosition) - fingerTipPosition;
                if(directionToColliderSurface.magnitude > 0.01f)
                    transform.rotation = Quaternion.LookRotation(directionToColliderSurface, Vector3.up);
            }
            else
            {
                transform.localRotation = Quaternion.identity;
            }

            PointerEventData data = InputModule.GetData();
            float rayLength = data.pointerCurrentRaycast.distance;
            if (rayLength == 0)
                rayLength = RayLength;


            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hit;
            Physics.Raycast(ray, out hit, rayLength, ~IgnoredLayers);
            Vector3 hitPosition;

            if (hit.collider != null)
            {
                hitPosition = hit.point - transform.forward * 0.0001f;

                if (showCross)
                {
                    RayCastCrossObject.SetActive(true);
                    RayCastCrossObject.transform.rotation = Quaternion.LookRotation(hit.normal);// * Quaternion.AngleAxis(Time.time * 100, hit.normal);
                }

                GameObject currentHitObject = hit.collider.gameObject;

                if (!currentHitObject.Equals(lastHitObject))
                {
                    // hovering
                    TriggerHoverEnd(lastHitObject);
                    TriggerHoverStart(currentHitObject);
                    lastHitObject = currentHitObject;
                }

                if (InputModule.ClickAction.GetStateUp(InputModule.TargetInputSource))
                {
                    // Released
                    TriggerReleased(currentHitObject);
                }
                if (InputModule.ClickAction.GetStateDown(InputModule.TargetInputSource))
                {
                    // Pressed
                    TriggerPressed(currentHitObject);
                }
            }
            else
            {
                hitPosition = transform.position + transform.forward * rayLength;
                if (showCross)
                {
                    if (rayLength >= RayLength)
                    {
                        RayCastCrossObject.SetActive(false);
                        RayCastCrossObject.transform.localRotation = Quaternion.identity;
                    }
                    else
                    {
                        RayCastCrossObject.SetActive(true);
                        RayCastCrossObject.transform.rotation = Quaternion.LookRotation(data.pointerCurrentRaycast.worldNormal);
                    }
                }

                TriggerHoverEnd(lastHitObject);
                lastHitObject = null;
            }

            if (showCross)
            {
                RayCastCrossObject.transform.position = hitPosition;
                RayCastCrossObject.transform.localScale = Vector3.one * (0.5f + Vector3.Distance(transform.position, hitPosition) * 0.6f);
            }

            lineRenderer.SetPosition(0, ray.origin);
            lineRenderer.SetPosition(1, Vector3.Lerp(ray.origin, hitPosition, 0.7f));
            lineRenderer.SetPosition(2, hitPosition);
        }

        private Collider PerformFingerTipSphereCast(Vector3 position, float maxLength)
        {
            if (Player.instance.rightHand.mainRenderModel == null)
                return null;

            Collider bestCollider = null;
            Vector3 bestDirectionToColliderSurface = Vector3.positiveInfinity;

            foreach (Collider collider in Physics.OverlapSphere(position, Mathf.Min(maxLength, FingerTipHoverDistance), ~IgnoredLayers))
            {
                Vector3 directionToColliderSurface = collider.ClosestPoint(position) - position;

                float distance = directionToColliderSurface.magnitude;
                if (bestCollider == null || distance < bestDirectionToColliderSurface.magnitude)
                {
                    bestCollider = collider;
                    bestDirectionToColliderSurface = directionToColliderSurface;
                }
            }
            /*{
                if (bestCollider == null)
                    return null;

                Ray ray = new Ray(transform.position, bestDirectionToColliderSurface);
                RaycastHit hit;
                Physics.Raycast(ray, out hit, RayLength, ~IgnoredLayers);

                if (hit.normal == null)
                    return null;

                float angle = Vector3.Angle(ray.direction, hit.normal);
                if (angle < 180 - 0.2f || angle > 180 + 0.2f)
                    return null;
            }*/

            return bestCollider;
        }



        private void TriggerPressed(GameObject obj)
        {
            ISAUIInteractable interactable = obj.GetComponent<ISAUIInteractable>();
            if (interactable != null)
                interactable.onTriggerPressed.Invoke();
        }

        private void TriggerReleased(GameObject obj)
        {
            ISAUIInteractable interactable = obj.GetComponent<ISAUIInteractable>();
            if (interactable != null)
                interactable.onTriggerReleased.Invoke();
        }



        private void TriggerHoverStart(GameObject obj)
        {
            ISAUIInteractable interactable = obj.GetComponent<ISAUIInteractable>();
            if (interactable != null)
                interactable.onHoverStart.Invoke();
        }

        private void TriggerHoverEnd(GameObject obj)
        {
            if (obj != null)
            {
                ISAUIInteractable lastInteractable = obj.GetComponent<ISAUIInteractable>();
                if (lastInteractable != null)
                    lastInteractable.onHoverEnd.Invoke();
            }
        }



        public void ResetHovering()
        {
            lastHitObject = null;
        }
    }

}