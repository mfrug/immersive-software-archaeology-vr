﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading;

namespace ISA.Modelling
{
    class ModelLoader
    {

        public static readonly ModelLoader instance = new ModelLoader();

        private ModelLoader() { }





        public ModelStorage ModelStorage { get; private set; }
        public ISASoftwareSystemQualityModel QualityModel{ get; private set; }
        public ISAVisualizationModel VisualizationModel { get; private set; }

        readonly HttpClient client = new HttpClient();



        public async Task TryToLoadModelInformationFromEclipse(Action CallBackSuccess, Action<string> CallBackFailure)
        {
            string modelStorageMetaDataModelLocation;
            try
            {
                modelStorageMetaDataModelLocation = await client.GetStringAsync("http://localhost:9005/models");
                Debug.Log("Model Storage Meta Data File Location: " + modelStorageMetaDataModelLocation);
            }
            catch (HttpRequestException e)
            {
                Debug.LogWarning(e.Message);
                CallBackFailure("Cannot load systems: the ISA Coalescence Server is not reachable. Please launch the server from within Eclipse");
                return;
            }

            ModelStorage = ParseXMLFileToObject<ModelStorage>(modelStorageMetaDataModelLocation);
            if(ModelStorage == null)
            {
                CallBackFailure("The ISA Eclipse plugin does not hold any analyzed software visualization models, yet!");
                return;
            }

            CallBackSuccess();
        }





        public void LoadQualityModel(string fullFilePath)
        {
            this.QualityModel = ParseXMLFileToObject<ISASoftwareSystemQualityModel>(fullFilePath);
        }

        public void LoadVisualizationModel(string fullFilePath)
        {
            /*
            Uri uri = new Uri(path);
            ResourceSet resourceSet = new ResourceSet();
            Resource resource = resourceSet.CreateResource(uri);
            EPackage root = resource.Load(null);
            */

            //fullFilePath = "C:\\Eclipse\\2020-09\\workspaces\\ISA-runtime\\.metadata\\.plugins\\dk.itu.cs.isa.storage\\models\\123.xml";
            this.VisualizationModel = ParseXMLFileToObject<ISAVisualizationModel>(fullFilePath);

            /*ThreadStart childref = new ThreadStart(delegate { InternalLoadVisualizationModel(fullFilePath, callback); });
            new Thread(childref).Start();*/
        }

        /*private static void InternalLoadVisualizationModel(string fullFilePath, Action<VisualizationModel> callback)
        {
            VisualizationModel model = ParseXMLFileToObject<VisualizationModel>(fullFilePath);
            callback(model);
        }*/

        private static T ParseXMLFileToObject<T>(string XmlFilename)
        {
            T returnObject = default;

            if (string.IsNullOrEmpty(XmlFilename))
                return returnObject;

            try
            {
                StreamReader xmlStream = new StreamReader(XmlFilename);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                returnObject = (T)serializer.Deserialize(xmlStream);
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
            }
            return returnObject;
        }
    }
}