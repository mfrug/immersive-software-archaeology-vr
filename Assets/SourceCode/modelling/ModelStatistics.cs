using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace ISA.Modelling
{
    public class ModelStatistics
    {
        public static float maxPlanetRadius { private set; get; }
        public static float maxPlanetConnectionWeight { private set; get; }


        public static float maxCitySize { private set; get; }
        public static float maxCityConnectionWeight { private set; get; }


        public static float maxBuildingConnectionWeight { private set; get; }
        public static float maxBuildingFloorHeight { get; private set; }
        public static float maxBuildingFloorDiameter { get; private set; }





        public static IEnumerator Initialize(ISAVisualizationModel model)
        {
            if (model.solarSystems.Length != 1)
                throw new Exception("There should be exactly one sun system in the model!");
            ISASolarSystem sunSystem = model.solarSystems[0];
            foreach (ISAPlanetConnection connection in sunSystem.planetConnections ?? Enumerable.Empty<ISAPlanetConnection>())
            {
                if ((float)connection.weight > maxPlanetConnectionWeight)
                    maxPlanetConnectionWeight = (float)connection.weight;
            }

            foreach (ISAPlanet planet in sunSystem.planets ?? Enumerable.Empty<ISAPlanet>())
            {
                if ((float)planet.radius > maxPlanetRadius)
                    maxPlanetRadius = (float)planet.radius;

                foreach (ISACityConnection connection in planet.cityConnections ?? Enumerable.Empty<ISACityConnection>())
                {
                    if ((float)connection.weight > maxCityConnectionWeight)
                        maxCityConnectionWeight = (float)connection.weight;
                }

                foreach (ISAContinent continent in planet.continents ?? Enumerable.Empty<ISAContinent>())
                {
                    analyzeContinent(continent);
                }
            }

            yield return null;
        }

        private static void analyzeContinent(ISAContinent continent)
        {
            if (continent is ISACompoundContinent)
            {
                foreach (ISAContinent subContinent in ((ISACompoundContinent)continent).subContinents ?? Enumerable.Empty<ISAContinent>())
                {
                    analyzeContinent(subContinent);
                }
            }
            else if (continent is ISACityContinent)
            {
                ISACity city = ((ISACityContinent)continent).city;
                if (city.buildings.Length > maxCitySize)
                    maxCitySize = city.buildings.Length;

                foreach (ISABuildingConnection connection in city.buildingConnections ?? Enumerable.Empty<ISABuildingConnection>())
                {
                    if ((float)connection.weight > maxBuildingConnectionWeight)
                        maxBuildingConnectionWeight = (float)connection.weight;
                }

                foreach (ISABuilding building in city.buildings ?? Enumerable.Empty<ISABuilding>())
                {
                    foreach (ISAFloor floor in building.floors ?? Enumerable.Empty<ISAFloor>())
                    {
                        if ((float)floor.height > maxBuildingFloorHeight)
                            maxBuildingFloorHeight = (float)floor.height;
                        if ((float)floor.diameter > maxBuildingFloorDiameter)
                            maxBuildingFloorDiameter = (float)floor.diameter;
                    }
                }
            }
        }
    }

}