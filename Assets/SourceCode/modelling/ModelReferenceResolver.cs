using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ISA.Modelling
{

    public class ModelReferenceResolver
    {
        private ISAVisualizationModel model;

        public ModelReferenceResolver(ISAVisualizationModel model)
        {
            this.model = model;
        }

        public ISAPlanet resolvePlanetFromEcoreReferenceString(string ecoreReference)
        {
            // example: @sunSystems.0
            string sunSystemRef = null;
            // example: @planets.23
            string planetRef = null;
            foreach (string segment in ecoreReference.Split('/'))
            {
                if(segment.StartsWith("@"))
                {
                    if (sunSystemRef == null)
                        sunSystemRef = segment.Trim();
                    else if (planetRef == null)
                        planetRef = segment.Trim();
                    else
                        throw new System.Exception("Unexpected behavior in planet reference resolving!");
                }
            }

            int sunSystemIndex = int.Parse(sunSystemRef.Split('.')[1]);
            int planetIndex = int.Parse(planetRef.Split('.')[1]);

            ISASolarSystem sunSystem = model.solarSystems[sunSystemIndex];
            return sunSystem.planets[planetIndex];
        }
    }

}
