using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelsForGlory.VoronoiDiagram;
using System;
using ISA.util;
using ISA.SolarOverview;
using ISA.Modelling;

namespace ISA.CityGeneration
{
    public class BuildingFloor3D
    {

        private static readonly Material ConstructionSiteFacadeMaterial = CityGenerator.InstantiateMaterial("City/BuildingFacade/ConstructionSiteFacadeMaterial");
        private static readonly Material BuildingFacadeMaterial = CityGenerator.InstantiateMaterial("City/BuildingFacade/BuildingFacadeMaterial");
        private static readonly Material BuildingRoofMaterial = CityGenerator.InstantiateMaterial("City/BuildingFacade/BuildingRoofMaterial");

        public BuildingInCity3D ParentBuilding { get; private set; }
        public float Index { get; internal set; }



        public string Name { get; internal set; }
        public float Diameter { get; internal set; }
        public float Height { get; internal set; }
        public float yOffset { get; internal set; }



        private GameObject gameObject;

        private ISAFloor ecoreModel;



        public BuildingFloor3D(BuildingInCity3D Parent, ISAFloor floorEcoreModel, int index)
        {
            ecoreModel = floorEcoreModel;
            ParentBuilding = Parent;
            Index = index;

            if (ecoreModel == null)
            {
                // ground floor
                Name = Parent.ecoreModel.name;
                Height = 0.05f;
            }
            else
            {
                Name = ecoreModel.name;

                //Diameter = (float)ecoreModel.diameter / ModelStatistics.maxBuildingFloorDiameter;
                //Diameter = 1f + Diameter * 10f;

                //Height = ((float)ecoreModel.height / ModelStatistics.maxBuildingFloorHeight);
                //Height = 0.05f + Height * 0.95f; // RandomFactory.INSTANCE.getRandomFloat(0.05f, 0.2f);

                Diameter = ecoreModel.diameter/2;
                Height = ecoreModel.height / 40f;
            }
        }



        public void SetupDiageticUI()
        {
            ISAUIInteractable interactable = gameObject.AddComponent<ISAUIInteractable>();
            interactable.onHoverStart.AddListener(delegate { ISAUIInteractionSynchronizer.INSTANCE.NotifyHoverStart(ParentBuilding.ecoreModel); });
            interactable.onHoverEnd.AddListener(delegate { ISAUIInteractionSynchronizer.INSTANCE.NotifyHoverEnd(ParentBuilding.ecoreModel); });
            interactable.onTriggerPressed.AddListener(delegate {
                if (!ISAUIInteractionSynchronizer.INSTANCE.isActive(ParentBuilding.ecoreModel))
                    ISAUIInteractionSynchronizer.INSTANCE.NotifyToggleActive(ParentBuilding.ecoreModel);
            });

            //ISAUIInteractionSynchronizer.INSTANCE.RegisterListener(ParentBuilding.ecoreModel, ParentBuilding);
            //ISAUIInteractionSynchronizer.INSTANCE.RegisterListener(ParentBuilding.ecoreModel, ParentBuilding.infoCanvasManager);
        }



        public void GenerateMesh()
        {
            gameObject = new GameObject("Floor #" + Index + ": " + Name);
            gameObject.transform.parent = ParentBuilding.gameObject.transform;
            gameObject.transform.localPosition = Vector3.up * (yOffset);

            Mesh mesh = BuildingMeshGenerator.GenerateMeshForVoronoiSite(ParentBuilding.AssignedGeneratedVoronoiSite, Height, 10f);
            gameObject.AddComponent<MeshFilter>().mesh = mesh;
            gameObject.AddComponent<MeshCollider>().convex = true;

            float scale = Diameter * CityGenerator.Convert01LengthToWorldSpace(CityGenerator.MinDistanceBetweenVoronoiPoints) * (2f / (mesh.bounds.size.x + mesh.bounds.size.z));
            BuildingMeshGenerator.ResizeUVs(mesh, scale);
            gameObject.transform.localScale = scale * new Vector3(1, 0, 1) + Vector3.up;

            if (Index >= 0)
                gameObject.transform.rotation = Quaternion.Euler(0, RandomFactory.INSTANCE.getRandomFloat(360f), 0);

            if (Index == -1)
            {
                applyMaterialConcrete(false);
                addGroundFloorProps();
            }
            else
            {
                if (ecoreModel.constructionSite)
                {
                    applyMaterialConstructionSite();
                }
                else
                {
                    if (ecoreModel.windowFloor)
                    {
                        applyMaterialConcrete(false);
                        addLargeWindows();
                    }
                    else
                    {
                        applyMaterialConcrete(false);
                    }

                    if (ecoreModel.antennaFloor)
                    {
                        addSatteliteDishes();
                    }
                }
            }

            SetupDiageticUI();
        }

        private void applyMaterialConstructionSite()
        {
            gameObject.name += " [construction site]";

            Material[] materials = new Material[3];
            materials[0] = ConstructionSiteFacadeMaterial;
            materials[0].SetColor("StripeColor", ParentBuilding.Color);
            materials[0].SetInt("ShowStripes", 0);
            materials[1] = BuildingRoofMaterial;
            materials[2] = BuildingRoofMaterial;
            gameObject.AddComponent<MeshRenderer>().materials = materials;
        }

        private void applyMaterialConcrete(bool showStripes)
        {
            //Material mat2 = Resources.Load("City/DefaultBuildingWall", typeof(Material)) as Material;
            Material[] materials = new Material[3];
            materials[0] = BuildingFacadeMaterial;
            materials[0].SetColor("StripeColor", ParentBuilding.Color);
            materials[0].SetInt("ShowStripes", 0);
            materials[1] = BuildingRoofMaterial;
            materials[2] = BuildingRoofMaterial;
            gameObject.AddComponent<MeshRenderer>().materials = materials;

            if (showStripes)
                materials[0].SetInt("ShowStripes", 1);
        }

        private void addGroundFloorProps()
        {
            gameObject.name += " [ground floor]";

            ObjectPlacementHelper.PlaceObjectsOnBuildingFacadeSetting settings = ObjectPlacementHelper.GetDefaultPlaceObjectsOnBuildingFacadeSetting();
            settings.floorObject = gameObject;
            settings.site = ParentBuilding.AssignedGeneratedVoronoiSite;
            settings.floorHeight = Height;
            settings.pathToPrefab = "City/Windows/Door";
            settings.prefabSize = 0.01f;
            settings.minimalFloorWidth = 0.05f;
            settings.relativePosition = new Vector2(0.5f, 0f);
            settings.absolutePosition = new Vector2(0f, 0.015f);
            /*settings.spaceBetweenPrefabsInArray = 2*.02f;
            settings.randomPlacementQuotaInArray = 1f;*/
            ObjectPlacementHelper.PlaceSingleObjectsOnBuildingFacade(settings);
        }

        private void addLargeWindows()
        {
            gameObject.name += " [large windows]";

            ObjectPlacementHelper.PlaceObjectsOnBuildingFacadeSetting settings = ObjectPlacementHelper.GetDefaultPlaceObjectsOnBuildingFacadeSetting();
            settings.floorObject = gameObject;
            settings.site = ParentBuilding.AssignedGeneratedVoronoiSite;
            settings.floorHeight = Height;
            settings.pathToPrefab = "City/Windows/LargePanoramaWindow";
            settings.prefabSize = 0.01f;
            settings.minimalFloorWidth = 0.01f;
            settings.relativePosition = new Vector2(0.5f, 0f);
            settings.spaceBetweenPrefabsInArray = .02f;
            settings.randomPlacementQuotaInArray = 1f;

            float spaceBetweenRows = 0.03f;
            int numberOfFittingRows = (int)Mathf.Floor((Height - 0.02f - spaceBetweenRows / 2f) / spaceBetweenRows);
            for (int i = 0; i <= numberOfFittingRows; i++)
            {
                settings.absolutePosition = new Vector2(0.5f, .02f + spaceBetweenRows * i);
                ObjectPlacementHelper.PlaceArrayOfObjectsOnBuildingFacade(settings);
            }
        }

        private void addSatteliteDishes()
        {
            gameObject.name += " [satellite dishes]";

            ObjectPlacementHelper.PlaceObjectsOnBuildingFacadeSetting settings = ObjectPlacementHelper.GetDefaultPlaceObjectsOnBuildingFacadeSetting();
            settings.floorObject = gameObject;
            settings.site = ParentBuilding.AssignedGeneratedVoronoiSite;
            settings.floorHeight = Height;
            settings.pathToPrefab = "City/SatelliteDish/SatelliteDish";
            settings.prefabSize = 0.01f;
            settings.minimalFloorWidth = 0.032f;
            settings.relativePosition = new Vector2(1f, 1f);
            //settings.absolutePosition = new Vector2(-.015f, -.015f);
            settings.absolutePosition = new Vector2(0f, -.015f);
            ObjectPlacementHelper.PlaceSingleObjectsOnBuildingFacade(settings);
        }

        private void addSmallWindowsAndBalcony()
        {
            gameObject.name += " [balcony]";

            ObjectPlacementHelper.PlaceObjectsOnBuildingFacadeSetting settings = ObjectPlacementHelper.GetDefaultPlaceObjectsOnBuildingFacadeSetting();
            settings.floorObject = gameObject;
            settings.site = ParentBuilding.AssignedGeneratedVoronoiSite;
            settings.floorHeight = Height;
            settings.pathToPrefab = "City/Windows/Balcony";
            settings.prefabSize = 0.01f;
            settings.minimalFloorWidth = 0.032f;
            settings.relativePosition = new Vector2(0.5f, 0f);
            settings.absolutePosition = new Vector2(0.5f, .02f);
            settings.spaceBetweenPrefabsInArray = .04f;
            settings.randomPlacementQuotaInArray = 0.9f;
            ObjectPlacementHelper.PlaceArrayOfObjectsOnBuildingFacade(settings);

            settings.pathToPrefab = "City/Windows/SmallWindow";
            settings.prefabSize = 0.01f;
            settings.minimalFloorWidth = 0.01f;
            settings.relativePosition = new Vector2(0.5f, 0f);
            settings.spaceBetweenPrefabsInArray = .015f;
            settings.randomPlacementQuotaInArray = 0.7f;

            float spaceBetweenRows = 0.03f;
            int numberOfFittingRows = (int)Mathf.Floor((Height - 0.02f - spaceBetweenRows / 2f) / spaceBetweenRows);
            for (int i = 1; i <= numberOfFittingRows; i++)
            {
                settings.absolutePosition = new Vector2(0.5f, .02f + spaceBetweenRows * i);
                ObjectPlacementHelper.PlaceArrayOfObjectsOnBuildingFacade(settings);
            }
        }
    }
}
