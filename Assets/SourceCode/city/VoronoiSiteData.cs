﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using PixelsForGlory.VoronoiDiagram;

namespace ISA.CityGeneration
{
    public struct VoronoiSiteData
    {

        public enum Type
        {
            Building, // Center of a building
            Garden, // a garden that belongs to a building, should be next to a complex
            Free // unoccupied space that got randomly filled
        }



        public Type type { get; private set; }
        public BuildingInCity3D building { get; private set; }
        //public List<VoronoiDiagramGeneratedSite<VoronoiSiteData>> otherSitesConnectedByStreet { get; private set; }
        public List<StreetSegment> connectedStreetSegments { get; private set; }



        public VoronoiSiteData(BuildingInCity3D building, Type type)
        {
            this.building = building;
            this.type = type;
            //this.otherSitesConnectedByStreet = new List<VoronoiDiagramGeneratedSite<VoronoiSiteData>>();
            this.connectedStreetSegments = new List<StreetSegment>();
        }
    }
}