﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelsForGlory.VoronoiDiagram;
using System;
using Valve.VR.InteractionSystem;
using ISA.util;

namespace ISA.CityGeneration
{
    public class CityGenerator : MonoBehaviour
    {
        internal static float MinDistanceBetweenVoronoiPoints;

        internal static readonly int CITY_DIMENSIONS = 5;
        internal static readonly int VORONOI_DIMENSIONS = 50000;

        public City3D city { get; private set; }



        internal bool IsBuildingAt(Vector3 position, float maxDistance)
        {
            foreach(BuildingInCity3D building in city.buildings)
                if (Vector3.Distance(ConvertVoronoiSpaceToWorldSpace3D(building.AssignedGeneratedVoronoiSite.Centroid), position) < maxDistance)
                    return true;
            return false;
        }

        public IEnumerator doStart(Transform parentToPlaceIn, float scale)
        {
            city = new City3D(SceneHandler.INSTANCE.VisitedCityOriginalData.cityEcoreModel);
            city.SetupDataModels(false);

            int width = VORONOI_DIMENSIONS;
            int height = VORONOI_DIMENSIONS;
            VoronoiDiagram<VoronoiSiteData> voronoiDiagram = new VoronoiDiagram<VoronoiSiteData>(new Rect(0f, 0f, width, height));
            yield return null;

            List<VoronoiDiagramSite<VoronoiSiteData>> voronoiPoints = city.CalculateVoronoiPoints();
            //DebugVisually(voronoiPoints);
            voronoiDiagram.AddSites(voronoiPoints);
            yield return null;

            voronoiDiagram.GenerateSites(1);
            yield return null;

            /*
            // Saving image to file
            // DON'T USE! Image will be too large to save!!!!!!!!!!!

            var outImg = new Texture2D(width, height);
            outImg.SetPixels(voronoiDiagram.Get1DSampleArray());
            outImg.Apply();

            System.IO.File.WriteAllBytes("diagram.png", outImg.EncodeToPNG());
            */

            assignVoronoiSitesToBuildings(voronoiDiagram, false);
            yield return null;

            //CitySceneManager.INSTANCE.UpdateInfoStatus("generating street layout", 0);
            yield return StartCoroutine(city.GenerateAndUpdateStreetSegments(voronoiDiagram));

            GameObject streetParentObject = new GameObject("Streets Parent");
            streetParentObject.transform.parent = transform;
            for (int i = 0; i < city.streets.Count; i++)
            {
                //CitySceneManager.INSTANCE.UpdateInfoStatus("generating street meshes", (float)i / city.streets.Count);
                GameObject streetObject = new GameObject("Street #" + (i + 1));
                streetObject.transform.parent = streetParentObject.transform;
                if (!city.streets[i].GenerateMesh(streetObject, (1f - (float)i / city.streets.Count) / 1000f))
                {
                    // The generation did not result in any new street structures.
                    // Remove this container object to not fill the object hierarchy with useless empty objects.
                    Destroy(streetObject);
                }
                else
                {
                    streetObject.transform.localPosition = Vector3.zero;
                    streetObject.transform.localRotation = Quaternion.identity;
                }
            }
            streetParentObject.transform.localPosition = Vector3.zero;
            streetParentObject.transform.localRotation = Quaternion.identity;

            yield return StartCoroutine(city.GenerateBuildingMeshes(this, gameObject));

            //CreateReflectionProbes(new Vector2(CITY_DIMENSIONS, CITY_DIMENSIONS), 5);

            //CitySceneManager.INSTANCE.HideInfoStatus();

            transform.parent = parentToPlaceIn.transform;
            transform.localPosition = new Vector3(-0.5f * scale * CITY_DIMENSIONS, 0f, -0.5f * scale * CITY_DIMENSIONS);
            transform.localScale = scale * Vector3.one;
            transform.localRotation = Quaternion.identity;

            Debug.Log("FINISHED WITH GENERATING THE CITY");
        }

        private void CreateReflectionProbes(Vector2 space, int resolution)
        {
            GameObject probesParentGameObject = new GameObject("Reflection Probes");
            probesParentGameObject.transform.parent = gameObject.transform;

            for (int x = 0; x < resolution; x++)
            {
                for (int z = 0; z < resolution; z++)
                {
                    Vector3 position = new Vector3(((float)x / resolution) * space.x, 0.5f, ((float)z / resolution) * space.y);
                    if (!IsBuildingAt(position, 2f))
                        continue;

                    string name = "Reflection Probe [" + ((float)x / resolution) * space.x + "," + ((float)z / resolution) * space.y + "]";
                    Vector3 probeSize = new Vector3((1f / resolution) * space.x, 10, (1f / resolution) * space.y);
                    ReflectionProbeGenerator.GenerateReflectionProbe(probesParentGameObject.gameObject.transform, position, probeSize, name, 256, true);
                }
            }
        }



        private void assignVoronoiSitesToBuildings(VoronoiDiagram<VoronoiSiteData> voronoiDiagram, bool visualDebug)
        {
            // Assign each voronoi site (the tiles) to a building and also keep track of
            // to what part the site belongs (i.e., center, building complex, or garden).
            foreach (KeyValuePair<int, VoronoiDiagramGeneratedSite<VoronoiSiteData>> site in voronoiDiagram.GeneratedSites)
            {
                if(visualDebug)
                    visuallyDebugSite(voronoiDiagram, site.Value);

                BuildingInCity3D building = site.Value.SiteData.building;
                switch (site.Value.SiteData.type)
                {
                    case VoronoiSiteData.Type.Building:
                        building.AssignedGeneratedVoronoiSite = site.Value;
                        if (visualDebug)
                        {
                            Vector3 pos = ConvertVoronoiSpaceToWorldSpace3D(site.Value.Centroid);
                            Debug.DrawLine(pos, pos + Vector3.up * 0.5f, building.Color, float.MaxValue);
                        }
                        break;
                    case VoronoiSiteData.Type.Garden:
                        building.SurroundingVoronoiSites.Add(site.Value);
                        break;
                }
            }
        }




        private void DebugVisually(List<VoronoiDiagramSite<VoronoiSiteData>> voronoiPoints)
        {
            foreach (VoronoiDiagramSite<VoronoiSiteData> site in voronoiPoints)
            {
                Color color;
                if (site.SiteData.type == VoronoiSiteData.Type.Building)
                    color = Color.blue;
                else if (site.SiteData.type == VoronoiSiteData.Type.Garden)
                    color = Color.green;
                else
                    color = Color.gray;

                Vector3 startPoint = ConvertVoronoiSpaceToWorldSpace3D(site.Coordinate);
                Vector3 endPoint = startPoint + Vector3.up / 3f;
                Debug.DrawLine(startPoint, endPoint, color, float.MaxValue);

                //Debug.Log("Voronoi point");
                //Debug.Log("\tVoronoi Space:\t" + "x = " + site.Coordinate.x + "y = " + site.Coordinate.y);
                //Debug.Log("\tWorld Space:\t" + "x = " + startPoint.x + "y = " + startPoint.y);
            }
        }

        private void visuallyDebugSite(VoronoiDiagram<VoronoiSiteData> voronoiDiagram, VoronoiDiagramGeneratedSite<VoronoiSiteData> site)
        {
            Vector3 startPoint = ConvertVoronoiSpaceToWorldSpace3D(site.Centroid);
            Vector3 endPoint = startPoint + Vector3.up / 10f;
            Debug.DrawLine(startPoint, endPoint, Color.HSVToRGB(0, 0f, 0.25f), float.MaxValue);

            foreach (VoronoiDiagramGeneratedEdge edge in site.Edges)
            {
                startPoint = ConvertVoronoiSpaceToWorldSpace3D(edge.LeftEndPoint);
                endPoint = ConvertVoronoiSpaceToWorldSpace3D(edge.RightEndPoint);

                Color color = Color.HSVToRGB(0, 0, 0.25f);
                /*if (site.SiteData.type == VoronoiSiteData.Type.Garden)
                    color = Color.green;*/
                Debug.DrawLine(startPoint, endPoint, color, float.MaxValue);
            }

            foreach (int neighborIndex in site.NeighborSites)
            {
                VoronoiDiagramGeneratedSite<VoronoiSiteData> neighbor = voronoiDiagram.GeneratedSites[neighborIndex];
                if (neighbor.IsEdge)
                    continue;

                startPoint = ConvertVoronoiSpaceToWorldSpace3D(site.Centroid);
                endPoint = ConvertVoronoiSpaceToWorldSpace3D(neighbor.Centroid);
                //Debug.DrawLine(startPoint, endPoint, Color.HSVToRGB(0, 0, 0.1f), float.MaxValue);
            }
        }

        public static float Convert01LengthToWorldSpace(float length01)
        {
            return length01 * (float)CITY_DIMENSIONS;
        }

        public static float ConvertVoronoiLengthToWorldSpace(float voronoiLength)
        {
            return voronoiLength * ((float)CITY_DIMENSIONS / (float)VORONOI_DIMENSIONS);
        }

        public static Vector2 ConvertVoronoiSpaceToWorldSpace2D(Vector2 voronoiPos)
        {
            return new Vector2(voronoiPos.x, voronoiPos.y) * ((float)CITY_DIMENSIONS / (float)VORONOI_DIMENSIONS); // - new Vector3((float)cityDimensions / 2, 0, (float)cityDimensions / 2);
        }

        public static Vector3 ConvertVoronoiSpaceToWorldSpace3D(Vector2 voronoiPos)
        {
            Vector2 result = ConvertVoronoiSpaceToWorldSpace2D(voronoiPos);
            return new Vector3(result.x, 0, result.y);
        }



        internal static GameObject InstantiateGameObject(string pathToResource)
        {
            return Instantiate(Resources.Load(pathToResource) as GameObject);
        }

        internal static Material InstantiateMaterial(string pathToResource)
        {
            return Instantiate(Resources.Load(pathToResource) as Material);
        }
    }
}