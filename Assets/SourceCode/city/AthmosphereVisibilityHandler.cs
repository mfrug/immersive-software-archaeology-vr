using AfGD;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AthmosphereVisibilityHandler : MonoBehaviour
{
    public float transitionZoneSize = 0.1f;

    public MeshRenderer innerAtmosphereRenderer;
    private Color innerAtmosphereOriginalColor;

    public MeshRenderer outerAtmosphereRenderer;
    private Color outerAtmosphereOriginalColor;


    void Start()
    {
        innerAtmosphereOriginalColor = innerAtmosphereRenderer.material.GetColor("_BaseColor");
        outerAtmosphereOriginalColor = outerAtmosphereRenderer.material.GetColor("BaseColor");
    }

    public void UpdateShaders(bool comingFromOutside)
    {
        float playerDistanceFromCenter = (SceneHandler.INSTANCE.VRHeadCollider.transform.position - transform.position).magnitude;
        float radius = transform.lossyScale.x / 2;

        float t01;
        if (comingFromOutside)
            t01 = 1f - (playerDistanceFromCenter - radius) / transitionZoneSize / radius;
        else
            t01 = (radius - playerDistanceFromCenter) / transitionZoneSize / radius;
        t01 = Mathf.Min(1, Mathf.Max(0, t01));
        float eased_t01 = EasingFunctions.Crossfade(EasingFunctions.SmoothStart2, EasingFunctions.SmoothStop2, 0.5f, t01);

        Color innerAtmosphereDynamicColor = innerAtmosphereOriginalColor;
        innerAtmosphereDynamicColor.a = eased_t01;
        innerAtmosphereRenderer.material.SetColor("_BaseColor", innerAtmosphereDynamicColor);

        Color outerAtmosphereDynamicColor = outerAtmosphereOriginalColor;
        outerAtmosphereDynamicColor.a = 1f - eased_t01;
        outerAtmosphereRenderer.material.SetColor("BaseColor", outerAtmosphereDynamicColor);
    }

}
