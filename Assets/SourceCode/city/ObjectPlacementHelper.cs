using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ISA.util;
using PixelsForGlory.VoronoiDiagram;

namespace ISA.CityGeneration
{
    public class ObjectPlacementHelper
    {

        internal struct PlaceObjectsOnBuildingFacadeSetting
        {
            public GameObject floorObject;
            public VoronoiDiagramGeneratedSite<VoronoiSiteData> site;

            public string pathToPrefab;

            public float minimalFloorWidth;
            public float floorHeight;
            public float prefabSize;

            public Vector2 relativePosition;
            public Vector2 absolutePosition;

            public float spaceBetweenPrefabsInArray;
            public float randomPlacementQuotaInArray;
        }

        internal static PlaceObjectsOnBuildingFacadeSetting GetDefaultPlaceObjectsOnBuildingFacadeSetting()
        {
            return new PlaceObjectsOnBuildingFacadeSetting
            {
                floorObject = null,
                site = null,

                pathToPrefab = "",

                minimalFloorWidth = 0f,
                floorHeight = 0.1f,
                prefabSize = 0.01f,

                relativePosition = Vector2.zero,
                absolutePosition = Vector2.zero,

                spaceBetweenPrefabsInArray = 0.05f,
                randomPlacementQuotaInArray = 0.5f
            };
        }



        internal static List<GameObject> PlaceArrayOfObjectsOnBuildingFacade(PlaceObjectsOnBuildingFacadeSetting settings)
        {
            RandomFactory rand = new RandomFactory(RandomFactory.INSTANCE.getRandomInt(int.MaxValue));
            int antiLoopStuckVariable = 0;

            List<GameObject> generatedObjects = new List<GameObject>();
            for (int v = 0; v < settings.site.Vertices.Count; v++)
            {
                Vector2 vertex = settings.site.Vertices[v] - settings.site.Centroid;
                Vector2 nextVertex = settings.site.Vertices[(v + 1) % settings.site.Vertices.Count] - settings.site.Centroid;
                if (Vector2.Distance(vertex, nextVertex) < 100)
                    // Wall = WAAAAAAAAAY too small
                    continue;
                if (CityGenerator.ConvertVoronoiLengthToWorldSpace(Vector2.Distance(vertex, nextVertex)) < settings.minimalFloorWidth / settings.floorObject.transform.localScale.x)
                    // Wall = too small
                    continue;

                Vector3 edgeStart = CityGenerator.ConvertVoronoiSpaceToWorldSpace3D(vertex);
                Vector3 edgeEnd = CityGenerator.ConvertVoronoiSpaceToWorldSpace3D(nextVertex);

                Vector3 wallDirection = edgeStart - edgeEnd;
                Vector3 wallDirectionNormal = Vector3.Cross(wallDirection, Vector3.up).normalized;
                Quaternion rotation = Quaternion.LookRotation(wallDirectionNormal);


                float width = Vector3.Distance(edgeStart, edgeEnd);
                int numberOfPrefabsToPlace = Mathf.Max(1, (int) Mathf.Floor(width * settings.floorObject.transform.localScale.x / settings.spaceBetweenPrefabsInArray));
                float windowSpace = (numberOfPrefabsToPlace - 1) * settings.spaceBetweenPrefabsInArray / settings.floorObject.transform.localScale.x;
                float margin = .5f * (width - windowSpace);

                bool placedPrefab = false;
                for (int i = 0; i < numberOfPrefabsToPlace; i++)
                {
                    // Randomly skip some parts :)
                    if (!rand.getRandomBool(settings.randomPlacementQuotaInArray))
                        continue;
                    placedPrefab = true;

                    GameObject newObject = CityGenerator.InstantiateGameObject(settings.pathToPrefab);
                    newObject.name = settings.pathToPrefab;
                    newObject.transform.parent = settings.floorObject.transform;

                    // Translate in wall direction (x-direction when looking on wall from normal POV)
                    newObject.transform.localPosition = edgeEnd + wallDirection.normalized * (margin + ((float) i/Mathf.Max(1, numberOfPrefabsToPlace-1) * windowSpace));
                    // Translate in y direction
                    newObject.transform.localPosition += Vector3.up * (settings.relativePosition.y * settings.floorHeight + settings.absolutePosition.y);

                    newObject.transform.localRotation = rotation;
                    newObject.transform.localScale = settings.prefabSize * new Vector3(1 / settings.floorObject.transform.localScale.x, 1 / settings.floorObject.transform.localScale.y, 1 / settings.floorObject.transform.localScale.z);

                    generatedObjects.Add(newObject);
                }

                // If no prafab was placed (due to randomly skipping all), repeat this cycle!
                if (!placedPrefab && antiLoopStuckVariable < 50)
                {
                    v--;
                    antiLoopStuckVariable++;
                }
                else
                {
                    antiLoopStuckVariable = 0;
                }
            }

            return generatedObjects;
        }



        internal static List<GameObject> PlaceSingleObjectsOnBuildingFacade(PlaceObjectsOnBuildingFacadeSetting settings)
        {
            List<GameObject> generatedObjects = new List<GameObject>();
            for (int v = 0; v < settings.site.Vertices.Count; v++)
            {
                Vector2 vertex = settings.site.Vertices[v] - settings.site.Centroid;
                Vector2 nextVertex = settings.site.Vertices[(v + 1) % settings.site.Vertices.Count] - settings.site.Centroid;
                //if (Vector2.Distance(vertex, nextVertex) < 100)
                //    // Wall = WAAAAAAAAAY too small
                //    continue;
                //if (CityGenerator.ConvertVoronoiLengthToWorldSpace(Vector2.Distance(vertex, nextVertex)) < settings.minimalFloorWidth / settings.floorObject.transform.localScale.x)
                //    // Wall = too small
                //    continue;

                Vector3 edgeStart = CityGenerator.ConvertVoronoiSpaceToWorldSpace3D(vertex);
                Vector3 edgeEnd = CityGenerator.ConvertVoronoiSpaceToWorldSpace3D(nextVertex);

                float width = Vector3.Distance(edgeStart, edgeEnd);

                Vector3 wallDirection = edgeStart - edgeEnd;
                Vector3 wallDirectionNormal = Vector3.Cross(wallDirection, Vector3.up).normalized;
                if (wallDirectionNormal == Vector3.zero)
                {
                    continue;
                }
                Quaternion rotation = Quaternion.LookRotation(wallDirectionNormal);

                GameObject newObject = CityGenerator.InstantiateGameObject(settings.pathToPrefab);
                newObject.name = settings.pathToPrefab;
                newObject.transform.parent = settings.floorObject.transform;

                // Put Prefab in bottom center of wall
                newObject.transform.localPosition = edgeEnd + .5f * wallDirection;
                // Translate in wall direction (x-direction when looking on wall from normal POV)
                newObject.transform.localPosition += wallDirection.normalized * (width * (-1 * settings.relativePosition.x + 0.5f) - settings.absolutePosition.x);
                // Translate in y direction
                newObject.transform.localPosition += Vector3.up * (settings.relativePosition.y * settings.floorHeight + settings.absolutePosition.y);

                newObject.transform.localRotation = rotation;
                newObject.transform.localScale = settings.prefabSize * new Vector3(1/ settings.floorObject.transform.localScale.x, 1/ settings.floorObject.transform.localScale.y, 1/ settings.floorObject.transform.localScale.z);

                generatedObjects.Add(newObject);
            }

            return generatedObjects;
        }
    }
}