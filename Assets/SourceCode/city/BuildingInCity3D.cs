﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelsForGlory.VoronoiDiagram;
using ISA.util;
using System.Linq;
using ISA.UI;
using ISA.SolarOverview;
using static ISA.UI.AbstractConnectionCreator;

namespace ISA.CityGeneration
{
    public class BuildingInCity3D : AbstractBuilding3D, ISASynchronizedUIElementListener
    {
        public List<BuildingFloor3D> Floors { get; private set; }
        public float Height { get; private set; }
        public float MaxDiameter { get; private set; }
        public int NumberOfAntennaFloors { get; private set; }
        public Color Color { get; private set; }


        // IMPORTANT!! Don't use this point after the Voronoi Floyd relaxation was run!
        public Vector2 InitialVoronoiCenterPoint;
        //public List<Vector2> SubComplexPoints { get; private set; }
        public List<Vector2> VoronoiPointsAroundBuilding { get; private set; }


        internal VoronoiDiagramGeneratedSite<VoronoiSiteData> AssignedGeneratedVoronoiSite;
        //internal List<VoronoiDiagramGeneratedSite<VoronoiSiteData>> ComplexVoronoiSites = new List<VoronoiDiagramGeneratedSite<VoronoiSiteData>>();
        internal List<VoronoiDiagramGeneratedSite<VoronoiSiteData>> SurroundingVoronoiSites = new List<VoronoiDiagramGeneratedSite<VoronoiSiteData>>();

        public GameObject gameObject { get; private set; }
        public BuildingInfoCanvasManager infoCanvasManager { get; private set; }
        public BuildingHighlightManager highlightManager { get; private set; }
        public ISABuilding ecoreModel { get; private set; }





        public BuildingInCity3D(ISABuilding ecoreBuildingModel)
        {
            ecoreModel = ecoreBuildingModel;

            this.Height = 0f;
            Color = Color.HSVToRGB(RandomFactory.INSTANCE.getRandomFloat(), RandomFactory.INSTANCE.getRandomFloat(0.5f, 0.9f), RandomFactory.INSTANCE.getRandomFloat(0.25f, 0.9f));

            InitialVoronoiCenterPoint = new Vector2(ecoreModel.position.x, ecoreModel.position.y);

            if (ecoreModel.floors == null || ecoreModel.floors.Length == 0)
                return;

            // add info for building floors!
            Floors = new List<BuildingFloor3D>();
            BuildingFloor3D groundFloor = new BuildingFloor3D(this, null, -1);
            this.Height += groundFloor.Height;
            Floors.Add(groundFloor);

            float averageDiameter = 0f;
            MaxDiameter = 0;
            NumberOfAntennaFloors = 0;
            for (int i=ecoreModel.floors.Length-1; i>=0; i--)
            {
                ISAFloor floor = ecoreModel.floors[i];
                BuildingFloor3D newFloor = new BuildingFloor3D(this, floor, i);

                if (floor.antennaFloor)
                    NumberOfAntennaFloors++;

                newFloor.yOffset = this.Height;
                this.Height += newFloor.Height;
                averageDiameter += newFloor.Diameter / (float) ecoreModel.floors.Length;
                if (newFloor.Diameter > MaxDiameter)
                    MaxDiameter = newFloor.Diameter;

                Floors.Add(newFloor);
            }

            //groundFloor.Diameter = Floors[1].Diameter;
            //groundFloor.Diameter = Mathf.Max(averageDiameter, 2);
            groundFloor.Diameter = MaxDiameter;
        }

        public void NotifyUpdateOnElement(ISANamedVisualElement e)
        {
            if (highlightManager == null)
                // too early in the process, the planet and it's UI have not been generated
                return;

            if (ISAUIInteractionSynchronizer.INSTANCE.isActive(e))
            {
                highlightManager.SetActive();
            }
            else if (ISAUIInteractionSynchronizer.INSTANCE.isHovered(e))
            {
                highlightManager.SetHovered();
            }
            else
            {
                highlightManager.Hide();
            }
        }

        public void SetupDiageticUI()
        {
            ISAUIInteractionSynchronizer.INSTANCE.RegisterListener(ecoreModel, this);


            GameObject highlightCircleObject = Object.Instantiate(Resources.Load("SolarOverview/UI/BuildingHighlightCircleCanvas") as GameObject);
            highlightCircleObject.name += " (DeleteOnClone)";
            highlightCircleObject.transform.parent = gameObject.transform;
            highlightCircleObject.transform.localPosition = Vector3.zero;
            highlightCircleObject.transform.localScale = 2f * gameObject.transform.lossyScale.x * Vector3.one / Planet3D.OVERSIZED_GENERATION_FACTOR;
            highlightCircleObject.transform.localRotation = Quaternion.identity;
            highlightManager = highlightCircleObject.GetComponent<BuildingHighlightManager>();
            highlightManager.SetName(ecoreModel.name);

            // Find existing info canvas, must be present and spawned somewhere already for a preview building on a planet
            //GameObject infoCanvas = Object.Instantiate(Resources.Load("SolarOverview/UI/BuildingInfoCanvas") as GameObject);

            BuildingOnPlanet3D buildingOnPlanet3D = null;
            foreach (Planet3D planet in SolarSystemManager.INSTANCE.SolarSystemGenerator.planetDataList)
            {
                // Go through planet overview
                foreach (CityContinent3D city in planet.cityContinents)
                {
                    buildingOnPlanet3D = BuildingConnectionCreator.INSTANCE.GetBuildingGameObject(city, ecoreModel.qualifiedName, SearchQueryType.EQUALS);
                    if (buildingOnPlanet3D != null)
                        goto CheckIfBuildingFound;
                }
            }
            CheckIfBuildingFound:
            if(buildingOnPlanet3D == null)
            {
                Debug.LogError("Did not find a preview building while trying to construct the information canvas for a detailed building: " + ecoreModel.qualifiedName);
                return;
            }

            infoCanvasManager = buildingOnPlanet3D.infoCanvasManager;
            if(infoCanvasManager == null)
            {
                buildingOnPlanet3D.ExplicitelyInstantiateInfoCanvas();
                infoCanvasManager = buildingOnPlanet3D.infoCanvasManager;
            }
            infoCanvasManager.Setup(this);
        }

        public void GenerateVoronoiPointsAroundBuilding()
        {
            int numberOfPoints = RandomFactory.INSTANCE.getRandomInt(5, 8);
            VoronoiPointsAroundBuilding = new List<Vector2>();
            for (int i = 0; i < numberOfPoints; i++)
            {
                Vector2 relativePoint = GeometryHelper.CalculateVertexPositionIn01Circle(i, numberOfPoints);
                Vector2 absolutePoint = InitialVoronoiCenterPoint + relativePoint * CityGenerator.MinDistanceBetweenVoronoiPoints;
                VoronoiPointsAroundBuilding.Add(absolutePoint);
            }
        }

        /*public static float CalculateInnerCircleRemapping(int size)
        {
            float relativeSize = (float)(size - MIN_SUB_COMPLEXES) / (MAX_SUB_COMPLEXES - MIN_SUB_COMPLEXES);
            return MAX_SIZE_ON_VORONOI * (0.2f + relativeSize * 0.8f);
        }

        public static float CalculateOuterCircleRemapping(int size)
        {
            return 1.5f * CalculateInnerCircleRemapping(size);
        }*/






        public IEnumerator GenerateMesh(GameObject parentObject)
        {
            gameObject = new GameObject("Building: " + ecoreModel.qualifiedName);
            gameObject.transform.parent = parentObject.transform;
            gameObject.transform.localPosition = CityGenerator.ConvertVoronoiSpaceToWorldSpace3D(AssignedGeneratedVoronoiSite.Centroid);
            gameObject.transform.localRotation = Quaternion.identity;

            //GameObject debugObject = CityGenerator.InstantiateGameObject("City/SatelliteDish/SatelliteDish");
            //debugObject.name = "BUILDING DEBUG OBJECT";
            //debugObject.transform.parent = gameObject.transform;
            //debugObject.transform.localPosition = Vector3.up;
            //debugObject.transform.localScale = 0.05f * Vector3.one;
            //debugObject.transform.rotation.SetLookRotation(Vector3.down);

            SetupDiageticUI();

            foreach (BuildingFloor3D floor in Floors ?? Enumerable.Empty<BuildingFloor3D>())
            {
                floor.GenerateMesh();
                yield return null;
            }
        }
    }
}