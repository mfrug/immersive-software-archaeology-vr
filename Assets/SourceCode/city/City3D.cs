using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelsForGlory.VoronoiDiagram;
using ISA.util;

using Microsoft.Msagl.Core;
using Microsoft.Msagl.Core.Geometry;
using Microsoft.Msagl.Core.GraphAlgorithms;
using Microsoft.Msagl.Layout.Incremental;
using Microsoft.Msagl.Core.Layout;
using Microsoft.Msagl.Core.Geometry.Curves;
using System.Threading;

namespace ISA.CityGeneration
{
    public class City3D
    {
        public List<BuildingInCity3D> buildings { private set; get; }
        public List<Street> streets { private set; get; }

        public float NarrowestStreetWidth { get { return streets[streets.Count - 1].width; } }
        public float WidestStreetWidth { get { return streets[0].width; } }

        public ISACity ecoreModel { private set; get; }

        private bool graphLayouted;
        private bool streetSegmentsGenerated;

        public City3D(ISACity ecoreCityModel)
        {
            ecoreModel = ecoreCityModel;
        }

        public void SetupDataModels(bool visualDebug)
        {
            buildings = new List<BuildingInCity3D>();
            for(int i=0; i<(ecoreModel.buildings ?? Array.Empty<ISABuilding>()).Length; i++)
            {
                BuildingInCity3D newBuilding = new BuildingInCity3D(ecoreModel.buildings[i]);
                buildings.Add(newBuilding);
            }
            normalizeBuildingPositions();

            float shortestDistanceBetweenBuildings = float.MaxValue;
            for(int i=0; i<buildings.Count; i++)
            {
                for(int j=i+1; j<buildings.Count; j++)
                {
                    float dist = Vector2.Distance(buildings[i].InitialVoronoiCenterPoint, buildings[j].InitialVoronoiCenterPoint);
                    if (dist < shortestDistanceBetweenBuildings)
                        shortestDistanceBetweenBuildings = dist;
                }
            }
            CityGenerator.MinDistanceBetweenVoronoiPoints = shortestDistanceBetweenBuildings / 3f;

            foreach (BuildingInCity3D building in buildings)
            {
                building.GenerateVoronoiPointsAroundBuilding();
            }

            streets = new List<Street>();
            for (int i = 0; i < buildings.Count; i++)
            {
                BuildingInCity3D building = buildings[i];
                for (int j = i + 1; j < buildings.Count; j++)
                {
                    BuildingInCity3D otherBuilding = buildings[j];

                    if(RandomFactory.INSTANCE.getRandomBool(0.3f))
                    {
                        Street street = new Street(this, building, otherBuilding);
                        street.width = RandomFactory.INSTANCE.getRandomFloat(1f, 10f);
                        streets.Add(street);
                    }
                }
            }
            //foreach(ISABuildingConnection conn in ecoreModel.buildingConnections)
            //{
            //    Building3D building = null;
            //    Building3D otherBuilding = null;
            //    foreach(Building3D currentBuilding in buildings)
            //    {
            //        if (currentBuilding.ecoreModel.Equals(conn.building1))
            //            building = currentBuilding;
            //    }

            //    Street street = new Street(this, building, otherBuilding);
            //    street.width = RandomFactory.INSTANCE.getRandomFloat(1f, 10f);
            //    streets.Add(street);
            //}
            streets.Sort();
            streets.Reverse();

            //placeBuildingsRandomly();
            //placeBuildingsInCircle(1f);
            //yield return null;

            //yield return SceneHandler.INSTANCE.StartCoroutine(placeBuildingsBasedOnGraphLayout());

            if (visualDebug)
            {
                foreach (Street street in streets)
                {
                    Vector3 pos1 = new Vector3(street.building1.InitialVoronoiCenterPoint.x * CityGenerator.CITY_DIMENSIONS, 0, street.building1.InitialVoronoiCenterPoint.y * CityGenerator.CITY_DIMENSIONS);
                    Vector3 pos2 = new Vector3(street.building2.InitialVoronoiCenterPoint.x * CityGenerator.CITY_DIMENSIONS, 0, street.building2.InitialVoronoiCenterPoint.y * CityGenerator.CITY_DIMENSIONS);
                    Debug.DrawLine(pos1, pos2, Color.HSVToRGB(0, 0, 0.25f), float.MaxValue);
                }
                foreach (BuildingInCity3D building in buildings)
                {
                    Vector3 pos = new Vector3(building.InitialVoronoiCenterPoint.x * CityGenerator.CITY_DIMENSIONS, 0, building.InitialVoronoiCenterPoint.y * CityGenerator.CITY_DIMENSIONS);
                    Debug.DrawLine(pos, pos + Vector3.up * 0.5f, building.Color, float.MaxValue);
                }
            }
        }

        private void normalizeBuildingPositions()
        {
            float smallestPosition = float.MaxValue;
            float largestPosition = float.MinValue;
            foreach (BuildingInCity3D building in buildings)
            {
                if ((float)building.InitialVoronoiCenterPoint.x < smallestPosition)
                    smallestPosition = (float)building.InitialVoronoiCenterPoint.x;
                if ((float)building.InitialVoronoiCenterPoint.y < smallestPosition)
                    smallestPosition = (float)building.InitialVoronoiCenterPoint.y;

                if ((float)building.InitialVoronoiCenterPoint.x > largestPosition)
                    largestPosition = (float)building.InitialVoronoiCenterPoint.x;
                if ((float)building.InitialVoronoiCenterPoint.y > largestPosition)
                    largestPosition = (float)building.InitialVoronoiCenterPoint.y;
            }

            // normalize to 0-1 range
            foreach (BuildingInCity3D building in buildings)
            {
                float newX = ((float)building.InitialVoronoiCenterPoint.x - smallestPosition) / (Mathf.Abs(smallestPosition) + Mathf.Abs(largestPosition));
                float newY = ((float)building.InitialVoronoiCenterPoint.y - smallestPosition) / (Mathf.Abs(smallestPosition) + Mathf.Abs(largestPosition));
                Vector2 position01 = new Vector2(newX, newY) * 0.8f + 0.1f * Vector2.one;
                //Debug.Log(building.InitialVoronoiCenterPoint + "  ->  " + position01);
                building.InitialVoronoiCenterPoint = position01;
            }
        }

        private void placeBuildingsInCircle(float range)
        {
            for(int i=0; i<buildings.Count; i++)
            {
                Vector2 relativePoint = GeometryHelper.CalculateVertexPositionIn01Circle(i, buildings.Count);
                Vector2 absolutePoint = (0.5f * Vector2.one + 0.4f * relativePoint) * range;
                buildings[i].InitialVoronoiCenterPoint = absolutePoint;
            }
        }

        private IEnumerator placeBuildingsBasedOnGraphLayout()
        {
            Dictionary<BuildingInCity3D, Node> nodeMapping = new Dictionary<BuildingInCity3D, Node>();

            GeometryGraph graph = new GeometryGraph();
            foreach(BuildingInCity3D building in buildings)
            {
                Node n = new Node(CurveFactory.CreateRectangle(3, 3, new Point()));
                n.Center = new Point(building.InitialVoronoiCenterPoint.x, building.InitialVoronoiCenterPoint.y);
                //n.BoundingBox = new Rectangle(new Point(-1, -1), new Point(1, 1));
                n.UserData = building;

                graph.Nodes.Add(n);
                nodeMapping.Add(building, n);
            }
            yield return null;

            foreach (Street street in streets)
            {
                Edge e = new Edge(nodeMapping[street.building1], nodeMapping[street.building2]);
                e.Weight = (int) Mathf.Round(street.width);
                graph.Edges.Add(e);
            }
            graph.UpdateBoundingBox();
            //graph.RootCluster = new Cluster(new Point(CityGenerator.VORONOI_DISTANCE_01 / 2, CityGenerator.VORONOI_DISTANCE_01 / 2));
            yield return null;

            FastIncrementalLayoutSettings settings;
            //settings = FastIncrementalLayoutSettings.CreateFastIncrementalLayoutSettings();
            settings = new FastIncrementalLayoutSettings
            {
                ApplyForces = true, // false
                ApproximateRepulsion = true,
                ApproximateRouting = true,
                AttractiveForceConstant = 1.0,
                AttractiveInterClusterForceConstant = 1.0,
                AvoidOverlaps = true,
                ClusterGravity = 1.0,
                Decay = 0.9,
                DisplacementThreshold = 0.00000005,
                Friction = 0.8,
                GravityConstant = 1.0,
                InitialStepSize = 2.0,
                InterComponentForces = false,
                Iterations = 0,
                LogScaleEdgeForces = false,
                MaxConstraintLevel = 2,
                MaxIterations = 20,
                MinConstraintLevel = 0,
                MinorIterations = 1,
                ProjectionIterations = 5,
                RepulsiveForceConstant = 2.0,
                RespectEdgePorts = false,
                RouteEdges = false,
                RungeKuttaIntegration = true,
                UpdateClusterBoundariesFromChildren = true,
                NodeSeparation = 20
            };

            graphLayouted = false;
            ThreadStart threadRef = new ThreadStart(delegate {
                Microsoft.Msagl.Miscellaneous.LayoutHelpers.CalculateLayout(graph, settings, null);
                graphLayouted = true;
            });
            new Thread(threadRef).Start();
            while (!graphLayouted)
            {
                yield return null;
            }

            //settings.structuralConstraints.Add(new TestConstraint(graph.Nodes));
            //settings.IncrementalRun(graph);
            //FastIncrementalLayout layout = new FastIncrementalLayout(graph, settings, settings.MinConstraintLevel, anyCluster => settings);
            //layout.Run();

            float smallestPosition = float.MaxValue;
            float largestPosition = float.MinValue;
            foreach (Node node in graph.Nodes)
            {
                if ((float)node.Center.X < smallestPosition)
                    smallestPosition = (float)node.Center.X;
                if ((float)node.Center.Y < smallestPosition)
                    smallestPosition = (float)node.Center.Y;

                if ((float)node.Center.X > largestPosition)
                    largestPosition = (float)node.Center.X;
                if ((float)node.Center.Y > largestPosition)
                    largestPosition = (float)node.Center.Y;
            }
            yield return null;

            // normalize to 0-1 range
            foreach (Node node in graph.Nodes)
            {
                float newX = ((float)node.Center.X - smallestPosition) / (Mathf.Abs(smallestPosition) + Mathf.Abs(largestPosition));
                float newY = ((float)node.Center.Y - smallestPosition) / (Mathf.Abs(smallestPosition) + Mathf.Abs(largestPosition));
                Vector2 position01 = new Vector2(newX, newY) * 0.8f + 0.1f * Vector2.one;
                ((BuildingInCity3D)node.UserData).InitialVoronoiCenterPoint = position01;
            }
            yield return null;
        }

        /*private class TestConstraint : IConstraint
        {
            private IList<Node> nodes;

            public Test(IList<Node> nodes)
            {
                this.nodes = nodes;
            }

            public IEnumerable<Node> Nodes => nodes;

            public int Level => 2;

            public double Project()
            {
                return 0;
            }
        }*/

        private void placeBuildingsRandomly()
        {
            for (int i = 0; i < buildings.Count; i++)
            {
                BuildingInCity3D building = buildings[i];

                Vector2 position = Vector2.zero;
                int positioningAttempt = 0;
            TryNewPosition:
                position = new Vector2(RandomFactory.INSTANCE.getRandomFloat(0.1f, 0.9f), RandomFactory.INSTANCE.getRandomFloat(0.1f, 0.9f));
                foreach (BuildingInCity3D otherBuilding in buildings)
                {
                    if (Vector2.Distance(position, otherBuilding.InitialVoronoiCenterPoint) <= 3f * CityGenerator.MinDistanceBetweenVoronoiPoints)
                    {
                        if (positioningAttempt == 100)
                        {
                            Debug.LogError("Could not position a building without colliding with another after several failed attempts!");
                            break;
                        }
                        // too little space, try again
                        positioningAttempt++;
                        goto TryNewPosition;
                    }
                }

                building.InitialVoronoiCenterPoint = position;
            }
        }

        /*private Building[] getClosestBuildings(Building building, int searchedAmount)
        {
            if (buildings.Count - 1 <= searchedAmount)
                throw new System.Exception("Searching for more buildings than possible!");

            Building[] result = new Building[searchedAmount];
            float[] bestDistances = new float[searchedAmount];

            Vector2 center = building.CenterPoint;
            foreach (Building otherBuilding in buildings)
            {
                if (otherBuilding == building)
                    continue;
                Vector2 otherCenter = otherBuilding.CenterPoint;

            }

            return result;
        }*/



        public List<VoronoiDiagramSite<VoronoiSiteData>> CalculateVoronoiPoints()
        {
            List<VoronoiDiagramSite<VoronoiSiteData>> voronoiPoints = new List<VoronoiDiagramSite<VoronoiSiteData>>();

            // First, get the points for each building!
            foreach(BuildingInCity3D building in buildings)
            {
                Vector2 pos = building.InitialVoronoiCenterPoint * (CityGenerator.VORONOI_DIMENSIONS - 1);
                voronoiPoints.Add(new VoronoiDiagramSite<VoronoiSiteData>(pos, new VoronoiSiteData(building, VoronoiSiteData.Type.Building)));

                foreach (Vector2 point in building.VoronoiPointsAroundBuilding)
                {
                    pos = point * (CityGenerator.VORONOI_DIMENSIONS - 1);
                    voronoiPoints.Add(new VoronoiDiagramSite<VoronoiSiteData>(pos, new VoronoiSiteData(building, VoronoiSiteData.Type.Garden)));
                }
            }

            //return voronoiPoints;

            Debug.Log("Starting to fill in points - MinDistanceBetweenVoronoiPoints=" + CityGenerator.MinDistanceBetweenVoronoiPoints);

            int sizeBeforeFilling = voronoiPoints.Count;
            int pointsToCreateMax = 10000;

            // Then, fill out additional points to make the city more interesting
            while (true)
            {
                int attempts = 0;
            MakeNewPoint:
                Vector2 pos = new Vector2(RandomFactory.INSTANCE.getRandomFloat(), RandomFactory.INSTANCE.getRandomFloat()) * (CityGenerator.VORONOI_DIMENSIONS - 1);
                foreach (VoronoiDiagramSite<VoronoiSiteData> voronoiPoint in voronoiPoints)
                {
                    if (Vector2.Distance(pos, voronoiPoint.Coordinate) <= 2f * CityGenerator.MinDistanceBetweenVoronoiPoints * (CityGenerator.VORONOI_DIMENSIONS - 1))
                    {
                        // too little space
                        if (attempts == 100)
                        {
                            // tried 100 times without success, that's good enough :)
                            Debug.Log("Can't find any more available space to fill in points in the voronoi! Filled in " + (voronoiPoints.Count - sizeBeforeFilling) + " points!");
                            return voronoiPoints;
                        }
                        // try again
                        attempts++;
                        goto MakeNewPoint;
                    }
                }

                voronoiPoints.Add(new VoronoiDiagramSite<VoronoiSiteData>(pos, new VoronoiSiteData(null, VoronoiSiteData.Type.Free)));
                if(--pointsToCreateMax == 0)
                {
                    Debug.Log("Maximum voronoi fill points reached: Filled in " + (voronoiPoints.Count - sizeBeforeFilling) + " points in the voronoi diagram!");
                    return voronoiPoints;
                }
            }
        }



        public IEnumerator GenerateAndUpdateStreetSegments(VoronoiDiagram<VoronoiSiteData> voronoiDiagram)
        {
            streetSegmentsGenerated = false;
            ThreadStart threadRef = new ThreadStart(delegate {
                for (int i = 0; i < streets.Count; i++)
                //foreach (Street street in city.streets)
                {
                    streets[i].GenerateAndUpdateSegments(voronoiDiagram);
                    /*
                    if (i % 20 == 0)
                        CitySceneManager.INSTANCE.UpdateInfoStatus("generating street layout", (float)i / streets.Count);
                    */
                }
                streetSegmentsGenerated = true;
            });

            new Thread(threadRef).Start();
            while (!streetSegmentsGenerated)
            {
                yield return null;
            }
        }



        internal IEnumerator GenerateBuildingMeshes(CityGenerator generator, GameObject parentGameObject)
        {
            GameObject buildingsParentObject = new GameObject("Buildings Parent");
            buildingsParentObject.transform.parent = parentGameObject.transform;
            buildingsParentObject.transform.localPosition = Vector3.zero;
            buildingsParentObject.transform.localRotation = Quaternion.identity;

            for (int i=0; i<buildings.Count; i++)
            //foreach (BuildingData building in buildings)
            {
                //CitySceneManager.INSTANCE.UpdateInfoStatus("generating buildings", (float)i / buildings.Count);
                yield return generator.StartCoroutine(buildings[i].GenerateMesh(buildingsParentObject));
            }
        }
    }
}