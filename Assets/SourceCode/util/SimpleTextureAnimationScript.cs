using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTextureAnimationScript : MonoBehaviour
{
    [SerializeField]
    float speed = 1.0f;

    void Update()
    {
        GetComponent<Renderer>().material.mainTextureOffset = Vector2.right * Time.time * speed;
    }
}
