using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveToggleable : MonoBehaviour
{

    public bool showAtStart = true;
    public Transform AttachToAtStart = null;

    public void Start()
    {
        if (AttachToAtStart != null)
        {
            Vector3 localPosition = transform.localPosition;
            Vector3 localScale = transform.localScale;
            Quaternion localRotation = transform.localRotation;

            transform.parent = AttachToAtStart;
            transform.localPosition = localPosition;
            transform.localScale = localScale;
            transform.localRotation = localRotation;
        }

        if (!showAtStart)
            gameObject.SetActive(false);
    }

    public void ToggleActive()
    {
        if (this.isActiveAndEnabled)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
        }
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
