﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyBoxLightDirection : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Shader.SetGlobalVector("_SunLightDirection", transform.forward);
        Shader.SetGlobalColor("_SunLightColor", GetComponent<Light>().color);
        Shader.SetGlobalFloat("_SunLightIntensity", GetComponent<Light>().intensity);
    }

    // Update is called once per frame
    void Update()
    {
        Shader.SetGlobalVector("_SunLightDirection", transform.forward);
        Shader.SetGlobalColor("_SunLightColor", GetComponent<Light>().color);
        Shader.SetGlobalFloat("_SunLightIntensity", GetComponent<Light>().intensity);
    }
}
