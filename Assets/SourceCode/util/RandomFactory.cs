﻿using System.Collections;
using UnityEngine;

namespace ISA.util
{
    public class RandomFactory
    {

        public static RandomFactory INSTANCE = new RandomFactory(24061337);



        private System.Random rand;

        public RandomFactory(int seed)
        {
            rand = new System.Random(seed);
        }



        public float getRandomFloat()
        {
            return (float)rand.NextDouble();
        }

        public float getRandomFloat(float max)
        {
            return getRandomFloat() * max;
        }

        public float getRandomFloat(float min, float max)
        {
            if (max < min)
                throw new System.Exception("max value must be larger than or equal to min value!");
            return min + getRandomFloat(max-min);
        }



        public int getRandomInt()
        {
            return rand.Next();
        }
        public int getRandomInt(int max)
        {
            return rand.Next(max);
        }
        public int getRandomInt(int min, int max)
        {
            return rand.Next(min, max);
        }



        public bool getRandomBool(float probabilityForTrue)
        {
            return rand.NextDouble() <= probabilityForTrue;
        }

        public bool getRandomBool()
        {
            return getRandomBool(0.5f);
        }



        public Vector3 getRandomVector(float minMagnitude, float maxMagnitude)
        {
            return getRandomFloat(minMagnitude, maxMagnitude) * getRandomNormalizedVector();
        }

        public Vector3 getRandomVector(float fixedMagnitude)
        {
            return fixedMagnitude * getRandomNormalizedVector();
        }

        public Vector3 getRandomNormalizedVector()
        {
            float x = Mathf.Sin(Mathf.Deg2Rad * getRandomFloat(0, 360));
            float y = Mathf.Sin(Mathf.Deg2Rad * getRandomFloat(0, 360));
            float z = Mathf.Sin(Mathf.Deg2Rad * getRandomFloat(0, 360));
            return new Vector3(x, y, z).normalized;
        }



        public Quaternion getRandomRotation()
        {
            return Quaternion.Euler(getRandomFloat(360), getRandomFloat(360), getRandomFloat(360));
        }
    }
}