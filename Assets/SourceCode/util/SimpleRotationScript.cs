using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ISA.util
{
    public class SimpleRotationScript : MonoBehaviour
    {

        public Vector3 rotationAxis = Vector3.zero;
        public float rotationAngle = 0f;
        public float rotationSpeed = 1f;



        void Update()
        {
            rotationAngle += Time.deltaTime * rotationSpeed;
            transform.rotation = Quaternion.AngleAxis(rotationAngle, rotationAxis);
        }
    }
}