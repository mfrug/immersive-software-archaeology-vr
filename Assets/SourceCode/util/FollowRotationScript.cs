using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace ISA.util
{
    public class FollowRotationScript : MonoBehaviour
    {

        public Transform TransformToFollow;
        public bool Inversed = false;


        void Update()
        {
            Vector3 lookRotation;
            if (!Inversed)
                lookRotation = TransformToFollow.position - transform.position;
            else
                lookRotation = transform.position - TransformToFollow.position;

            if (lookRotation.magnitude > Vector3.kEpsilon)
                transform.rotation = Quaternion.LookRotation(lookRotation);
        }
    }
}