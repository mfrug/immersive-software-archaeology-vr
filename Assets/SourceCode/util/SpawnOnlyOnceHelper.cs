using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOnlyOnceHelper : MonoBehaviour
{

    public static bool AlreadySpawned = false;

    void Start()
    {
        if (AlreadySpawned)
            gameObject.SetActive(false);
        AlreadySpawned = true;
    }
}
