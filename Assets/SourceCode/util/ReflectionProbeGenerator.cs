using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace ISA.util
{

    public class ReflectionProbeGenerator
    {

        public static ReflectionProbe GenerateReflectionProbe(Transform parent,  Vector3 localPosition, Vector3 probeSize, string name = "Unnamed Reflection Probe", int resolution = 256, bool renderProbe = false)
        {
            GameObject probeGameObject = new GameObject(name);
            if(parent != null)
                probeGameObject.transform.parent = parent;
            if (localPosition != null)
                probeGameObject.transform.localPosition = localPosition;
            else
                probeGameObject.transform.localPosition = Vector3.zero;

            ReflectionProbe probeComponent = probeGameObject.AddComponent<ReflectionProbe>() as ReflectionProbe;
            probeComponent.resolution = 256;
            probeComponent.size = probeSize;
            probeComponent.hdr = true;
            probeComponent.mode = ReflectionProbeMode.Realtime;
            probeComponent.refreshMode = ReflectionProbeRefreshMode.ViaScripting;
            probeComponent.timeSlicingMode = ReflectionProbeTimeSlicingMode.AllFacesAtOnce;
            probeComponent.importance = 0;

            if (renderProbe)
                probeComponent.RenderProbe();

            return probeComponent;
        }

    }

}
