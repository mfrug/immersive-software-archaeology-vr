using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ISA.util
{

    public class VisualDebugger
    {

        public static void DrawPoint(Vector3 p)
        {
            DrawPoint(p, 0.1f * Vector3.one, Color.red);
        }

        public static void DrawPoint(Vector3 p, Vector3 size)
        {
            DrawPoint(p, size, Color.red);
        }

        public static void DrawPoint(Vector3 p, Color color)
        {
            DrawPoint(p, 0.1f * Vector3.one, color);
        }

        public static void DrawPoint(Vector3 p, Vector3 size, Color color)
        {
            Debug.DrawLine(p + size.x * Vector3.left, p + size.x * Vector3.right, color, float.MaxValue);
            Debug.DrawLine(p + size.y * Vector3.down, p + size.y * Vector3.up, color, float.MaxValue);
            Debug.DrawLine(p + size.z * Vector3.forward, p + size.z * Vector3.back, color, float.MaxValue);
        }

    }

}
