﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ISA.util
{
    public class SimpleCameraRotationScript : MonoBehaviour
    {
        public Transform rotationcenter;

        public float RotationSpeed = 10.0f;
        public float RotationDistance = 5f;

        private void Start()
        {
            transform.rotation = Quaternion.LookRotation(Vector3.forward);
            transform.position = rotationcenter.position - RotationDistance * Vector3.forward;
        }

        void Update()
        {
            transform.RotateAround(rotationcenter.position, new Vector3(0.0f, 1.0f, 0.0f), RotationSpeed * Time.deltaTime);
        }
    }
}