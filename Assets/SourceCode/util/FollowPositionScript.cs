using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPositionScript : MonoBehaviour
{

    public Transform transformToFollow;

    void FixedUpdate()
    {
        transform.position = transformToFollow.position;
    }

}
