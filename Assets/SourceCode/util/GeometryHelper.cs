using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ISA.util
{

    public class GeometryHelper
    {

        public static Vector2 CalculateVertexPositionIn01Circle(int vertexIndex, int circleResolution)
        {
            float angle = (float)vertexIndex * 360.0f / (float)circleResolution;
            return new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad));
        }

    }

}
