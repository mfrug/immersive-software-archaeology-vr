﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using TMPro;
using System.Linq;
using ISA.UI;
using System.Threading;
using ISA.util;
using System.Threading.Tasks;
using ISA.Modelling;

namespace ISA.SolarOverview
{
    public class SolarSystemGenerator : MonoBehaviour
    {
        public List<Planet3D> planetDataList { private set; get; }



        public IEnumerator GenerateSolarSystem(ModelSelectionUICanvasHandler callingHandler, ISAVisualizationModel model)
        {
            callingHandler.UpdateGenerationProgress("Creating base data from selected model", 0);

            // First, generate the data that contains information on the layout
            if (model.solarSystems.Length != 1)
                throw new Exception("There should be exactly one sun system in the model!");
            ISASolarSystem sunSystem = model.solarSystems[0];

            ISATreeNode sunSystemNode = GlobalTreeViewUICanvasHandler.INSTANCE.CreateEntry(sunSystem, null);
            sunSystemNode.ToggleExpanded();
            GlobalTreeViewUICanvasHandler.INSTANCE.AssignRootNode(sunSystemNode);

            planetDataList = new List<Planet3D>();
            foreach (ISAPlanet planetEcoreModel in sunSystem.planets ?? Enumerable.Empty<ISAPlanet>())
            {
                if (planetEcoreModel.radius == 0)
                    continue;

                Planet3D newPlanetData = new Planet3D(planetEcoreModel);
                planetDataList.Add(newPlanetData);

                yield return null;
            }

            //yield return StartCoroutine(PlanetPositionCalculation.INSTANCE.SetupPlanetPositions(this));

            // Then, use the data to generate 3D meshes!
            for(int i=0; i<planetDataList.Count; i++)
            {
                callingHandler.UpdateGenerationProgress("Generating planet mesh for \"" + planetDataList[i].name + "\"", (float)i / planetDataList.Count);
                yield return StartCoroutine(planetDataList[i].GenerateMesh(false));
            }

            // Further polisihing: reflection probes
            /*foreach (PlanetData planetData in planetDataList)
            {
                ReflectionProbe probe = ReflectionProbeGenerator.GenerateReflectionProbe(planetData.gameObject.transform, planetData.gameObject.transform.position, planetData.radius * 3 * Vector3.one, "Reflection Probe", 256, false);
                probe.nearClipPlane = planetData.radius * 2;
                probe.RenderProbe();

                //GenerateTeleportPoint(planet);

                yield return null;
            }*/

            SolarSystemManager.INSTANCE.NotifyFinishedLoading();
        }
        
        /*private void GenerateTeleportPoint(PlanetData planet)
        {
            GameObject teleportPoint = Instantiate(TeleportPointPrefab);
            teleportPoint.transform.parent = planet.gameObject.transform.parent;

            Vector3 position = planet.gameObject.transform.position;
            position.y = 0;

            Vector3 stepBackToSun = 1.5f * position / position.magnitude;

            teleportPoint.transform.position = position - stepBackToSun - new Vector3(0, 0.95f, 0);
        }*/

        public void Reset()
        {
            foreach(Planet3D planetData in planetDataList)
            {
                planetData.ResetSizeAndPosition();
            }
        }

        private Planet3D findPlanetByName(string name)
        {
            foreach(Planet3D planet in planetDataList)
            {
                if (planet.name.Equals(name))
                    return planet;
            }
            return null;
        }
    }
}