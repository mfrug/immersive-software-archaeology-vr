using ISA.UI;
using ISA.util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ISA.SolarOverview
{

    public class CompoundContinent3D : Continent3D
    {
        public List<Continent3D> children { get; private set; }

        public float totalBuidings { get; private set; }
        public float totalCities { get; private set; }



        public CompoundContinent3D(ISACompoundContinent continentEcoreModel, Planet3D planetData, CompoundContinent3D parentContinent, ISATreeNode parentNode) : base(planetData, parentContinent)
        {
            name = continentEcoreModel.name ?? "Unnamed Continent";
            totalBuidings = (float)continentEcoreModel.accumulatedCitySizes;

            children = new List<Continent3D>();

            ISATreeNode continentNode = GlobalTreeViewUICanvasHandler.INSTANCE.CreateEntry(continentEcoreModel, parentNode);
            // cities must come first (required for mesh generation!)
            foreach (ISAContinent subContinentEcoreModel in continentEcoreModel.subContinents ?? Enumerable.Empty<ISAContinent>())
            {
                if(subContinentEcoreModel is ISACityContinent)
                {
                    children.Add(new CityContinent3D((ISACityContinent)subContinentEcoreModel, planetData, this, continentNode));
                    totalCities++;
                }
            }
            foreach (ISAContinent subContinentEcoreModel in continentEcoreModel.subContinents ?? Enumerable.Empty<ISAContinent>())
            {
                if (subContinentEcoreModel is ISACompoundContinent)
                {
                    CompoundContinent3D newContinent = new CompoundContinent3D((ISACompoundContinent) subContinentEcoreModel, planetData, this, continentNode);
                    children.Add(newContinent);
                    totalCities += newContinent.totalCities;
                }
            }

            ISAUIInteractionSynchronizer.INSTANCE.RegisterListener(continentEcoreModel, this);

            ISAUIInteractable continentInteractable = gameObject.AddComponent<ISAUIInteractable>();
            continentInteractable.onHoverStart.AddListener(delegate {
                //Highlight();
                //planetData.HoverStart();
                ISAUIInteractionSynchronizer.INSTANCE.NotifyHoverStart(continentEcoreModel);
            });
            continentInteractable.onHoverEnd.AddListener(delegate {
                //UnHighlight();
                //planetData.HoverEnd();
                ISAUIInteractionSynchronizer.INSTANCE.NotifyHoverEnd(continentEcoreModel);
            });
            continentInteractable.onTriggerPressed.AddListener(delegate {
                //planetData.infoUIManager.ToggleLocked();
                //planetData.highlightManager.ToggleLocked();
                //SolarSystemObjectReferencesManager.INSTANCE.TreeViewManager.HandleHighlight(gameObject, planetData.infoUIManager.locked);
                ISAUIInteractionSynchronizer.INSTANCE.NotifyToggleActive(continentEcoreModel);
            });
        }

        public override void NotifyUpdateOnElement(ISANamedVisualElement e)
        {
            if (ISAUIInteractionSynchronizer.INSTANCE.isHovered(e))
            {
                foreach (Continent3D child in children)
                    child.gameObject.GetComponent<ISAUIInteractable>().onHoverStart.Invoke();
            }
            else
            {
                foreach (Continent3D child in children)
                    child.gameObject.GetComponent<ISAUIInteractable>().onHoverEnd.Invoke();
            }
        }



        public override void AssignMesh(GameObject parentObject)
        {
            base.AssignMesh(parentObject);

            foreach(Continent3D child in children)
            {
                child.AssignMesh(gameObject);
            }
        }

        /*public override float CalculateSize()
        {
            float result = 0f;
            foreach (ContinentData child in children)
                result += child.CalculateSize();
            return result;
        }*/



        public CityContinent3D getFirstContainedCity()
        {
            foreach (Continent3D child in children)
            {
                if (child is CityContinent3D)
                {
                    return (CityContinent3D)child;
                }
                else if (child is CompoundContinent3D)
                {
                    CityContinent3D foundCity = ((CompoundContinent3D)child).getFirstContainedCity();
                    if (foundCity != null)
                        return foundCity;
                }
            }

            return null;
        }
    }

}
