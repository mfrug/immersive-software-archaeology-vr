using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ISA.util;
using HullDelaunayVoronoi.Delaunay;
using HullDelaunayVoronoi.Hull;
using HullDelaunayVoronoi.Primitives;
using HullDelaunayVoronoi.Voronoi;
using EpForceDirectedGraph.cs;
using System.Linq;
using ISA.UI;
using ISA.Modelling;

namespace ISA.SolarOverview
{

    public class Planet3D : SolarElement3D
    {
        public static readonly float OVERSIZED_GENERATION_FACTOR = 10f;

        public static readonly float PLANET_RADIUS_MIN = 0.1f;
        public static readonly float PLANET_RADIUS_MAX = 0.4f;

        public static readonly float PLANET_MIN_RADIUS = 1.5f;
        public static readonly float PLANET_MAX_RADIUS = 3.0f;


        public ISAPlanet planetEcoreModel { get; private set; }
        public string qualifiedName { get; private set; }
        public string name { get; private set; }
        public float diameter { get; private set; }
        public List<Continent3D> rootContinents { get; private set; }
        public List<CityContinent3D> cityContinents { get; private set; }


        public int totalCities { get; private set; }
        public int totalBuildings { get; private set; }



        private VoronoiMesh3 voronoi;

        private float adjustmentToPlanetPiecesToCitiesRatio = 1f;
        private readonly float desiredPlanetPiecesToCitiesRatio = 4f;



        public GameObject gameObject { get; private set; }
        public GameObject PlanetHook;

        public List<Mesh> continentMeshes { get; private set; }
        private Material oceanMaterial;



        public PlanetHighlightManager highlightManager { get; private set; }






        public Planet3D(ISAPlanet planetEcoreModel)
        {
            this.planetEcoreModel = planetEcoreModel;
            name = planetEcoreModel.name;
            qualifiedName = planetEcoreModel.qualifiedName;

            totalBuildings = (int)planetEcoreModel.radius;
            diameter = PLANET_RADIUS_MIN + ((float)planetEcoreModel.radius / ModelStatistics.maxPlanetRadius) * (PLANET_RADIUS_MAX - PLANET_RADIUS_MIN);

            GameObject parent = Object.Instantiate(Resources.Load("SolarOverview/Planets/Planet") as GameObject);
            parent.transform.parent = SolarSystemManager.INSTANCE.SolarSystemGenerator.gameObject.transform;
            //parent.transform.localPosition = calculateRandomPosition(1f);
            parent.transform.localPosition = new Vector3(planetEcoreModel.position.x, planetEcoreModel.position.y, planetEcoreModel.position.z);
            parent.name = "Parent of " + name;

            gameObject = parent.transform.GetChild(0).gameObject;
            gameObject.SetActive(false);
            gameObject.name = name;

            ISATreeNode planetNode = GlobalTreeViewUICanvasHandler.INSTANCE.CreateEntry(planetEcoreModel, GlobalTreeViewUICanvasHandler.INSTANCE.rootNode);
            planetNode.ToggleExpanded();
            rootContinents = new List<Continent3D>();
            foreach(ISAContinent continentEcoreModel in planetEcoreModel.continents ?? Enumerable.Empty<ISAContinent>())
            {
                if (continentEcoreModel is ISACompoundContinent)
                    rootContinents.Add(new CompoundContinent3D((ISACompoundContinent)continentEcoreModel, this, null, planetNode));
                else if (continentEcoreModel is ISACityContinent)
                    rootContinents.Add(new CityContinent3D((ISACityContinent)continentEcoreModel, this, null, planetNode));
            }

            cityContinents = Continent3D.filterAllContainedCityContinents(rootContinents);
            totalCities = cityContinents.Count;

            ISAUIInteractable planetInteractable = gameObject.AddComponent<ISAUIInteractable>();
            planetInteractable.onHoverStart.AddListener(delegate { ISAUIInteractionSynchronizer.INSTANCE.NotifyHoverStart(planetEcoreModel); });
            planetInteractable.onHoverEnd.AddListener(delegate { ISAUIInteractionSynchronizer.INSTANCE.NotifyHoverEnd(planetEcoreModel); });
            planetInteractable.onTriggerPressed.AddListener(delegate {
                if(!ISAUIInteractionSynchronizer.INSTANCE.isActive(planetEcoreModel))
                    ISAUIInteractionSynchronizer.INSTANCE.NotifyToggleActive(planetEcoreModel);
            });
        }

        /// <summary>
        /// Finds a suitable position for this planet by checking where other planets were previsouly positioned.
        /// The method will recursively call itself if it fails to find a suitable position after a series of
        /// attempts, increasing the additionalSlackMultiplier parameter each time.
        /// </summary>
        /// <param name="additionalSlackMultiplyer">a multiplier for for outer orbit limit that is increased
        /// recursively if the method cannot find a suitable position</param>
        /// <returns></returns>
        private Vector3 calculateRandomPosition(float additionalSlackMultiplier)
        {
            for (int n = 0; n < 10000; n++)
            {
                float orbit = RandomFactory.INSTANCE.getRandomFloat(PLANET_MIN_RADIUS, PLANET_MAX_RADIUS * additionalSlackMultiplier);

                Vector3 position = RandomFactory.INSTANCE.getRandomNormalizedVector();
                position.y *= 1f / PLANET_MIN_RADIUS * 0.2f;
                position *= orbit;

                bool positionOkay = true;
                foreach(Planet3D otherPlanet in SolarSystemManager.INSTANCE.SolarSystemGenerator.planetDataList)
                {
                    if (Vector3.Distance(position, otherPlanet.gameObject.transform.position) < diameter * 2f + otherPlanet.diameter * 2f + 0.5f)
                    {
                        positionOkay = false;
                        break;
                    }
                }
                if (positionOkay)
                {
                    return position;
                }
            }

            Debug.LogWarning("Cannot find a suitable position for planet " + name + ". Retrying with more space.");
            return calculateRandomPosition(additionalSlackMultiplier * 1.5f);
        }






        public Mesh GetOptimalLandMassMesh(CityContinent3D city, params Mesh[] excludedMeshes)
        {
            if (continentMeshes == null)
                throw new System.NullReferenceException("Land mass meshes were tried to be accessed before they were generated!");

            // We want cities on to be located next to each other if they are contained in the same hierarchy level
            // or belong to the same branch along the hierarchy tree.
            // Search for a referenceCity, i.e., a city that should serve as reference point for finding a position.
            CityContinent3D referenceCity = findCityToAlignPositioningAt(city);
            if (referenceCity != null)
            {
                Vector3 referenceCityCenter = referenceCity.gameObject.GetComponent<MeshRenderer>().bounds.center;
                //VisualDebugger.DrawPoint(referenceCityCenter, Color.red);

                List<float> distances = new List<float>();
                float shortestDistance = float.MaxValue;
                for (int i = 0; i < continentMeshes.Count; i++)
                {
                    Vector3 continentCenter = continentMeshes[i].bounds.center + gameObject.transform.position;
                    //VisualDebugger.DrawPoint(continentCenter, Vector3.one * 0.05f, Color.blue);

                    distances.Add((continentCenter - referenceCityCenter).magnitude);
                    if (distances[i] < shortestDistance)
                        shortestDistance = distances[i];
                }

                int bestMeshIndex = distances.IndexOf(shortestDistance);
                Mesh mesh = continentMeshes[bestMeshIndex];
                continentMeshes.RemoveAt(bestMeshIndex);

                mesh.Optimize();
                mesh.RecalculateNormals();
                return mesh;
            }
            else
            {
                // assign ANY non-taken mesh
                Mesh mesh = continentMeshes[0];
                continentMeshes.RemoveAt(0);
                return mesh;
            }
        }

        private CityContinent3D findCityToAlignPositioningAt(Continent3D continent)
        {
            if (continent.isRootLevel())
                return null;

            CityContinent3D firstCitySibling = continent.getFirstCitySibling();

            if (firstCitySibling == null)
            {
                // "continent" is contained in a compound continent that has no direct city child
                // => find reference city in first compound child

                CompoundContinent3D firstCompoundSibling = continent.getFirstCompoundSibling();

                if (firstCompoundSibling == null)
                    return null;
                else if (firstCompoundSibling.Equals(continent))
                    return findCityToAlignPositioningAt(continent.parentContinent);
                else
                    return firstCompoundSibling.getFirstContainedCity();
            }
            if (firstCitySibling == continent)
            {
                // "continent" is the first city on this hierachy level to find a position on the planet
                // => find a city in the parents to orient positioning!

                return findCityToAlignPositioningAt(continent.parentContinent);
                //return getFirstCitySibling(continent.parentContinent);
            }
            else
            {
                return firstCitySibling;
            }
        }






        public IEnumerator GenerateMesh(bool debug)
        {
            PlanetHook = SpawnEmptyObject(gameObject.transform, Vector3.down * 9.5f * 0, "Planet Hook Object");

            GenerateCore();
            GenerateOcean();
            yield return null;

            if (SolarSystemManager.INSTANCE.generateLand) {
                if (debug)
                    Debug.Log("Actual City Continents: " + cityContinents.Count);

                List<Vertex3> vertices = CalculateMeshVertices(cityContinents.Count, OVERSIZED_GENERATION_FACTOR, debug);
                yield return null;

                voronoi = new VoronoiMesh3();
                voronoi.Generate(vertices);
                yield return null;

                // generate the land mass pieces
                yield return SolarSystemManager.INSTANCE.SolarSystemGenerator.StartCoroutine(
                    PlanetMeshGenerator.GenerateLandMassMeshes(
                        voronoi,
                        gameObject.transform.position,
                        OVERSIZED_GENERATION_FACTOR,
                        debug,
                        delegate(List<Mesh> result) { continentMeshes = result; }
                    )
                );
                /*continentMeshes.Sort(delegate (Mesh m1, Mesh m2)
                {
                    float volume1 = PlanetMeshGenerator.CalculateVolumeOfMesh(m1);
                    float volume2 = PlanetMeshGenerator.CalculateVolumeOfMesh(m2);
                    //return volume1.CompareTo(volume2);
                    return volume2.CompareTo(volume1);
                });*/
                //landMassMeshes.Reverse();

                if (continentMeshes.Count < desiredPlanetPiecesToCitiesRatio * cityContinents.Count)
                {
                    adjustmentToPlanetPiecesToCitiesRatio *= 1.5f;
                    yield return SolarSystemManager.INSTANCE.SolarSystemGenerator.StartCoroutine(GenerateMesh(debug));
                    yield break;
                }

                GameObject landmassParentObject = new GameObject("Land Mass");
                landmassParentObject.transform.parent = gameObject.transform;
                landmassParentObject.transform.localPosition = Vector3.zero;

                foreach (Continent3D continent in rootContinents)
                    continent.AssignMesh(landmassParentObject);

                gameObject.SetActive(true);
                gameObject.transform.position += Vector3.up * 50;
                foreach (CityContinent3D city in cityContinents)
                {
                    yield return null;
                    if (!city.SpawnBuildings(diameter, false, true))
                    {
                        // Buildings were not spawned correclty
                        // --> might clip through planet or not have been placed at all
                        Debug.LogWarning("Building Position not ideal within city \"" + name + "\". Will try again.");

                        // Skip a frame and try again, if it doesn't work then, nevermind
                        yield return null;
                        if(!city.SpawnBuildings(diameter, false, false))
                        {
                            Debug.LogWarning("> Building Position still not ideal within city \"" + name + "\". Will ignore.");
                        }
                        else
                        {
                            Debug.LogWarning("> Successfully repositioned buildings.");
                        }
                    }
                    city.SetupDiageticUI();
                }

                gameObject.transform.position -= Vector3.up * 50;
            }

            ResetSizeAndPosition();
            gameObject.GetComponent<FloatingBehavior>().Initialize();
            gameObject.SetActive(true);

            SetupDiageticUI();

            GenerateSphereClouds();
            GenerateAtmosphere();
            yield return null;
        }

        private void SetupDiageticUI()
        {
            ISAUIInteractionSynchronizer.INSTANCE.RegisterListener(planetEcoreModel, this);

            GameObject planetInfoUIObject = Object.Instantiate(Resources.Load("SolarOverview/UI/PlanetInfoCanvas") as GameObject);
            planetInfoUIObject.transform.GetChild(0).GetComponent<PlanetInfoCanvasManager>().Setup(this);

            GameObject highlightCircleObject = Object.Instantiate(Resources.Load("SolarOverview/UI/PlanetHighlightCircleCanvas") as GameObject);
            highlightCircleObject.name += " (DeleteOnClone)";
            highlightCircleObject.transform.parent = gameObject.transform;
            highlightCircleObject.transform.localPosition = Vector3.zero;
            highlightCircleObject.transform.localScale = OVERSIZED_GENERATION_FACTOR * 1.5f * Vector3.one;
            highlightManager = highlightCircleObject.GetComponent<PlanetHighlightManager>();
            highlightManager.SetName(name);
        }

        public override void NotifyUpdateOnElement(ISANamedVisualElement e)
        {
            if (highlightManager == null)
                // too early in the process, the planet and it's UI have not been generated
                return;

            if (ISAUIInteractionSynchronizer.INSTANCE.isActive(e))
            {
                highlightManager.SetActive();
                //oceanMaterial.SetInt("Highlighted", 1);
            }
            else if (ISAUIInteractionSynchronizer.INSTANCE.isHovered(e))
            {
                highlightManager.SetHovered();
                //oceanMaterial.SetInt("Highlighted", 1);
            }
            else
            {
                highlightManager.Hide();
                //oceanMaterial.SetInt("Highlighted", 0);
            }
        }



        public void ResetSizeAndPosition()
        {
            gameObject.transform.localPosition = Vector3.zero;
            gameObject.transform.localScale = (1f / OVERSIZED_GENERATION_FACTOR) * diameter * Vector3.one;
            gameObject.SetActive(true);

            gameObject.GetComponent<FloatingBehavior>().Reset();
        }



        private List<Vertex3> CalculateMeshVertices(int numberOfCityContinents, float radius, bool debug)
        {
            List<Vertex3> vertices = new List<Vertex3>();

            /*float minDist = radius / 3f;
            List<Vector3> cityContinentsVertices = PointsOnSphereDistribution.FillPointsRandomlyByDistance(minDist, radius / 3f);
            while(cityContinentsVertices.Count < numberOfCityContinents * 2)
            {
                minDist *= 0.8f;
                cityContinentsVertices = PointsOnSphereDistribution.FillPointsRandomlyByDistance(minDist, radius / 3f);
            }
            vertices.AddRange(ConvertToVertex3(cityContinentsVertices));*/

            //List<Vector3> cityContinentsVertices = PointsOnSphereDistribution.FillPointsRandomlyByNumber(Math.Max(numberOfCityContinents * 3, 20), radius / 3f);
            float distanceBetweenPoints = radius / 10f;
            List<Vector3> cityContinentsVertices = new List<Vector3>();
            while (cityContinentsVertices.Count < numberOfCityContinents * adjustmentToPlanetPiecesToCitiesRatio * desiredPlanetPiecesToCitiesRatio)
            {
                cityContinentsVertices = PointsOnSphereDistribution.FillPointsRandomlyByDistance(distanceBetweenPoints, radius / 3f);
                distanceBetweenPoints *= 0.8f;
            }
            vertices.AddRange(ConvertToVertex3(cityContinentsVertices));

            if (debug)
                Debug.Log("City Continent Vertices: " + cityContinentsVertices.Count);

            List<Vector3> innerVertices = PointsOnSphereDistribution.FillPointsRandomlyByDistance(radius / 10f, radius / 4f);
            vertices.AddRange(ConvertToVertex3(innerVertices));
            List<Vector3> outerVertices = PointsOnSphereDistribution.FillPointsRandomlyByDistance(radius / 5f, radius);
            vertices.AddRange(ConvertToVertex3(outerVertices));

            if (debug)
            {
                foreach (Vector3 v in cityContinentsVertices)
                    VisualDebugger.DrawPoint(gameObject.transform.position + v, Color.green);
                foreach (Vector3 v in innerVertices)
                    VisualDebugger.DrawPoint(gameObject.transform.position + v, Color.red);
                foreach (Vector3 v in outerVertices)
                    VisualDebugger.DrawPoint(gameObject.transform.position + v, Color.blue);
            }

            return vertices;
        }

        private IEnumerable<Vertex3> ConvertToVertex3(List<Vector3> verts)
        {
            List<Vertex3> outVertices = new List<Vertex3>(verts.Count);
            for (int i = 0; i < verts.Count; i++)
            {
                outVertices.Add(Vertex3ISA.FromVector3(verts[i]));
            }
            return outVertices;
        }




        private void GenerateCore()
        {
            GameObject oceanObject = Object.Instantiate(Resources.Load("SolarOverview/Planets/PlanetCore") as GameObject);
            oceanObject.transform.parent = gameObject.transform;
            oceanObject.transform.localPosition = Vector3.zero;
            oceanObject.transform.localScale = 1.29f * OVERSIZED_GENERATION_FACTOR * Vector3.one;
        }

        private void GenerateOcean()
        {
            GameObject oceanObject = Object.Instantiate(Resources.Load("SolarOverview/Planets/PlanetOcean") as GameObject);
            oceanObject.transform.parent = gameObject.transform;
            oceanObject.transform.localPosition = Vector3.zero;
            oceanObject.transform.localScale = 1.3f * OVERSIZED_GENERATION_FACTOR * Vector3.one;

            //oceanObject.GetComponent<MeshRenderer>().material.renderQueue = 3000;
            oceanMaterial = oceanObject.GetComponent<MeshRenderer>().material;

            ISAUIInteractable interactable = oceanObject.AddComponent<ISAUIInteractable>();
            interactable.onHoverStart.AddListener(delegate { ISAUIInteractionSynchronizer.INSTANCE.NotifyHoverStart(planetEcoreModel); });
            interactable.onHoverEnd.AddListener(delegate { ISAUIInteractionSynchronizer.INSTANCE.NotifyHoverEnd(planetEcoreModel); });
            interactable.onTriggerPressed.AddListener(delegate {
                if (!ISAUIInteractionSynchronizer.INSTANCE.isActive(planetEcoreModel))
                    ISAUIInteractionSynchronizer.INSTANCE.NotifyToggleActive(planetEcoreModel);
            });
        }

        /// <summary>
        /// Use a simple sphere with a transparent cloud shader for clouds
        /// </summary>
        private void GenerateSphereClouds()
        {
            GameObject[] cloudSpheres = new GameObject[2];
            for(int i=0; i<cloudSpheres.Length; i++)
            {
                cloudSpheres[i] = Object.Instantiate(Resources.Load("SolarOverview/Planets/PlanetClouds") as GameObject);
                cloudSpheres[i].transform.parent = gameObject.transform;
                cloudSpheres[i].transform.localPosition = Vector3.zero;
                cloudSpheres[i].transform.localScale = 1.5f * OVERSIZED_GENERATION_FACTOR * Vector3.one;
                cloudSpheres[i].GetComponent<MeshRenderer>().material.renderQueue = 3100;
            }
            cloudSpheres[1].transform.rotation = Quaternion.Euler(0, 90, 0);
        }

        /// <summary>
        /// Use a lot of simple quads to fake volumetric clouds: too heavy
        /// </summary>
        private void GenerateQuadClouds()
        {
            for(int i=0; i<50; i++)
            {
                GameObject cloudObject = Object.Instantiate(Resources.Load("SolarOverview/Planets/VolumetricCloud") as GameObject);
                cloudObject.transform.parent = gameObject.transform;
                cloudObject.transform.localPosition = Vector3.zero;
                cloudObject.transform.localScale = 1.55f * OVERSIZED_GENERATION_FACTOR * Vector3.one;
                //cloudObject.GetComponent<MeshRenderer>().material.renderQueue = 3100;
            }
        }

        private void GenerateAtmosphere()
        {
            GameObject atmosphereObject = Object.Instantiate(Resources.Load("SolarOverview/Planets/PlanetAtmosphere") as GameObject);
            atmosphereObject.transform.parent = gameObject.transform;
            atmosphereObject.transform.localPosition = Vector3.zero;
            atmosphereObject.transform.localScale = 1.6f * OVERSIZED_GENERATION_FACTOR * Vector3.one;
            //atmosphereObject.GetComponent<MeshRenderer>().material.renderQueue = 3200;

            /*
            GameObject flippedAtmosphereObject = Object.Instantiate(Resources.Load("SolarOverview/Planets/PlanetAtmosphere") as GameObject);
            flippedAtmosphereObject.transform.parent = gameObject.transform;
            flippedAtmosphereObject.transform.localPosition = Vector3.zero;
            flippedAtmosphereObject.transform.localScale = 1.55f * OVERSIZED_GENERATION_FACTOR * Vector3.one;

            Vector3[] normals = flippedAtmosphereObject.GetComponent<MeshFilter>().mesh.normals;
            for (int i = 0; i < normals.Length; i++)
            {
                normals[i] = -normals[i];
            }
            flippedAtmosphereObject.GetComponent<MeshFilter>().mesh.normals = normals;

            int[] triangles = flippedAtmosphereObject.GetComponent<MeshFilter>().mesh.triangles;
            for (int i = 0; i < triangles.Length; i += 3)
            {
                int t = triangles[i];
                triangles[i] = triangles[i + 2];
                triangles[i + 2] = t;
            }
            flippedAtmosphereObject.GetComponent<MeshFilter>().mesh.triangles = triangles;
            */
        }
    }


}