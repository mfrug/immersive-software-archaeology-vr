using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ISA.util;
using HullDelaunayVoronoi.Delaunay;
using HullDelaunayVoronoi.Hull;
using HullDelaunayVoronoi.Primitives;
using HullDelaunayVoronoi.Voronoi;
using EpForceDirectedGraph.cs;
using System.Linq;
using ISA.UI;
using ISA.Modelling;

namespace ISA.SolarOverview
{

    public abstract class SolarElement3D : ISASynchronizedUIElementListener
    {
        public abstract void NotifyUpdateOnElement(ISANamedVisualElement e);

        protected GameObject SpawnEmptyObject(Transform parentTransform, Vector3 localPosition, string name)
        {
            GameObject hookObject = new GameObject(name);
            hookObject.transform.parent = parentTransform;
            hookObject.transform.localPosition = localPosition;
            if (localPosition.magnitude > 0)
            {
                Vector3 up = localPosition.normalized;
                Vector3 sideways = Vector3.Cross(up, Vector3.right);
                hookObject.transform.rotation = Quaternion.LookRotation(sideways, up);
            }
            return hookObject;
        }

        protected GameObject SpawnEmptyObject(Transform parentTransform, Vector3 localPosition, Quaternion rotation, string name)
        {
            GameObject hookObject = SpawnEmptyObject(parentTransform, localPosition, name);
            hookObject.transform.rotation = rotation;
            return hookObject;
        }
    }

}