using ISA.UI;
using ISA.util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;



namespace ISA.SolarOverview
{


    public class BuildingOnPlanet3D : SolarElement3D
    {
        public ISABuilding ecoreModelBuilding;

        public GameObject gameObject;

        public BuildingInfoCanvasManager infoCanvasManager { get; private set; }

        private Material buildingHightlightMat = Resources.Load("SolarOverview/Buildings/BuildingHighlight") as Material;
        private Material buildingWallMat = Resources.Load("SolarOverview/Buildings/BuildingWall") as Material;
        private Material buildingRoofMat = Resources.Load("SolarOverview/Buildings/BuildingRoof") as Material;



        public BuildingOnPlanet3D(ISABuilding ecoreModelBuilding)
        {
            this.ecoreModelBuilding = ecoreModelBuilding;
        }



        public bool GenerateMesh(Transform parentCityContinentTransform, Vector3 cityCenterPosition, Vector3 cityNormal, Vector3 cityNormalAxis1, Vector3 cityNormalAxis2, float planetScale, bool debug, bool abortIfImperfect)
        {
            gameObject = UnityEngine.Object.Instantiate(Resources.Load("SolarOverview/Buildings/Building.Block") as GameObject);
            gameObject.transform.parent = parentCityContinentTransform;

            float cityAreaDiameter = 0.002f;
            gameObject.transform.localPosition = cityCenterPosition
                + cityAreaDiameter * (float)ecoreModelBuilding.position.x * cityNormalAxis1 * (1 / planetScale)
                + cityAreaDiameter * (float)ecoreModelBuilding.position.y * cityNormalAxis2 * (1 / planetScale);

            Vector3 buildingNormal = cityNormal;

            // position
            {
                Ray ray = new Ray(gameObject.transform.position + 3f * cityNormal, -cityNormal);
                if (debug)
                {
                    VisualDebugger.DrawPoint(ray.origin, Vector3.one * 0.1f, Color.cyan);
                }

                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    if (debug)
                    {
                        Debug.DrawLine(ray.origin, hit.point, Color.green, float.MaxValue);
                        VisualDebugger.DrawPoint(hit.point, Vector3.one * 0.1f, Color.magenta);
                    }
                    gameObject.transform.position = hit.point;
                    buildingNormal = hit.normal.normalized;
                }
                else
                {
                    if (debug)
                    {
                        Debug.DrawLine(ray.origin, ray.origin + ray.direction, Color.red, float.MaxValue);
                    }
                    if (abortIfImperfect)
                    {
                        return false;
                    }
                }
            }

            float height = 0f;
            float maxDiameter = 0f;
            float averageDiameter = 0f;
            foreach (ISAFloor ecoreModelFloor in ecoreModelBuilding.floors ?? Enumerable.Empty<ISAFloor>())
            {
                //GameObject floorObject = UnityEngine.Object.Instantiate(Resources.Load("SolarOverview/Planets/models/Building.Block") as GameObject);
                //floorObject.transform.parent = buildingObject.transform;
                //floorObject.transform.localScale = new Vector3((float)floor.diameter, (float)floor.height, (float)floor.diameter);
                //floorObject.transform.localPosition = new Vector3(0, currentHeight, 0);

                height += (float)ecoreModelFloor.height / 20f;
                averageDiameter += (float)ecoreModelFloor.diameter / (float)ecoreModelBuilding.floors.Length;
                if ((float)ecoreModelFloor.diameter > maxDiameter)
                    maxDiameter = (float)ecoreModelFloor.diameter;
            }

            gameObject.name = ecoreModelBuilding.qualifiedName + " (diameter=" + averageDiameter + ", height=" + height + ") (DeleteOnClone)";
            gameObject.transform.rotation = Quaternion.LookRotation(Vector3.Cross(buildingNormal, Vector3.up).normalized, buildingNormal);
            gameObject.transform.localScale = 0.005f * (1 / planetScale) * new Vector3((float)averageDiameter, (float)height, (float)averageDiameter);

            SpawnEmptyObject(gameObject.transform, Vector3.up * height, "Building Connection Hook");
            return true;
        }

        public void SetupDiageticUI(ISATreeNode parentContinentNode)
        {
            ISAUIInteractionSynchronizer.INSTANCE.RegisterListener(ecoreModelBuilding, this);
            GlobalTreeViewUICanvasHandler.INSTANCE.CreateEntry(ecoreModelBuilding, parentContinentNode);

            // Don't instantiate every info panel here, otherwise the startup slows down insanely!
        }



        internal void Destroy()
        {
            GameObject.Destroy(gameObject);
        }



        public override void NotifyUpdateOnElement(ISANamedVisualElement e)
        {
            if (e.Equals(ecoreModelBuilding))
            {
                if (infoCanvasManager == null)
                    ExplicitelyInstantiateInfoCanvas();

                if (ISAUIInteractionSynchronizer.INSTANCE.isActive(e) || ISAUIInteractionSynchronizer.INSTANCE.isHovered(e))
                {
                    Material[] mats = new Material[] { buildingHightlightMat, buildingHightlightMat };
                    gameObject.GetComponent<MeshRenderer>().materials = mats;
                }
                else
                {
                    Material[] mats = new Material[] { buildingRoofMat, buildingWallMat };
                    gameObject.GetComponent<MeshRenderer>().materials = mats;
                }
            }
        }

        public void ExplicitelyInstantiateInfoCanvas()
        {
            GameObject infoCanvas = UnityEngine.Object.Instantiate(Resources.Load("SolarOverview/UI/BuildingInfoCanvas") as GameObject);
            infoCanvasManager = infoCanvas.transform.GetChild(0).GetComponent<BuildingInfoCanvasManager>();
            infoCanvasManager.Setup(this);
        }
    }


}