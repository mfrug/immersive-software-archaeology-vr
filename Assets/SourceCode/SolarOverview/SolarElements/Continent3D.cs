using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ISA.SolarOverview
{

    public abstract class Continent3D : SolarElement3D
    {

        public Planet3D ContainingPlanet3D;
        public CompoundContinent3D parentContinent { get; protected set; }

        public string name { get; protected set; }

        public GameObject gameObject { get; protected set; }

        public Continent3D(Planet3D planetData, CompoundContinent3D parentContinent)
        {
            this.ContainingPlanet3D = planetData;
            this.parentContinent = parentContinent;

            gameObject = new GameObject();
        }



        //public abstract float CalculateSize();

        public virtual void AssignMesh(GameObject parentObject)
        {
            gameObject.transform.parent = parentObject.transform;
            gameObject.transform.localPosition = Vector3.zero;

            gameObject.name = name;
            gameObject.layer = 6;
        }



        public static List<CityContinent3D> filterAllContainedCityContinents(List<Continent3D> list)
        {
            List<CityContinent3D> result = new List<CityContinent3D>();

            foreach (Continent3D c in list)
            {
                if (c is CityContinent3D)
                    result.Add((CityContinent3D)c);
                else if (c is CompoundContinent3D)
                    result.AddRange(filterAllContainedCityContinents(((CompoundContinent3D)c).children));
                else
                    throw new NotSupportedException("Missing support for a continent type: " + c.GetType().ToString());
            }

            return result;
        }



        public bool isRootLevel()
        {
            return parentContinent == null;
        }

        public CompoundContinent3D getFirstCompoundSibling()
        {
            if (isRootLevel())
                return null;

            foreach (Continent3D sibling in parentContinent.children)
            {
                if (sibling is CompoundContinent3D)
                    return (CompoundContinent3D)sibling;
            }

            return null;
        }

        public CityContinent3D getFirstCitySibling()
        {
            if (isRootLevel())
                return null;

            foreach (Continent3D sibling in parentContinent.children)
            {
                if (sibling is CityContinent3D)
                    return (CityContinent3D)sibling;
            }

            return null;
        }
    }

}
