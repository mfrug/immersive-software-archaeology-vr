using ISA.Modelling;
using ISA.UI;
using ISA.util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Valve.VR.InteractionSystem;
using static Valve.VR.InteractionSystem.Hand;

namespace ISA.SolarOverview
{

    public class CityContinent3D : Continent3D, SolarSystemOverviewLoadingListener
    {

        private MeshRenderer meshRenderer;

        private static readonly int MIN_BUILDING_PREFABS = 1;
        private static readonly int MAX_BUILDING_PREFABS = 5;

        public GameObject cityCenterHookObject;
        public GameObject connectionHookObject;

        public ISACity cityEcoreModel { get; private set; }
        private ISATreeNode continentNode;

        public float size { get; private set; }
        public float highestBuildingFloor { get; private set; }

        //private Dictionary<CityContinent, float> relationsToOtherCities;
        public Dictionary<ISABuilding, BuildingOnPlanet3D> buildingMap { get; private set; }

        private Material landMat = Resources.Load("SolarOverview/Planets/materials/PlanetLandMat") as Material;
        private Material landHightlightMat = Resources.Load("SolarOverview/Planets/materials/PlanetLandHighlightMat") as Material;



        public CityContinent3D(ISACityContinent continentEcoreModel, Planet3D planetData, CompoundContinent3D parentContinent, ISATreeNode parentNode) : base(planetData, parentContinent)
        {
            buildingMap = new Dictionary<ISABuilding, BuildingOnPlanet3D>();
            this.cityEcoreModel = continentEcoreModel.city;
            name = continentEcoreModel.name ?? "Unnamed City";
            size = (cityEcoreModel.buildings ?? Array.Empty<ISABuilding>()).Length;

            foreach(ISABuilding buildingEcoreModel in cityEcoreModel.buildings)
            {
                foreach (ISAFloor floorEcoreModel in buildingEcoreModel.floors ?? Array.Empty<ISAFloor>())
                {
                    if ((float)floorEcoreModel.height > highestBuildingFloor)
                        highestBuildingFloor = (float)floorEcoreModel.height;
                }
            }

            continentNode = GlobalTreeViewUICanvasHandler.INSTANCE.CreateEntry(cityEcoreModel, parentNode);
            ISAUIInteractionSynchronizer.INSTANCE.RegisterListener(cityEcoreModel, this);

            ISAUIInteractable interactable = gameObject.AddComponent<ISAUIInteractable>();
            interactable.onHoverStart.AddListener(delegate { ISAUIInteractionSynchronizer.INSTANCE.NotifyHoverStart(cityEcoreModel); });
            interactable.onHoverEnd.AddListener(delegate { ISAUIInteractionSynchronizer.INSTANCE.NotifyHoverEnd(cityEcoreModel); });
            interactable.onTriggerPressed.AddListener(delegate {
                if (!ISAUIInteractionSynchronizer.INSTANCE.isActive(cityEcoreModel))
                    ISAUIInteractionSynchronizer.INSTANCE.NotifyToggleActive(cityEcoreModel);
            });

            SolarSystemManager.INSTANCE.RegisterLoadingProcessObserver(this);
        }

        public void SetupDiageticUI()
        {
            GameObject cityInfoUIObject = UnityEngine.Object.Instantiate(Resources.Load("SolarOverview/UI/CityInfoCanvas") as GameObject);
            cityInfoUIObject.transform.GetChild(0).GetComponent<CityInfoCanvasManager>().Setup(this);
        }

        public override void NotifyUpdateOnElement(ISANamedVisualElement e)
        {
            if(e.Equals(cityEcoreModel))
            {
                // Handle city continent
                if (ISAUIInteractionSynchronizer.INSTANCE.isActive(e) || ISAUIInteractionSynchronizer.INSTANCE.isHovered(e))
                    meshRenderer.material = landHightlightMat;
                else
                    meshRenderer.material = landMat;
            }
        }

        // little trick for instantly traveling to a planet without UI interaction
        private static int cityToShow = 15;// (new System.Random()).Next(0, 30);
        private static int currentCity = 0;
        private int thisCity = -1;
        public void NotifySolarSystemOverviewFinishedLoading()
        {
            thisCity = currentCity++;
            //if (thisCity == cityToShow)
            //    LoadCityLevel();
        }
        public void NotifySolarSystemSwitchedInsideOutsideMode() {}

        //private IEnumerator changeCityDelayed()
        //{
        //    yield return new WaitForSeconds(2);
        //    LoadCityLevel();
        //}

        public bool LoadCityLevel()
        {
            Debug.Log("Tring to Load City: " + name + " (index: " + thisCity + ")");
            return SceneHandler.INSTANCE.LoadCityImmersively(this);
            //SceneHandler.INSTANCE.StartCoroutine(changeCityDelayed());
        }



        /*public override float CalculateSize()
        {
            return size;
        }*/

        public override void AssignMesh(GameObject parentObject)
        {
            base.AssignMesh(parentObject);

            Mesh optimalMesh = null;
            while(!isValidMesh(optimalMesh))
                optimalMesh = ContainingPlanet3D.GetOptimalLandMassMesh(this);
            gameObject.AddComponent<MeshFilter>().mesh = optimalMesh;

            meshRenderer = gameObject.AddComponent<MeshRenderer>();
            gameObject.AddComponent<MeshCollider>().convex = true;

            meshRenderer.material = landMat;
        }

        /// <summary>
        /// Checks whether a given mesh is valid, i.e., whether it features a triangle with normals
        /// pointing to the outwards direction of the planet, so that cities can be placed on top.
        /// </summary>
        /// <param name="mesh">Mesh to be checked</param>
        /// <returns>true if the mesh is valid, false otherwise</returns>
        private bool isValidMesh(Mesh mesh)
        {
            if (mesh == null)
                return false;

            Vector3 directionToSpace = mesh.bounds.center;

            for (int i = 0; i < mesh.vertexCount; i++)
            {
                Vector3 vert = mesh.vertices[i];
                if (IsInnerVertex(vert))
                    continue;

                Vector3 normal = mesh.normals[i];
                if (Math.Abs(Vector3.Dot(directionToSpace.normalized, normal.normalized)) > 0.8f)
                    return true;
            }
            return false;
        }

        public bool SpawnBuildings(float planetScale, bool debug, bool abortIfImperfect)
        {
            Mesh mesh = gameObject.GetComponent<MeshFilter>().mesh;
            findLargestAreaOnContinent(mesh, out Vector3 averagedPosition, out Vector3 averagedNormal, out Vector3 averagedNormalAxis1, out Vector3 averagedNormalAxis2);

            cityCenterHookObject = SpawnEmptyObject(gameObject.transform, averagedPosition, Quaternion.LookRotation(averagedNormalAxis1, averagedNormal), "City Center");
            connectionHookObject = SpawnEmptyObject(gameObject.transform, averagedPosition + (0.5f / planetScale) * averagedNormal, Quaternion.LookRotation(averagedNormalAxis1, averagedNormal), "City Connection Hook");
            //Debug.DrawLine(planet.gameObject.transform.position + averagedPosition, planet.gameObject.transform.position + averagedPosition + averagedNormal, Color.red, float.MaxValue);

            //spawnFewAbstractBuildings(planetScale, averagedPosition, averagedNormal, averagedNormalAxis1, averagedNormalAxis2);

            foreach (ISABuilding ecoreModelBuilding in cityEcoreModel.buildings)
            {
                BuildingOnPlanet3D building3D = new BuildingOnPlanet3D(ecoreModelBuilding);
                if (!building3D.GenerateMesh(gameObject.transform, averagedPosition, averagedNormal, averagedNormalAxis1, averagedNormalAxis2, planetScale, debug, abortIfImperfect))
                {
                    foreach (ISABuilding buildingEcoreModel in buildingMap.Keys)
                        buildingMap[buildingEcoreModel].Destroy();
                    buildingMap.Clear();
                    return false;
                }

                buildingMap.Add(ecoreModelBuilding, building3D);
            }

            foreach (ISABuilding buildingEcoreModel in buildingMap.Keys)
            {
                BuildingOnPlanet3D building3D = buildingMap[buildingEcoreModel];
                building3D.SetupDiageticUI(continentNode);
            }

            return true;
        }

        private bool findLargestAreaOnContinent(Mesh mesh, out Vector3 centerPosition, out Vector3 centerNormal, out Vector3 centerNormalAxis1, out Vector3 centerNormalAxis2)
        {
            centerPosition = Vector3.zero;
            centerNormal = Vector3.up;
            centerNormalAxis1 = Vector3.right;
            centerNormalAxis2 = Vector3.forward;

            Dictionary<Vector3, HashSet<Vector3>> normalToVerticesMap = new Dictionary<Vector3, HashSet<Vector3>>();
            Dictionary<Vector3, float> normalToAreaMap = new Dictionary<Vector3, float>();
            for (int i = 0; i < mesh.triangles.Length; i += 3)
            {
                Vector3 vertex1 = mesh.vertices[mesh.triangles[i + 0]];
                Vector3 vertex2 = mesh.vertices[mesh.triangles[i + 1]];
                Vector3 vertex3 = mesh.vertices[mesh.triangles[i + 2]];
                if (IsInnerVertex(vertex1) || IsInnerVertex(vertex2) || IsInnerVertex(vertex3))
                    continue;

                Vector3 normal = Vector3.Cross(vertex2 - vertex1, vertex3 - vertex1).normalized;
                float area = Vector3.Cross(vertex1 - vertex2, vertex1 - vertex3).magnitude / 2;
                if(findVectorInCollection(normalToVerticesMap.Keys, normal, out Vector3 existingNormal)
                    && normalToVerticesMap.TryGetValue(existingNormal, out HashSet<Vector3> existingVertices))
                {
                    if (!findVectorInCollection(existingVertices, vertex1, out _))
                        existingVertices.Add(vertex1);
                    if (!findVectorInCollection(existingVertices, vertex2, out _))
                        existingVertices.Add(vertex2);
                    if (!findVectorInCollection(existingVertices, vertex3, out _))
                        existingVertices.Add(vertex3);

                    normalToAreaMap[existingNormal] += area;
                }
                else
                {
                    HashSet<Vector3> newVertices = new HashSet<Vector3>();
                    newVertices.Add(vertex1);
                    newVertices.Add(vertex2);
                    newVertices.Add(vertex3);
                    normalToVerticesMap.Add(normal, newVertices);
                    normalToAreaMap.Add(normal, area);
                }
            }
            if (normalToAreaMap.Count == 0)
            {
                Debug.LogError("Did not find any triangles in mesh for continent" + name);
                return false;
            }

            Vector3 largestPolyNormal = Vector3.zero;
            float largestPolyArea = 0;
            foreach (Vector3 normal in normalToAreaMap.Keys)
            {
                if (normalToAreaMap[normal] > largestPolyArea)
                {
                    largestPolyNormal = normal;
                    largestPolyArea = normalToAreaMap[normal];
                }
            }

            centerPosition = Vector3.zero;
            foreach (Vector3 vertex in normalToVerticesMap[largestPolyNormal])
            {
                centerPosition += vertex;
            }
            centerPosition /= normalToVerticesMap[largestPolyNormal].Count;

            centerNormal = largestPolyNormal;
            centerNormalAxis1 = Vector3.Cross(centerNormal, Vector3.up).normalized;
            centerNormalAxis2 = Vector3.Cross(centerNormal, centerNormalAxis1).normalized;

            return true;
        }

        private bool IsInnerVertex(Vector3 v)
        {
            return v.magnitude < Planet3D.OVERSIZED_GENERATION_FACTOR / 2;
        }

        private bool findVectorInCollection(ICollection<Vector3> set, Vector3 vecToFind, out Vector3 vecInSet)
        {
            vecInSet = Vector3.zero;
            foreach (Vector3 vec in set)
            {
                if (Vector3.Distance(vec, vecToFind) < Vector3.kEpsilon)
                {
                    vecInSet = vec;
                    return true;
                }
            }
            return false;
        }

        private void spawnFewAbstractBuildings(float planetScale, Vector3 averagedPosition, Vector3 averagedNormal, Vector3 averagedNormalAxis1, Vector3 averagedNormalAxis2)
        {
            float relativeCitySize = size / ModelStatistics.maxCitySize;
            int numberOfPrefabsToPlace = MIN_BUILDING_PREFABS + Mathf.Min(MAX_BUILDING_PREFABS - MIN_BUILDING_PREFABS, Mathf.FloorToInt(relativeCitySize * (MAX_BUILDING_PREFABS - MIN_BUILDING_PREFABS + 1)));

            Vector3[] buildingLocations = new Vector3[numberOfPrefabsToPlace];
            float buildingPlacementRadius = 0.2f;
            for (int i = 0; i < numberOfPrefabsToPlace; i++)
            {
                int tryCount = 0;
                while (true)
                {
                tryAgain:
                    if (tryCount > 100)
                    {
                        buildingPlacementRadius *= 1.5f;
                        tryCount = 0;
                    }

                    buildingLocations[i] = averagedPosition
                        + RandomFactory.INSTANCE.getRandomFloat(-buildingPlacementRadius, buildingPlacementRadius) * averagedNormalAxis1
                        + RandomFactory.INSTANCE.getRandomFloat(-buildingPlacementRadius, buildingPlacementRadius) * averagedNormalAxis2;
                    for (int j = 0; j < i; j++)
                    {
                        if ((buildingLocations[j] - buildingLocations[i]).magnitude < 0.15f / planetScale)
                        {
                            tryCount++;
                            goto tryAgain;
                        }
                    }
                    goto distancesFine;
                }
            distancesFine:

                int random = RandomFactory.INSTANCE.getRandomInt(1, 4);
                GameObject building = UnityEngine.Object.Instantiate(Resources.Load("SolarOverview/Buildings/Building" + random) as GameObject);
                building.transform.parent = gameObject.transform;
                building.transform.localPosition = buildingLocations[i];
                building.transform.localScale = Vector3.one * 0.05f * (1 / planetScale);
                building.transform.rotation = Quaternion.LookRotation(averagedNormalAxis2, averagedNormal);
            }
        }



        /*public void CreateMockupRelations()
        {
            foreach (CityContinent otherCity in planet.cityContinents)
            {
                if (this.Equals(otherCity))
                    continue;

                float value;
                if (otherCity.HasRelationTo(this))
                    value = otherCity.GetRelationTo(this);
                else
                    value = Mathf.Max(0f, RandomFactory.INSTANCE.getRandomFloat(-0.5f, 1f));

                relationsToOtherCities.Add(otherCity, value);
            }
        }*/



        /*public bool HasRelationTo(CityContinent other)
        {
            return relationsToOtherCities.ContainsKey(other);
        }

        public float GetRelationTo(CityContinent other)
        {
            return relationsToOtherCities[other];
        }*/
    }

}