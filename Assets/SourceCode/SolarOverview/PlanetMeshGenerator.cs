using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ISA.util;
using HullDelaunayVoronoi.Delaunay;
using HullDelaunayVoronoi.Hull;
using HullDelaunayVoronoi.Primitives;
using HullDelaunayVoronoi.Voronoi;
using System;

namespace ISA.SolarOverview
{

    public class PlanetMeshGenerator
    {
        private static readonly int meshesPerFrame = 10;


        public static IEnumerator GenerateLandMassMeshes(VoronoiMesh3 voronoi, Vector3 absoluteBasePosition, float radius, bool debug, Action<List<Mesh>> callback)
        {
            List<Mesh> meshes = new List<Mesh>();

            foreach (VoronoiRegion<Vertex3> region in voronoi.Regions)
            {
                bool draw = true;

                List<Vertex3> verts = new List<Vertex3>();

                foreach (DelaunayCell<Vertex3> cell in region.Cells)
                {
                    if (Vertex3ISA.ToVector3(cell.CircumCenter).magnitude > 1f * radius)
                    {
                        draw = false;
                        break;
                    }
                    else
                    {
                        verts.Add(cell.CircumCenter);
                    }
                }

                if (!draw) continue;

                //If you find the convex hull of the voronoi region it
                //can be used to make a triangle mesh.

                ConvexHull3 hull = new ConvexHull3();
                try
                {
                    hull.Generate(verts, false);
                }
                catch (IndexOutOfRangeException)
                {
                    // This happens sometimes when the hull cannot be
                    // calculated because points are too close together
                    Debug.Log("Prevented crash due to IndexOutOfRangeException for planet");
                    continue;
                }

                bool isInBounds = false;
                bool isInCenter = false;
                foreach (Simplex<Vertex3> s in hull.Simplexs)
                {
                    foreach (Vertex3 v in s.Vertices)
                    {
                        if (Vertex3ISA.ToVector3(v).magnitude < 0.1f * radius)
                        {
                            isInCenter = true;
                        }
                        if (Vertex3ISA.ToVector3(v).magnitude < 0.6f * radius)
                        {
                            isInBounds = true;
                        }
                    }
                }
                if (!isInBounds || isInCenter)
                    continue;

                if (debug)
                    VisualDebugger.DrawPoint(absoluteBasePosition + new Vector3(hull.Centroid[0], hull.Centroid[1], hull.Centroid[2]), Color.yellow);

                List<Vector3> vertexPositions = new List<Vector3>();
                List<Vector3> vertexNormals = new List<Vector3>();
                List<int> triangleVertexIndices = new List<int>();
                generateTriangles(hull, triangleVertexIndices, vertexPositions, vertexNormals, radius);

                Mesh mesh = new Mesh();
                mesh.SetVertices(vertexPositions);
                mesh.SetNormals(vertexNormals);
                mesh.SetTriangles(triangleVertexIndices, 0);

                Vector2[] vertexUVs = new Vector2[vertexPositions.Count];
                for (int i = 0; i < vertexUVs.Length; i++)
                    vertexUVs[i] = Vector2.zero;
                calculateUVs(vertexPositions, vertexNormals, triangleVertexIndices, vertexUVs, radius);
                mesh.SetUVs(0, vertexUVs);

                mesh.RecalculateBounds();
                mesh.RecalculateNormals();

                meshes.Add(mesh);
                if(meshes.Count % meshesPerFrame == 0)
                    yield return null;
            }

            if(debug)
                Debug.Log("Meshes: " + meshes.Count);

            callback(meshes);
        }

        private static bool isOutwardTriangle(bool allVerticesOuter, bool allVerticesInner)
        {
            return !allVerticesInner && allVerticesOuter;
        }

        private static bool isSidewardTriangle(bool allVerticesOuter, bool allVerticesInner)
        {
            return !allVerticesInner && !allVerticesOuter;
        }

        private static List<int> generateTriangles(ConvexHull3 hull, List<int> triangleVertexIndices, List<Vector3> vertexPositions, List<Vector3> vertexNormals, float radius)
        {
            int skippedIndices = 0;
            for (int i = 0; i < hull.Simplexs.Count; i++)
            {
                bool allVerticesInner = true;
                List<Vector3> tempPositions = new List<Vector3>();
                for (int j = 0; j < 3; j++)
                {
                    Vector3 v = new Vector3();
                    v.x = hull.Simplexs[i].Vertices[j].X;
                    v.y = hull.Simplexs[i].Vertices[j].Y;
                    v.z = hull.Simplexs[i].Vertices[j].Z;

                    if (!isInnerVertex(v, radius))
                        allVerticesInner = false;

                    tempPositions.Add(v);
                }
                if (allVerticesInner)
                {
                    skippedIndices += 3;
                    continue;
                }

                vertexPositions.AddRange(tempPositions);

                Vector3 n = new Vector3();
                n.x = hull.Simplexs[i].Normal[0];
                n.y = hull.Simplexs[i].Normal[1];
                n.z = hull.Simplexs[i].Normal[2];

                int triangleIndex1, triangleIndex2, triangleIndex3;
                if (hull.Simplexs[i].IsNormalFlipped)
                {
                    triangleIndex1 = i * 3 + 2 - skippedIndices;
                    triangleIndex2 = i * 3 + 1 - skippedIndices;
                    triangleIndex3 = i * 3 + 0 - skippedIndices;
                }
                else
                {
                    triangleIndex1 = i * 3 + 0 - skippedIndices;
                    triangleIndex2 = i * 3 + 1 - skippedIndices;
                    triangleIndex3 = i * 3 + 2 - skippedIndices;
                }

                triangleVertexIndices.Add(triangleIndex1);
                triangleVertexIndices.Add(triangleIndex2);
                triangleVertexIndices.Add(triangleIndex3);

                vertexNormals.Add(n);
                vertexNormals.Add(n);
                vertexNormals.Add(n);
            }
            return triangleVertexIndices;
        }

        private static bool isInnerVertex(Vector3 v, float radius)
        {
            return v.magnitude < 0.5f * radius;
        }



        private static void calculateUVs(List<Vector3> vertexPositions, List<Vector3> vertexNormals, List<int> triangleVertexIndices, Vector2[] vertexUVs, float radius)
        {
            Vector3 averageInsidePosition = Vector3.zero;
            Vector3 averageOutsidePosition = Vector3.zero;
            Vector3 averageOutsideNormal = Vector3.zero;

            int innerVertices = 0;
            int outerVertices = 0;
            for (int i = 0; i < triangleVertexIndices.Count; i++)
            {
                Vector3 v = vertexPositions[triangleVertexIndices[i]];
                if (isInnerVertex(v, radius))
                {
                    innerVertices++;
                    averageInsidePosition += v;
                }
                else
                {
                    outerVertices++;
                    averageOutsidePosition += v;
                    averageOutsideNormal += vertexNormals[triangleVertexIndices[i]];
                }
            }
            averageInsidePosition /= innerVertices;
            averageOutsidePosition /= outerVertices;

            averageOutsideNormal /= outerVertices;
            averageOutsideNormal.Normalize();
            Vector3 axisU = Vector3.Cross(averageOutsideNormal, Vector3.up).normalized;
            Vector3 axisV = Vector3.Cross(axisU, averageOutsideNormal).normalized;

            //Debug.DrawLine(averageOutsidePosition + 50f * Vector3.up, averageOutsidePosition + 50f * Vector3.up + averageOutsideNormal, Color.blue, float.MaxValue);
            //Debug.DrawLine(averageOutsidePosition + 50f * Vector3.up, averageOutsidePosition + 50f * Vector3.up + axisU, Color.red, float.MaxValue);
            //Debug.DrawLine(averageOutsidePosition + 50f * Vector3.up, averageOutsidePosition + 50f * Vector3.up + axisV, Color.green, float.MaxValue);

            for (int i = 0; i < triangleVertexIndices.Count; i += 3)
            {
                // Get vertices of outward facing triangle
                int index1 = triangleVertexIndices[i + 0];
                int index2 = triangleVertexIndices[i + 1];
                int index3 = triangleVertexIndices[i + 2];

                Vector3 v1 = vertexPositions[index1];
                Vector3 v2 = vertexPositions[index2];
                Vector3 v3 = vertexPositions[index3];

                if (isInnerVertex(v1, radius) || isInnerVertex(v3, radius) || isInnerVertex(v3, radius))
                {
                    vertexUVs[index1] = performCylindricProjection(v1, averageOutsidePosition, averageOutsideNormal, axisV, axisU);
                    vertexUVs[index2] = performCylindricProjection(v2, averageOutsidePosition, averageOutsideNormal, axisV, axisU);
                    vertexUVs[index3] = performCylindricProjection(v3, averageOutsidePosition, averageOutsideNormal, axisV, axisU);

                    if (Mathf.Abs(vertexUVs[index1].x - vertexUVs[index2].x) > Mathf.PI
                        || Mathf.Abs(vertexUVs[index1].x - vertexUVs[index3].x) > Mathf.PI
                        || Mathf.Abs(vertexUVs[index2].x - vertexUVs[index3].x) > Mathf.PI)
                    {
                        //Debug.Log(vertexUVs[index1].x + "  |  " + vertexUVs[index2].x + "  |  " + vertexUVs[index3].x);
                        if(vertexUVs[index1].x < 0)
                            vertexUVs[index1].x += Mathf.PI * 2;

                        if (vertexUVs[index2].x < 0)
                            vertexUVs[index2].x += Mathf.PI * 2;

                        if (vertexUVs[index3].x < 0)
                            vertexUVs[index3].x += Mathf.PI * 2;
                    }
                }
                else
                {
                    vertexUVs[index1] = performPlanarProjection(v1 - averageOutsidePosition, axisU, axisV);
                    vertexUVs[index2] = performPlanarProjection(v2 - averageOutsidePosition, axisU, axisV);
                    vertexUVs[index3] = performPlanarProjection(v3 - averageOutsidePosition, axisU, axisV);
                }
            }
        }

        private static Vector2 performCylindricProjection(Vector3 worldCoordinateToProject, Vector3 worldPointOnCylinderAxis, Vector3 cylinderUpwardDirection, Vector3 cylinderForwardDirection, Vector3 cylinderRightDirection)
        {
            float pointProjectionOnOutwardVector = Vector3.Dot(cylinderUpwardDirection, worldCoordinateToProject - worldPointOnCylinderAxis);
            Vector2 positionAlongOutwardVector = performPlanarProjection(worldCoordinateToProject - worldPointOnCylinderAxis, cylinderRightDirection, cylinderForwardDirection);
            float angle = Mathf.Atan2(positionAlongOutwardVector.y, positionAlongOutwardVector.x);

            return new Vector2(angle, pointProjectionOnOutwardVector);
        }

        private static Vector2 performPlanarProjection(Vector3 localCoordinate, Vector3 axisU, Vector3 axisV)
        {
            float lengthAlongU = localCoordinate.magnitude * Vector3.Dot(localCoordinate, axisU);
            float lengthAlongV = localCoordinate.magnitude * Vector3.Dot(localCoordinate, axisV);
            return new Vector2(lengthAlongU, lengthAlongV);
        }










        private static float SignedVolumeOfTriangle(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 o)
        {
            Vector3 v1 = p1 - o;
            Vector3 v2 = p2 - o;
            Vector3 v3 = p3 - o;

            return Vector3.Dot(Vector3.Cross(v1, v2), v3) / 6f; ;
        }

        public static float CalculateVolumeOfMesh(Mesh mesh)
        {
            float volume = 0;
            Vector3[] vertices = mesh.vertices;
            int[] triangles = mesh.triangles;

            Vector3 o = new Vector3(0f, 0f, 0f);
            // Computing the center mass of the polyhedron as the fourth element of each mesh
            for (int i = 0; i < triangles.Length; i++)
            {
                o += vertices[triangles[i]];
            }
            o = o / mesh.triangles.Length;

            // Computing the sum of the volumes of all the sub-polyhedrons
            for (int i = 0; i < triangles.Length; i += 3)
            {
                Vector3 p1 = vertices[triangles[i + 0]];
                Vector3 p2 = vertices[triangles[i + 1]];
                Vector3 p3 = vertices[triangles[i + 2]];
                volume += SignedVolumeOfTriangle(p1, p2, p3, o);
            }
            return Mathf.Abs(volume);
        }
    }

}