using ISA.Modelling;
using ISA.SolarOverview;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;

namespace ISA.UI
{

    public class CityConnectionCreator : AbstractConnectionCreator
    {

        public static CityConnectionCreator INSTANCE { private set; get; }

        void Start()
        {
            INSTANCE = this;
        }



        internal override void findTransformsForElements(string cityName1, string cityName2, out List<Transform> transforms, SearchQueryType queryType = SearchQueryType.EQUALS)
        {
            transforms = new List<Transform>();
            // Add dummies to be replaced later
            transforms.Add(transform);
            transforms.Add(transform);
            transforms.Add(transform);
            transforms.Add(transform);

            {
                // This happens when a reference is targeted to a satellite!
                // TODO visualize satellites and handle references here
                if (!cityName1.Contains(";") || !cityName2.Contains(";"))
                    return;
            }

            if (!PlayerLocationTracker.INSTANCE.PlayerIsCurrentlyInSpaceship && SceneHandler.INSTANCE.lastCityGenerator != null)
            {
                if (namesMatch(SceneHandler.INSTANCE.lastCityGenerator.city.ecoreModel, cityName1, queryType))
                {
                    transforms[0] = SceneHandler.INSTANCE.lastCityGenerator.gameObject.transform;
                    transforms[1] = transforms[0];
                }
                if (namesMatch(SceneHandler.INSTANCE.lastCityGenerator.city.ecoreModel, cityName2, queryType))
                {
                    transforms[2] = SceneHandler.INSTANCE.lastCityGenerator.gameObject.transform;
                    transforms[3] = transforms[2];
                }
            }

            if (transforms[0].Equals(transform) || transforms[1].Equals(transform))
            {
                CityContinent3D city1 = findCityInOverview(cityName1, queryType);
                if (city1 != null)
                {
                    transforms[0] = city1.connectionHookObject.transform;
                    transforms[1] = transforms[0];
                }
            }
            if (transforms[2].Equals(transform) || transforms[3].Equals(transform))
            {
                CityContinent3D city2 = findCityInOverview(cityName2, queryType);
                if (city2 != null)
                {
                    transforms[2] = city2.connectionHookObject.transform;
                    transforms[3] = transforms[2];
                }
            }
        }

        private CityContinent3D findCityInOverview(string name, SearchQueryType queryType)
        {
            foreach (Planet3D planet in SolarSystemManager.INSTANCE.SolarSystemGenerator.planetDataList)
            {
                foreach (CityContinent3D city in planet.cityContinents)
                {
                    if (namesMatch(city.cityEcoreModel, name, queryType))
                        return city;
                }
            }

            Debug.LogError("Could not find continent for city " + name);
            return null;
        }

        internal override string getGranularity()
        {
            return "city";
        }
    }


}