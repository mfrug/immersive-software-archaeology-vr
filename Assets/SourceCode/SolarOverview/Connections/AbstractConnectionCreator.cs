using ISA.Modelling;
using ISA.SolarOverview;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;

namespace ISA.UI
{

    public abstract class AbstractConnectionCreator : MonoBehaviour
    {



        //internal Dictionary<string, List<GameObject>> elementAndDepthToConnectionObjectsMapping = new Dictionary<string, List<GameObject>>();
        //internal string buildElementDepthKey(ISANamedVisualElement element, int depth)
        //{
        //    return element.name + ";" + depth;
        //}

        internal static readonly int MAX_SLICING_DEPTH = 3;

        internal readonly HttpClient client = new HttpClient();



        public static Color GetColorForConnectionDepthOld2(int depth)
        {
            float relativeDepth = (float)depth / MAX_SLICING_DEPTH;
            Color color = Color.HSVToRGB((2f / 3f) - (1f / 3f) * relativeDepth, 0.5f, 1f);
            color.a = 1f;
            return color;
        }

        public static Color GetColorForConnectionDepthOld(int depth)
        {
            float relativeDepth = (Mathf.Abs((float)depth) - 1f) / ((float)MAX_SLICING_DEPTH - 1f);
            Color color = Color.HSVToRGB((2f / 3f) * relativeDepth, 0.5f, 1f);
            color.a = 1f;
            return color;
        }

        public static Color GetColorForConnectionDepth(int depth)
        {
            float relativeDepth = (Mathf.Abs((float)depth) - 1f) / ((float)MAX_SLICING_DEPTH - 1f);
            Color color;
            if (depth < 0)
                color = Color.HSVToRGB(1f + relativeDepth / 6f, 0.5f, 1f);
            else
                color = Color.HSVToRGB(0.5f + relativeDepth / 6f, 0.5f, 1f);
            color.a = 1f;
            return color;
        }



        public int SetConnections(ISANamedVisualElement element, int depth, Action callBackSuccess, Action callBackFailure)
        {
            if (depth == 0)
                return 0;
            else if (depth > MAX_SLICING_DEPTH)
                depth = MAX_SLICING_DEPTH;
            else if (depth < -MAX_SLICING_DEPTH)
                depth = -MAX_SLICING_DEPTH;

            _ = setConnectionsAsync(ModelLoader.instance.VisualizationModel.name, element, depth, callBackSuccess, callBackFailure);
            return depth;
        }




        internal abstract string getGranularity();

        private async Task setConnectionsAsync(string systemName, ISANamedVisualElement element, int depth, Action callBackSuccess, Action callBackFailure)
        {
            string request = "http://localhost:9005/slicing/" +
                "?systemName=" + systemName +
                "&elementName=" + element.qualifiedName +
                "&slicingDepth=" + Math.Abs(depth) +
                "&granularity=" + getGranularity() +
                "&slicingDirection=" + (depth < 0 ? "backward" : "forward");
            //Debug.Log("Request: " + request);

            try
            {
                string response = await client.GetStringAsync(request);
                Debug.Log("Response: " + response);
                SlicingResult result = JsonUtility.FromJson<SlicingResult>(response);
                //SlicingResult result = JsonConvert.DeserializeObject<SlicingResult>(response);
                //DeleteConnections(element, depth);
                updateConnections(element, result, 0, depth);
                callBackSuccess();
            }
            catch (Exception e)
            {
                Debug.LogError(e.Data);
                Debug.LogError(e.Source);
                Debug.LogError(e.Message);
                callBackFailure();
                return;
            }
        }

        private void updateConnections(ISANamedVisualElement subjectElement, SlicingResult result, int currentDepth, int targetDepth)
        {
            if(result.linkedElements != null)
            {
                foreach (SlicingResult childResult in result.linkedElements)
                {
                    updateConnections(subjectElement, childResult, currentDepth + Math.Sign(targetDepth), targetDepth);

                    if (result.element.Equals(childResult.element))
                        continue;
                    if (currentDepth != targetDepth)
                        continue;

                    //Debug.Log("Inserting new connection from: " + element + " -> " + childResult.element + " | weight=" + totalWeight * childResult.localWeight + " | depth = " + currentDepth);
                    if (targetDepth > 0)
                        insertNewConnection(subjectElement, result.element, childResult.element, currentDepth, childResult.localWeight);
                    else
                        insertNewConnection(subjectElement, childResult.element, result.element, currentDepth, childResult.localWeight);
                }
            }
        }

        private void insertNewConnection(ISANamedVisualElement subjectElement, string elementName1, string elementName2, int depth, int weight)
        {
            findTransformsForElements(elementName1, elementName2, out List<Transform> transforms);
            if (transforms == null || transforms.Count < 2)
            {
                Debug.LogWarning("Did not find transforms for either one of these elements: " + elementName1 + ", " + elementName2 + "!");
                return;
            }
            else if(transforms.Count == 2)
            {
                Transform t = ConnectionLineLayouter.RegisterConnection(transforms[0], transforms[1], subjectElement, weight, depth, 1);
                if (t != null)
                {
                    // new connection created
                    t.parent = transform;
                }
            }
            else if (transforms.Count == 4)
            {
                for (int i = 0; i < transforms.Count - 1; i++)
                {
                    if (transforms[i].Equals(transforms[i + 1]))
                        continue;

                    Transform t = ConnectionLineLayouter.RegisterConnection(transforms[i], transforms[i + 1], subjectElement, weight, depth, i);
                    if (t != null)
                    {
                        // new connection created
                        t.parent = transform;
                    }
                }
            }
            else // if (transforms.Count != 2 && transforms.Count != 4)
            {
                Debug.LogError("Found " + transforms.Count + " transforms between \"" + elementName1 + "\" and \"" + elementName2 + "\", 2 or 4 are allowed...");
                return;
            }
        }

        abstract internal void findTransformsForElements(string elementName1, string elementName2, out List<Transform> transforms, SearchQueryType queryType = SearchQueryType.EQUALS);

        public enum SearchQueryType
        {
            EQUALS,
            EQUALS_IGNORECASE,

            STARTSWITH,
            STARTSWITH_IGNORECASE,

            CONTAINS,
            CONTAINS_IGNORECASE,
        }

        public bool namesMatch(ISANamedVisualElement element, string testedName, SearchQueryType queryType)
        {
            if(element == null || testedName == null)
                throw new NullReferenceException();

            if (queryType == SearchQueryType.EQUALS)
            {
                return string.Equals(element.qualifiedName, testedName, StringComparison.Ordinal);
            }
            if (queryType == SearchQueryType.EQUALS_IGNORECASE)
            {
                return string.Equals(element.qualifiedName, testedName, StringComparison.OrdinalIgnoreCase);
            }

            if (queryType == SearchQueryType.STARTSWITH)
            {
                if (element.name.StartsWith(testedName, StringComparison.Ordinal))
                    return true;
                return element.qualifiedName.StartsWith(testedName, StringComparison.Ordinal);
            }
            if (queryType == SearchQueryType.STARTSWITH_IGNORECASE)
            {
                if (element.name.StartsWith(testedName, StringComparison.OrdinalIgnoreCase))
                    return true;
                return element.qualifiedName.StartsWith(testedName, StringComparison.OrdinalIgnoreCase);
            }

            if (queryType == SearchQueryType.CONTAINS)
            {
                return element.qualifiedName.Contains(testedName);
            }
            if (queryType == SearchQueryType.CONTAINS_IGNORECASE)
            {
                return element.qualifiedName.ToLower().Contains(testedName.ToLower());
            }

            throw new Exception("invalid search query type");
        }
    }


}