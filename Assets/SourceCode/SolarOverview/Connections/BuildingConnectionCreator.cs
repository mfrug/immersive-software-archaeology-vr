using ISA.CityGeneration;
using ISA.Modelling;
using ISA.SolarOverview;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;

namespace ISA.UI
{

    public class BuildingConnectionCreator : AbstractConnectionCreator
    {

        public static BuildingConnectionCreator INSTANCE { private set; get; }

        void Start()
        {
            INSTANCE = this;
        }



        internal override void findTransformsForElements(string buildingName1, string buildingName2, out List<Transform> transforms, SearchQueryType queryType = SearchQueryType.EQUALS)
        {
            transforms = new List<Transform>();
            // Add dummies to be replaced later
            transforms.Add(transform);
            transforms.Add(transform);
            transforms.Add(transform);
            transforms.Add(transform);

            int elementsFound = 0;

            // Try to find the building in current city
            if(!PlayerLocationTracker.INSTANCE.PlayerIsCurrentlyInSpaceship && SceneHandler.INSTANCE.lastCityGenerator != null)
            {
                foreach(BuildingInCity3D building in SceneHandler.INSTANCE.lastCityGenerator.city.buildings)
                {
                    if (namesMatch(building.ecoreModel, buildingName1, queryType))
                    {
                        transforms[0] = building.gameObject.transform;
                        transforms[1] = transforms[0];
                        elementsFound++;
                    }
                    if (namesMatch(building.ecoreModel, buildingName2, queryType))
                    {
                        transforms[2] = building.gameObject.transform;
                        transforms[3] = transforms[2];
                        elementsFound++;
                    }
                }
            }

            if (elementsFound < 2)
            {
                // Then, go through planet overview to find buidings
                foreach (Planet3D planet in SolarSystemManager.INSTANCE.SolarSystemGenerator.planetDataList)
                {
                    // Go through planet overview
                    foreach (CityContinent3D city in planet.cityContinents)
                    {
                        if(transforms[0].Equals(transform) || transforms[1].Equals(transform))
                        {
                            BuildingOnPlanet3D buildingObject = GetBuildingGameObject(city, buildingName1, queryType);
                            if (buildingObject != null)
                            {
                                transforms[0] = buildingObject.gameObject.transform; // .GetChild(0); // actually there is a hook object, but it is doing weird stuff (is way too high)
                                transforms[1] = city.connectionHookObject.transform;
                                elementsFound++;
                            }
                        }

                        if (transforms[2].Equals(transform) || transforms[3].Equals(transform))
                        {
                            BuildingOnPlanet3D buildingObject = GetBuildingGameObject(city, buildingName2, queryType);
                            if (buildingObject != null)
                            {
                                transforms[2] = city.connectionHookObject.transform;
                                transforms[3] = buildingObject.gameObject.transform; // .GetChild(0); // actually there is a hook object, but it is doing weird stuff (is way too high)
                                elementsFound++;
                            }
                        }
                    }

                    if (elementsFound == 2)
                    {
                        break;
                    }
                }
            }

            if (elementsFound < 2)
            {
                transforms = null;
            }
            else if(transforms[1].Equals(transforms[2]))
            {
                // In case two buildings are in the same city, do not draw connection over city hook transform
                transforms[1] = transforms[0];
                transforms[2] = transforms[3];
            }
        }

        public BuildingOnPlanet3D GetBuildingGameObject(CityContinent3D city, string buildingName, SearchQueryType queryType)
        {
            foreach (ISABuilding building in city.buildingMap.Keys)
                if (namesMatch(building, buildingName, queryType))
                    return city.buildingMap[building];
            return null;
        }

        internal override string getGranularity()
        {
            return "building";
        }
    }


}