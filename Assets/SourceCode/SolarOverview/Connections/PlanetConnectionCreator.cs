using ISA.Modelling;
using ISA.SolarOverview;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;

namespace ISA.UI
{

    public class PlanetConnectionCreator : AbstractConnectionCreator
    {

        public static PlanetConnectionCreator INSTANCE { private set; get; }

        void Start()
        {
            INSTANCE = this;
        }



        internal override void findTransformsForElements(string planetName1, string planetName2, out List<Transform> transforms, SearchQueryType queryType = SearchQueryType.EQUALS)
        {
            transforms = new List<Transform>();
            // Add dummies to be replaced later
            transforms.Add(transform);
            transforms.Add(transform);

            if (!PlayerLocationTracker.INSTANCE.PlayerIsCurrentlyInSpaceship && SceneHandler.INSTANCE.VisitedCityOriginalData != null && SceneHandler.INSTANCE.VisitedPlanetEnlargedGameObject != null)
            {
                if(namesMatch(SceneHandler.INSTANCE.VisitedCityOriginalData.ContainingPlanet3D.planetEcoreModel, planetName1, queryType))
                {
                    transforms[0] = SceneHandler.INSTANCE.VisitedPlanetEnlargedGameObject.transform;
                }
                if (namesMatch(SceneHandler.INSTANCE.VisitedCityOriginalData.ContainingPlanet3D.planetEcoreModel, planetName2, queryType))
                {
                    transforms[1] = SceneHandler.INSTANCE.VisitedPlanetEnlargedGameObject.transform;
                }
            }

            foreach (Planet3D planet in SolarSystemManager.INSTANCE.SolarSystemGenerator.planetDataList)
            {
                if (transforms[0].Equals(transform) && namesMatch(planet.planetEcoreModel, planetName1, queryType))
                    transforms[0] = planet.PlanetHook.transform;
                if (transforms[1].Equals(transform) && namesMatch(planet.planetEcoreModel, planetName2, queryType))
                    transforms[1] = planet.PlanetHook.transform;
            }
        }

        internal override string getGranularity()
        {
            return "planet";
        }
    }

}