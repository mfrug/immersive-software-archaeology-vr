using AfGD;
using ISA.SolarOverview;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ISA.UI
{

    [RequireComponent(typeof(LineRenderer))]
    public class ConnectionLineLayouter : MonoBehaviour, SceneListener
    {

        private static Dictionary<Transform, Dictionary<Transform, ConnectionLineLayouter>> transformToConnectionMap = new Dictionary<Transform, Dictionary<Transform, ConnectionLineLayouter>>();

        public static Transform RegisterConnection(Transform transform1, Transform transform2, ISANamedVisualElement element, float weight, int depth, int segmentIndex)
        {
            if (transform1.Equals(transform2))
                return null;

            if(transformToConnectionMap.TryGetValue(transform1, out Dictionary<Transform, ConnectionLineLayouter> targetTransformToConnectionMap)) {
                if (targetTransformToConnectionMap.TryGetValue(transform2, out ConnectionLineLayouter connection)) {
                    // Connection already exists: increase weight, otherwise we are done
                    connection.weight += weight;
                    return null;
                }
                else
                {
                    // Connection does not exist, transform2 not registered as destination point for transform1 yet
                    ConnectionLineLayouter newConnection = CreateNewConnection(transform1, transform2, element, weight, depth, segmentIndex);
                    targetTransformToConnectionMap.Add(transform2, newConnection);
                    return newConnection.transform;
                }
            }
            else
            {
                // Connection does not exist, transform1 not registered as starting point yet
                ConnectionLineLayouter newConnection = CreateNewConnection(transform1, transform2, element, weight, depth, segmentIndex);

                Dictionary<Transform, ConnectionLineLayouter> newTargetTransformToConnectionMap = new Dictionary<Transform, ConnectionLineLayouter>();
                newTargetTransformToConnectionMap.Add(transform2, newConnection);

                transformToConnectionMap.Add(transform1, newTargetTransformToConnectionMap);
                return newConnection.transform;
            }
        }

        private static ConnectionLineLayouter CreateNewConnection(Transform transform1, Transform transform2, ISANamedVisualElement element, float weight, int depth, int segmentIndex)
        {
            GameObject connectionLine = new GameObject("Connection between " + transform1.parent.gameObject.name + " and " + transform2.parent.gameObject.name + ", weight = " + weight + ", depth = " + depth);
            connectionLine.AddComponent<LineRenderer>();

            ConnectionLineLayouter layouter = connectionLine.AddComponent<ConnectionLineLayouter>();
            layouter.Setup(transform1, transform2, element, weight, depth, segmentIndex);
            return layouter;
        }

        public static void DeleteAllConnections()
        {
            foreach(Dictionary<Transform, ConnectionLineLayouter> targetTransformToConnectionMap in transformToConnectionMap.Values)
            {
                foreach (ConnectionLineLayouter  connection in targetTransformToConnectionMap.Values)
                {
                    GameObject.Destroy(connection.gameObject);
                }
            }
            transformToConnectionMap.Clear();
        }

        public static void DeleteConnections(ISANamedVisualElement element, int depth)
        {
            List<Transform> toDeleteFromOuterDict = new List<Transform>();
            foreach (Transform t1 in transformToConnectionMap.Keys)
            {
                Dictionary<Transform, ConnectionLineLayouter> targetTransformToConnectionMap = transformToConnectionMap[t1];
                List<Transform> toDeleteFromInnerDict = new List<Transform>();
                foreach (Transform t2 in targetTransformToConnectionMap.Keys)
                {
                    ConnectionLineLayouter conn = targetTransformToConnectionMap[t2];
                    if (conn.element.Equals(element) && conn.depth.Equals(depth))
                    {
                        GameObject.Destroy(conn.gameObject);
                        toDeleteFromInnerDict.Add(t2);
                    }
                }
                foreach(Transform t2 in toDeleteFromInnerDict)
                {
                    targetTransformToConnectionMap.Remove(t2);
                }
                if(targetTransformToConnectionMap.Count == 0)
                {
                    toDeleteFromOuterDict.Add(t1);
                }
            }
            foreach (Transform t1 in toDeleteFromOuterDict)
            {
                transformToConnectionMap.Remove(t1);
            }
        }












        private LineRenderer lineRenderer;
        private Transform transform1, transform2;

        private ISANamedVisualElement element;
        private int depth;
        private float weight;

        private float animationStart;

        private int segmentIndex;
        private readonly float[] ANIMATION_DURATIONS = new float[] { 0.1f, 4f, 0.1f };
        private readonly int[] NUMBER_OF_POINTS_PER_SEGMENT = new int[] { 2, 16, 2 };

        private readonly float MIN_LINE_WIDTH = 5f;
        private readonly float LINE_WEIGHT_INCREMENT = 0.5f;

        private float lineWidth;




        private void Setup(Transform transform1, Transform transform2, ISANamedVisualElement element, float weight, int depth, int segmentIndex)
        {
            this.transform1 = transform1;
            this.transform2 = transform2;
            this.segmentIndex = segmentIndex;

            this.element = element;
            this.depth = depth;
            this.weight = weight;
            this.lineWidth = (MIN_LINE_WIDTH + 10*Mathf.Log(weight)) / 1000f;

            lineRenderer = GetComponent<LineRenderer>();
            lineRenderer.positionCount = NUMBER_OF_POINTS_PER_SEGMENT[segmentIndex];
            Color color = AbstractConnectionCreator.GetColorForConnectionDepth(depth);
            lineRenderer.startColor = color;
            lineRenderer.endColor = color;

            if (segmentIndex == 0)
            {
                lineRenderer.widthCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
            }
            if (segmentIndex == 2)
            {
                lineRenderer.widthCurve = AnimationCurve.EaseInOut(0, 1, 1, 0);
            }

            lineRenderer.material = GameObject.Instantiate(Resources.Load("SolarOverview/Connections/ConnectionMaterial") as Material);
            lineRenderer.receiveShadows = false;
            lineRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

            animationStart = Time.time;
            for (int i=0; i<segmentIndex; i++)
                animationStart += ANIMATION_DURATIONS[i];

            SceneHandler.INSTANCE.AddListener(this);
        }



        void Update()
        {
            CurveSegment curve = CalculateCurve();
            float curveLength = CalculateCurveLength(curve);

            lineRenderer.widthMultiplier = SolarSystemManager.INSTANCE.transform.lossyScale.x * lineWidth;
            lineRenderer.material.SetFloat("_Tiling", (1f / lineRenderer.widthMultiplier) * curveLength);
            lineRenderer.material.SetFloat("_MoveSpeed", 0.1f * (1f / lineRenderer.widthMultiplier) * curveLength);

            if (Time.time < animationStart)
            {
                // Animation has noch started yet, position all points within the sun mesh
                SetPositions(SolarSystemManager.INSTANCE.transform.position, 0, NUMBER_OF_POINTS_PER_SEGMENT[segmentIndex] - 1);
                return;
            }
            if (Time.time >= animationStart + ANIMATION_DURATIONS[segmentIndex])
            {
                // animation is over, show all segments in most efficient way
                SetPositions(curve, 0, NUMBER_OF_POINTS_PER_SEGMENT[segmentIndex] - 1);
            }
            else
            {
                // animate the segment
                float t_01 = (Time.time - animationStart) / ANIMATION_DURATIONS[segmentIndex];
                if (t_01 >= 1f)
                    return;
                int fullyExtendedPoints = (int)Mathf.Floor(t_01 * (float)(NUMBER_OF_POINTS_PER_SEGMENT[segmentIndex] - 1f));

                // set positions of segments that are already fully extended
                SetPositions(curve, 0, fullyExtendedPoints);

                // extend the remaining segments
                SetPositions(curve.Evaluate(t_01), fullyExtendedPoints + 1, NUMBER_OF_POINTS_PER_SEGMENT[segmentIndex] - 1);
            }
        }

        private void SetPositions(CurveSegment curve, int startIndexIncl, int endIndexIncl)
        {
            for (int i = startIndexIncl; i <= endIndexIncl; i++)
            {
                float u = (float)i / ((float)NUMBER_OF_POINTS_PER_SEGMENT[segmentIndex] - 1f);
                lineRenderer.SetPosition(i, curve.Evaluate(u));
            }
        }

        private void SetPositions(Vector3 fixedPosition, int startIndexIncl, int endIndexIncl)
        {
            for (int i = startIndexIncl; i <= endIndexIncl; i++)
            {
                lineRenderer.SetPosition(i, fixedPosition);
            }
        }



        private CurveSegment CalculateCurve()
        {
            if(NUMBER_OF_POINTS_PER_SEGMENT[segmentIndex] == 2)
                // dirty trick to distinguish between short intra-city connections and long range inter-planet connections
                return new CurveSegment(
                    transform1.position,
                    transform1.position,
                    transform2.position,
                    transform2.position,
                    CurveType.BEZIER);

            else
            {
                //float bezierHandleMultiplier = SolarSystemObjectReferencesManager.INSTANCE.transform.lossyScale.x;
                //float bezierHandleMultiplier = Mathf.Max(0.2f, Vector3.Distance(transform1.position, transform2.position) / 2f);
				float distance = Vector3.Distance(transform1.position, transform2.position);
                float bezierHandleMultiplier = Mathf.Sqrt(distance) + distance/2;

                return new CurveSegment(
                transform1.position,
                transform1.position + transform1.up.normalized * bezierHandleMultiplier,
                transform2.position + transform2.up.normalized * bezierHandleMultiplier,
                transform2.position,
                CurveType.BEZIER);
            }
        }

        private float CalculateCurveLength(CurveSegment curve)
        {
            float curveLength = 0;
            Vector3 lastSegmentPosition = curve.Evaluate(0);
            for (int i = 1; i < NUMBER_OF_POINTS_PER_SEGMENT[segmentIndex]; i++)
            {
                float u = (float)i / ((float)NUMBER_OF_POINTS_PER_SEGMENT[segmentIndex] - 1f);
                Vector3 nextSegmentPosition = curve.Evaluate(u);

                curveLength += Vector3.Distance(lastSegmentPosition, nextSegmentPosition);
                lastSegmentPosition = nextSegmentPosition;
            }
            return curveLength;
        }

        public void NotifyCityChangeTriggered()
        {
            DeleteAllConnections();
        }

        public void NotifyCityChangeComplete() {}
    }

}