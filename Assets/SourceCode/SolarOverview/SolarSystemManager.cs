using ISA.UI;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

namespace ISA.SolarOverview
{

    public class SolarSystemManager : MonoBehaviour, PlayerLocationListener
    {
        public static SolarSystemManager INSTANCE { get; private set; }

        void Awake()
        {
            INSTANCE = this;
        }



        // Propagation of loading finished event

        public bool IsLoaded { get; private set; }

        public List<SolarSystemOverviewLoadingListener> loadingProcessObservers = new List<SolarSystemOverviewLoadingListener>();
        public GameObject[] elementsToActivateOnLoadFinish;

        public void RegisterLoadingProcessObserver(SolarSystemOverviewLoadingListener newListener)
        {
            loadingProcessObservers.Add(newListener);
        }

        internal void NotifyFinishedLoading()
        {
            IsLoaded = true;
            foreach (SolarSystemOverviewLoadingListener obs in loadingProcessObservers)
                obs.NotifySolarSystemOverviewFinishedLoading();
            foreach (GameObject obj in elementsToActivateOnLoadFinish)
                obj.SetActive(true);

            PlayerLocationTracker.INSTANCE.AddListener(this);

            Debug.Log("Sun system visualization generation finished.");
        }



        public SolarSystemGenerator SolarSystemGenerator;

        public Transform InfoCanvasParentWorldSpace;
        public Transform InfoCanvasParentSecondaryHand;



        public bool generateLand;



        public void Reset()
        {
            // Do this to reset relative to user
            /*{
                Vector3 position = SceneHandler.INSTANCE.VRHeadCollider.transform.position + 3f * SceneHandler.INSTANCE.VRHeadCollider.transform.forward;
                position.y = SceneHandler.INSTANCE.VRHeadCollider.transform.position.y - 0.5f;
                transform.position = position;
                transform.rotation = Quaternion.identity;
            transform.localScale = Vector3.one * 0.7f;
            }*/

            if(PlayerLocationTracker.INSTANCE.PlayerIsCurrentlyInSpaceship || SceneHandler.INSTANCE.lastCityGenerator == null)
            {
                // Do this to center the planets right in the middle of the spaceship
                {
                    transform.localPosition = new Vector3(0, 2.5f, 0);
                    transform.localRotation = Quaternion.identity;
                    transform.localScale = Vector3.one * 1;
                }


                SolarSystemGenerator.Reset();
            }
            else
            {
                // Project planets into sky
                transform.localScale = Vector3.one * 75;
                transform.position = SceneHandler.INSTANCE.lastCityGenerator.gameObject.transform.position + Vector3.up * 250;
                transform.localRotation = Quaternion.identity;

                foreach (Planet3D planet in SolarSystemGenerator.planetDataList)
                {
                    if (SceneHandler.INSTANCE.VisitedCityOriginalData.ContainingPlanet3D.Equals(planet))
                    {
                        planet.gameObject.SetActive(false);

                        Debug.Log("gameObject.transform.parent.lossyScale: " + planet.gameObject.transform.parent.lossyScale.x);
                        Debug.Log("SceneHandler.INSTANCE.VisitedPlanetEnlargedGameObject.transform.lossyScale: " + SceneHandler.INSTANCE.VisitedPlanetEnlargedGameObject.transform.lossyScale.x);

                        planet.gameObject.transform.rotation = SceneHandler.INSTANCE.VisitedPlanetEnlargedGameObject.transform.rotation;
                        planet.gameObject.transform.localScale = Vector3.one * SceneHandler.INSTANCE.VisitedPlanetEnlargedGameObject.transform.lossyScale.x / planet.gameObject.transform.parent.lossyScale.x;
                        planet.gameObject.transform.position = SceneHandler.INSTANCE.VisitedPlanetEnlargedGameObject.transform.position;

                        break;
                    }
                }
            }
        }

        public void HideAllCanvases()
        {
            ISAUIInteractionSynchronizer.INSTANCE.ResetAndNotifyAll();
        }



        public void Scale(float newScale)
        {
            transform.localScale = newScale * Vector3.one;
        }



        public void OnPlayerEntersSpaceship()
        {
            Reset();
        }

        public void OnPlayerExitsSpaceship()
        {
            Reset();
        }
    }

    public interface SolarSystemOverviewLoadingListener
    {
        public void NotifySolarSystemOverviewFinishedLoading();
        public void NotifySolarSystemSwitchedInsideOutsideMode();
    }

}
