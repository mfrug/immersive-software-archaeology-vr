using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using ISA.util;
using System;

namespace ISA.SolarOverview
{
    [RequireComponent(typeof(Rigidbody))]
    public class FloatingBehavior : MonoBehaviour
    {
        public bool MaintainSpinAroundAxis = false;
        public bool DampSpinAroundAxis = true;

        private Vector3 initialVelocity;

        private Rigidbody rigidBody;



        public void Initialize()
        {
            initialVelocity = new Vector3(0, -1 * RandomFactory.INSTANCE.getRandomFloat(.1f, .2f), 0);

            rigidBody = GetComponent<Rigidbody>();
            //rigidBody.angularVelocity = initialVelocity;
            //rigidBody.mass = Mathf.PI * Mathf.Pow(radius, 3) * 4 / 3;
        }

        private void FixedUpdate()
        {
            if (rigidBody == null)
                return;

            Vector3 diff = gameObject.transform.parent.position - transform.position;
            float distance = diff.magnitude;
            if(distance > 0.001f)
            {
                // Add a force to the planet that pulls it back to the original position
                // However, slow it down by half of its current velocity direction, helps with overshooting
                rigidBody.AddForce(diff - 0.5f * rigidBody.velocity);

                if (Vector3.Dot(diff.normalized, rigidBody.velocity.normalized) < 0)
                {
                    // The planet is on its way "back" to the original position
                    if(rigidBody.velocity.magnitude > 0.01f)
                    {
                        // The planet has significant speed, it could use a brake
                        if (distance < 1f)
                        {
                            float brakeAcceleration = Mathf.Pow(1f - distance, 4);
                            rigidBody.AddForce(brakeAcceleration * (-2f) * rigidBody.velocity);
                        }
                    }
                }
            }

            // Maintain initial spin
            if(DampSpinAroundAxis)
            {
                if (rigidBody.angularVelocity.magnitude > 0.001f)
                    rigidBody.AddTorque(-0.05f * rigidBody.angularVelocity);
            }
            else if(MaintainSpinAroundAxis)
            {
                if (rigidBody.angularVelocity.magnitude > initialVelocity.magnitude)
                    rigidBody.AddTorque(-0.002f * rigidBody.angularVelocity);
                else
                    rigidBody.AddTorque(0.001f * initialVelocity);
            }
        }

        internal void Reset()
        {
            if(rigidBody != null)
            {
                rigidBody.velocity = Vector3.zero;
                rigidBody.angularVelocity = Vector3.zero;
            }
        }
    }
}
