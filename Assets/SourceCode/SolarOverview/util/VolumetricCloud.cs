using ISA.util;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumetricCloud : MonoBehaviour
{

    public GameObject[] cloudQuadPrefabs;
    public int density = 50;
    public float opacity = 0.1f;
    public float quadHeight = 0.5f;
    public float quadSpread = 0.05f;
    public float quadScale = 0.2f;
    public float rotationSpeed = 1f;

    private float rotationAngle;
    private Vector3 rotationAxis;



    void Start()
    {
        for(int i=0; i<density; i++)
        {
            int index6 = RandomFactory.INSTANCE.getRandomInt(0, cloudQuadPrefabs.Length-1);
            //GameObject newQuad = Instantiate<GameObject>(cloudQuadPrefabs[index]);
            GameObject newQuad = Instantiate<GameObject>(cloudQuadPrefabs[0]);
            newQuad.transform.parent = transform;
            newQuad.transform.localPosition = quadHeight * Vector3.back + RandomFactory.INSTANCE.getRandomFloat(quadSpread) * RandomFactory.INSTANCE.getRandomNormalizedVector();
            newQuad.transform.localScale = quadScale * RandomFactory.INSTANCE.getRandomFloat(0.8f, 1.2f) * Vector3.one;
            //newQuad.transform.rotation = Quaternion.Euler(RandomFactory.INSTANCE.getRandomFloat(-10, 10), RandomFactory.INSTANCE.getRandomFloat(-10, 10), RandomFactory.INSTANCE.getRandomFloat(360));
            newQuad.transform.rotation = RandomFactory.INSTANCE.getRandomRotation();

            newQuad.GetComponent<MeshRenderer>().material.SetFloat("opacity", RandomFactory.INSTANCE.getRandomFloat(0.8f, 1.2f) * opacity);
        }

        rotationAxis = RandomFactory.INSTANCE.getRandomNormalizedVector();
        rotationAngle = RandomFactory.INSTANCE.getRandomFloat(360);
        rotationSpeed *= RandomFactory.INSTANCE.getRandomFloat(0.8f, 1.2f);
    }



    void Update()
    {
        rotationAngle += Time.deltaTime * rotationSpeed;
        transform.rotation = Quaternion.AngleAxis(rotationAngle, rotationAxis);
    }
}
