using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HullDelaunayVoronoi.Primitives;


namespace ISA.SolarOverview
{

    public class Vertex3ISA : Vertex3
    {

        public static Vertex3 FromVector3(Vector3 v)
        {
            return new Vertex3(v.x, v.y, v.z);
        }

        public static Vector3 ToVector3(Vertex3 v)
        {
            return new Vector3(v.X, v.Y, v.Z);
        }

    }

}
