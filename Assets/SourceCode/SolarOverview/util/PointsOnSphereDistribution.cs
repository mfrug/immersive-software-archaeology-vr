using HullDelaunayVoronoi.Primitives;
using ISA.util;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ISA.SolarOverview
{

    public class PointsOnSphereDistribution
    {

        public static List<Vector3> FillPointsRandomlyByDistance(float minimumDistanceBetweenPoints, float sphereRadius = 1f)
        {
            return FillPointsRandomly(int.MaxValue, minimumDistanceBetweenPoints, sphereRadius);
        }



        public static List<Vector3> FillPointsRandomlyByNumber(int vertexCount, float sphereRadius = 1f)
        {
            float minimumDistanceBetweenPoints = sphereRadius / (vertexCount / 12f);
            return FillPointsRandomly(vertexCount, minimumDistanceBetweenPoints, sphereRadius);
        }



        public static List<Vector3> FillPointsRandomly(int vertexCount, float minimumDistanceBetweenPoints, float sphereRadius = 1f)
        {
            List<Vector3> vertices = new List<Vector3>();
            int failedChecks = 0;

            while (failedChecks < 1000 && vertices.Count < vertexCount)
            {
                Vector3 newPoint = RandomFactory.INSTANCE.getRandomNormalizedVector() * sphereRadius;

                int failedChecksBeforeTrying = failedChecks;
                foreach (Vector3 existingPoint in vertices)
                {
                    if (Vector3.Distance(existingPoint, newPoint) < minimumDistanceBetweenPoints)
                    {
                        failedChecks++;
                        break;
                    }
                }
                if (failedChecksBeforeTrying - failedChecks == 0)
                {
                    vertices.Add(newPoint);
                    failedChecks = 0;
                }
            }

            return vertices;
        }



        public static List<Vector3> EvenlyDistributeNumberOfPoints(int numberOfPoints, float sphereRadius = 1f)
        {
            List<Vector3> vertices = new List<Vector3>();
            float phi = Mathf.PI * (3f - Mathf.Sqrt(5f)); // golden angle in radians

            for (int i=0; i<numberOfPoints; i++)
            {
                Vector3 newPoint = Vector3.one;

                newPoint.y = 1f - (i / (float)(numberOfPoints - 1f)) * 2f; // y goes from 1 to -1
                float radius = Mathf.Sqrt(1f - newPoint.y * newPoint.y); // radius at y

                float theta = phi * (float) i; // golden angle increment

                newPoint.x = Mathf.Cos(theta) * radius;
                newPoint.z = Mathf.Sin(theta) * radius;

                vertices.Add(newPoint * sphereRadius);
            }

            return vertices;
        }
    }

}
