using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Http;
using System.Threading.Tasks;
using ISA.Modelling;
using System;
using ISA.UI;

public class OpenInIDEManager : MonoBehaviour
{

    private static readonly HttpClient client = new HttpClient();

    public AbstractInfoCanvasManager canvasManager;



    void Start()
    {
        
    }



    public void OpenInIDE()
    {
        _ = bla();
    }

    private async Task bla()
    {
        string request = "http://localhost:9005/open/" +
                "?systemName=" + ModelLoader.instance.VisualizationModel.name +
                "&classifierQualifiedName=" + canvasManager.ecoreElement.qualifiedName;

        try
        {
            string response = await client.GetStringAsync(request);
            Debug.Log("Response: " + response);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }



}
