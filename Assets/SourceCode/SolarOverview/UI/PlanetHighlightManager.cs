using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using ISA.SolarOverview;
using System;
using UnityEngine.UI;

namespace ISA.UI
{

    public class PlanetHighlightManager : AbstractHighlightManager
    {
        public void Start()
        {
            Hide();

            NameParent.localScale = new Vector3(
                NameParent.localScale.x * 0.002f / NameParent.lossyScale.x,
                NameParent.localScale.y * 0.002f / NameParent.lossyScale.y,
                NameParent.localScale.z * 0.002f / NameParent.lossyScale.z);
            NameParent.localPosition = new Vector3(
                NameParent.localPosition.x,
                330,// * titleTransform.localScale.y,
                NameParent.localPosition.z);
        }

        public void FixedUpdate()
        {
            transform.rotation = Quaternion.LookRotation(SceneHandler.INSTANCE.VRHeadCollider.transform.position - gameObject.transform.position);
        }

    }

}
