using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using ISA.SolarOverview;
using System;
using UnityEngine.UI;

namespace ISA.UI
{

    public abstract class AbstractHighlightManager : MonoBehaviour
    {
        public Transform NameParent;
        public TMP_Text NameText;
        public Image NameFrameImage;

        public Color TextHighlightColor;
        public Color TextPassiveColor;

        public Image CircleImage;

        public Color CircleHighlightColor;
        public Color CirclePassiveColor;



        public void SetName(string name)
        {
            NameText.text = name;
            RectTransform t = NameParent.GetComponent<RectTransform>();
            t.sizeDelta = new Vector2(NameText.preferredWidth + 30, t.rect.height);
        }



        public void SetHovered()
        {
            CircleImage.gameObject.SetActive(true);
            CircleImage.color = CirclePassiveColor;

            NameParent.gameObject.SetActive(false);
        }

        public void SetActive()
        {
            CircleImage.gameObject.SetActive(true);
            CircleImage.color = CircleHighlightColor;

            NameParent.gameObject.SetActive(true);
            NameText.color = TextHighlightColor;
            NameFrameImage.color = TextHighlightColor;
        }

        public void Hide()
        {
            CircleImage.gameObject.SetActive(false);

            NameParent.gameObject.SetActive(true);
            NameText.color = TextPassiveColor;
            NameFrameImage.color = TextPassiveColor;
        }
    }

}
