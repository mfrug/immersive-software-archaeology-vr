using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ISA.SolarOverview;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using ISA.util;
using ISA.VR;

namespace ISA.UI
{
    [RequireComponent(typeof(AttachToInteractable))]
    [RequireComponent(typeof(UIDisposableElement))]
    public abstract class AbstractInfoCanvasManager : MonoBehaviour, ISASynchronizedUIElementListener, SceneListener
    {
        private static List<AbstractInfoCanvasManager> ActiveCanvases = new List<AbstractInfoCanvasManager>();
        public static readonly int CanvasesPerRow = 3;

        [HideInInspector]
        public float BirthTimeStamp;

        public Canvas[] CanvasesToRegisterPointerCameraFor;

        private AttachToInteractable interactable;
        public GameObject grabHintObject;

        internal UIDisposableElement disposableComponent;
        //internal Transform targetTransform;

        [HideInInspector]
        public ISANamedVisualElement ecoreElement;
        [HideInInspector]
        public SlicingButtonManager.SlicingTargetLevel TargetLevel = SlicingButtonManager.SlicingTargetLevel.BUILDING;

        public TMP_Text NameText;
        public TMP_Text TagsText;

        public GameObject SlicingCanvas;
        public Button SlicingClearButton;

        public LocalTreeViewUICanvasHandler TreeViewCanvas;
        public CanvasToElementConnectionManager connectionManager;



        public static bool showGrabHint = true;
        private static List<GameObject> activeGrabHints = new List<GameObject>();
        public static void HideGrabHints()
        {
            showGrabHint = false;
            foreach (GameObject activeHint in activeGrabHints)
                activeHint.SetActive(false);
        }



        public void Setup()
        {
            if(TreeViewCanvas != null)
            {
                ISATreeNode root = SetupTreeView(ecoreElement, null);
                root.ToggleExpanded();
                TreeViewCanvas.AssignRootNode(root);
                TreeViewCanvas.UpdateTreeView();
            }

            ISAUIInteractionSynchronizer.INSTANCE.RegisterListenerAfterNextNotifyEvent(ecoreElement, this);

            disposableComponent = GetComponent<UIDisposableElement>();
            disposableComponent.OnDispose.AddListener(delegate {
                SlicingClearButton.onClick.Invoke();
                ISAUIInteractionSynchronizer.INSTANCE.NotifyActiveState(ecoreElement, false);
                ISAUIInteractionSynchronizer.INSTANCE.NotifyHovered(ecoreElement, false);
            });
        }

        private ISATreeNode SetupTreeView(ISANamedVisualElement el, ISATreeNode parentNode)
        {
            ISATreeNode node;
            if (el is ISACityContinent)
                // Skip city continents
                node = parentNode;
            else
                node = TreeViewCanvas.CreateEntry(el, parentNode);

            if (el is ISAPlanet)
            {
                foreach (ISAContinent continent in ((ISAPlanet)el).continents)
                    SetupTreeView(continent, node);
            }
            else if (el is ISACompoundContinent)
            {
                foreach (ISAContinent continent in ((ISACompoundContinent)el).subContinents)
                    SetupTreeView(continent, node);
            }
            else if (el is ISACityContinent)
            {
                SetupTreeView(((ISACityContinent)el).city, node);
            }
            else if (el is ISACity)
            {
                foreach (ISABuilding building in ((ISACity)el).buildings)
                    SetupTreeView(building, node);
            }
            return node;
        }




        public void Start()
        {
            GameObject parentInPrefab = transform.parent.gameObject;
            transform.parent = SolarSystemManager.INSTANCE.InfoCanvasParentWorldSpace;
            GameObject.Destroy(parentInPrefab);

            interactable = GetComponent<AttachToInteractable>();
            interactable.NotifyOnAttach.AddListener(delegate { UIDisposalBinManager.INSTANCE.Show(); HideGrabHints(); });
            interactable.NotifyOnDetach.AddListener(delegate { UIDisposalBinManager.INSTANCE.Hide(); });

            gameObject.SetActive(false);
            foreach (Canvas c in CanvasesToRegisterPointerCameraFor)
                c.worldCamera = SceneHandler.INSTANCE.VRPointerCamera;
        }

        public void FixedUpdate()
        {
            if(!ActiveCanvases.Contains(this))
            {
                layoutCanvasNextToElement();
            }
        }

        protected virtual void layoutCanvasNextToElement()
        {
            if (connectionManager.elementHookTransform == null)
            {
                Debug.LogError("Cannot layout info canvas next to element, as its hook transform is not available: " + connectionManager.ContainingCanvasManager.ecoreElement.qualifiedName);
                return;
            }

            Vector3 headToTargetTransform = SceneHandler.INSTANCE.VRHeadCollider.transform.position - connectionManager.elementHookTransform.position;
            Vector3 lookDirectionNormal = Vector3.Cross(headToTargetTransform.normalized, Vector3.up).normalized;
            
            if(PlayerLocationTracker.INSTANCE.PlayerIsCurrentlyInSpaceship)
                transform.position = connectionManager.elementHookTransform.position + lookDirectionNormal * Planet3D.OVERSIZED_GENERATION_FACTOR * connectionManager.elementHookTransform.lossyScale.x * (1f + headToTargetTransform.magnitude * 0.5f);
            else
                transform.position = connectionManager.elementHookTransform.position + lookDirectionNormal * Planet3D.OVERSIZED_GENERATION_FACTOR * connectionManager.elementHookTransform.lossyScale.x * (1f + Mathf.Pow(headToTargetTransform.magnitude, 0.1f));

            transform.rotation = Quaternion.LookRotation(transform.position - SceneHandler.INSTANCE.VRHeadCollider.transform.position);
            transform.localScale = Vector3.one * (0.3f + headToTargetTransform.magnitude * 0.65f);
        }



        public virtual void NotifyUpdateOnElement(ISANamedVisualElement e)
        {
            if (ISAUIInteractionSynchronizer.INSTANCE.isActive(e))
            {
                // is active, regardless of hover state
                if (ActiveCanvases.Contains(this))
                    return;

                BirthTimeStamp = Time.time;
                ActiveCanvases.Add(this);
                SlicingCanvas.SetActive(true);
                if(TreeViewCanvas != null)
                    TreeViewCanvas.gameObject.SetActive(true);

                OnSwitchToActive(e);
            }
            else
            {
                ActiveCanvases.Remove(this);
                SlicingCanvas.SetActive(false);
                if (TreeViewCanvas != null)
                    TreeViewCanvas.gameObject.SetActive(false);

                if (ISAUIInteractionSynchronizer.INSTANCE.isHovered(e))
                {
                    // is _not_ active, but hovered
                    OnSwitchToHovered(e);
                }
                else
                {
                    // Neither active nor hovered
                    gameObject.SetActive(false);
                }
            }
        }

        protected virtual void OnSwitchToActive(ISANamedVisualElement e)
        {
            gameObject.SetActive(true);
            PlaceAsChildOfHand();
            transform.localScale = Vector3.one / 2f;

            if (showGrabHint)
            {
                grabHintObject.SetActive(true);
                activeGrabHints.Add(grabHintObject);
            }
        }

        protected virtual void OnSwitchToHovered(ISANamedVisualElement e)
        {
            gameObject.SetActive(true);
            transform.parent = SolarSystemManager.INSTANCE.InfoCanvasParentWorldSpace;
            layoutCanvasNextToElement();
        }



        public void PlaceAsChildOfHand()
        {
            transform.parent = SolarSystemManager.INSTANCE.InfoCanvasParentSecondaryHand;
            for (int i=0; true; i++)
            {
                //float rightOffset = 0.35f * (i % CanvasesPerRow);
                //float forwardOffset = 0.1f * Mathf.Pow((i % CanvasesPerRow), 2);
                float angle = (1f - (i % CanvasesPerRow) / (float) (CanvasesPerRow-1)) * 70f;
                float x = 0.55f * Mathf.Cos(Mathf.Deg2Rad * angle);
                float z = 0.55f * Mathf.Sin(Mathf.Deg2Rad * angle);

                // Place above and benath hand, alternating whenever a full line of canvases was placed
                //float y = 0.3f * (2f * Mathf.Floor((float)(i % (2 * CanvasesPerRow)) / CanvasesPerRow) - 1f) * Mathf.Floor((float)(CanvasesPerRow + i) / (CanvasesPerRow * 2f));
                // Stack lines on top of each other
                float y = 0.3f * Mathf.Floor((float)(i) / (CanvasesPerRow));

                Vector3 positionOn01Sphere = new Vector3(x, y, z);
                Vector3 newLocalPos = positionOn01Sphere * 1f;

                transform.localPosition = newLocalPos;
                transform.localRotation = Quaternion.LookRotation(newLocalPos);
                //transform.rotation = Quaternion.LookRotation(transform.position - SceneHandler.INSTANCE.VRHeadPosition.position);
                bool conflictFound = false;
                foreach (AbstractInfoCanvasManager other in ActiveCanvases)
                {
                    if (this.Equals(other))
                        continue;
                    /*
                    Vector3 closestPointThis = this.CanvasCollider.ClosestPoint(other.CanvasCollider.bounds.center);
                    Vector3 closestPointOther = other.CanvasCollider.ClosestPoint(this.CanvasCollider.bounds.center);
                    if (Vector3.Dot(closestPointOther - closestPointThis, closestPointThis - this.CanvasCollider.bounds.center) < 0)
                    */
                    if (Vector3.Distance(transform.position, other.transform.position) < 0.1f)
                    {
                        conflictFound = true;
                        break;
                    }
                }
                if (!conflictFound)
                {
                    return;
                }
            }
        }



        public virtual void OnDispose() { }



        public virtual void NotifyCityChangeTriggered() { }

        public virtual void NotifyCityChangeComplete() { }
    }
}
