using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ISA.SolarOverview;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

namespace ISA.UI
{
    public class PlanetInfoCanvasManager : AbstractInfoCanvasManager
    {
        public TMP_Text CitiesText;
        public TMP_Text BuildingsText;

        public Planet3D planet { private set; get; }



        public void Setup(Planet3D planet)
        {
            this.planet = planet;
            ecoreElement = planet.planetEcoreModel;
            //targetTransform = planet.gameObject.transform;

            NameText.text = planet.name;
            TagsText.text = string.Join(", ", ecoreElement.tags);
            CitiesText.text = "" + planet.totalCities;
            BuildingsText.text = "" + planet.totalBuildings;

            base.Setup();
        }

    }
}
