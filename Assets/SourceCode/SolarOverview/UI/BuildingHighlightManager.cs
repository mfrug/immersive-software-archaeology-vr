using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using ISA.SolarOverview;
using System;
using UnityEngine.UI;

namespace ISA.UI
{

    public class BuildingHighlightManager : AbstractHighlightManager
    {
        public void Start()
        {
            Hide();
        }

        public void FixedUpdate()
        {
            Vector3 headOnSurface = SceneHandler.INSTANCE.VRHeadCollider.transform.position;
            headOnSurface.y = 0;
            Vector3 buildingOnSurface = gameObject.transform.position;
            buildingOnSurface.y = 0;
            Vector3 buildingToHeadOnSurface = headOnSurface - buildingOnSurface;
            transform.rotation = Quaternion.LookRotation(buildingToHeadOnSurface);

            NameParent.position = new Vector3(NameParent.position.x,
                Mathf.Max(gameObject.transform.position.y+1f, Mathf.Min(gameObject.transform.position.y+50f, SceneHandler.INSTANCE.VRHeadCollider.transform.position.y + 1f)),
                NameParent.position.z);

            Vector3 headToNamePlate = NameParent.position - SceneHandler.INSTANCE.VRHeadCollider.transform.position;
            NameParent.rotation = Quaternion.LookRotation(headToNamePlate);
            NameParent.localScale = Vector3.one * Mathf.Max(0.3f, Mathf.Min(6f, headToNamePlate.magnitude * 0.1f));
        }

    }

}
