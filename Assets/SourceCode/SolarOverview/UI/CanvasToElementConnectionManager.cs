using AfGD;
using ISA.SolarOverview;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ISA.UI
{
    public class CanvasToElementConnectionManager : MonoBehaviour, SceneListener, PlayerLocationListener
    {
        public int BezierResolution = 16;
        public Gradient color;

        public LineRenderer lineRenderer;

        public AbstractInfoCanvasManager ContainingCanvasManager;
        public Transform connectionRoot;

        public Transform elementHookTransform { private set; get; }
        private float elementHookBezierMultipler;



        void Start()
        {
            lineRenderer.positionCount = BezierResolution;
            lineRenderer.colorGradient = color;

            establishConnection();

            SceneHandler.INSTANCE.AddListener(this);
            PlayerLocationTracker.INSTANCE.AddListener(this);
        }

        private void establishConnection()
        {
            string elementQualifiedName = ContainingCanvasManager.ecoreElement.qualifiedName;
            if (ContainingCanvasManager.ecoreElement is ISAPlanet)
            {
                // Use the connection creator to find the hook transform
                // Simply pass the name twice and use first entry
                PlanetConnectionCreator.INSTANCE.findTransformsForElements(elementQualifiedName, elementQualifiedName, out List<Transform> transforms);
                if (transforms[0] == null)
                    Debug.LogError("Cannot find planet for " + elementQualifiedName);
                elementHookTransform = transforms[0];
                elementHookBezierMultipler = 0;
            }
            else if (ContainingCanvasManager.ecoreElement is ISACity)
            {
                // Use the connection creator to find the hook transform
                // Simply pass the name twice and use first entry
                CityConnectionCreator.INSTANCE.findTransformsForElements(elementQualifiedName, elementQualifiedName, out List<Transform> transforms);
                if (transforms[0] == null)
                    Debug.LogError("Cannot find city for " + elementQualifiedName);
                elementHookTransform = transforms[0];
                elementHookBezierMultipler = 1;
            }
            else if (ContainingCanvasManager.ecoreElement is ISABuilding)
            {
                // Use the connection creator to find the hook transform
                // Simply pass the name twice and use first entry
                BuildingConnectionCreator.INSTANCE.findTransformsForElements(elementQualifiedName, elementQualifiedName, out List<Transform> transforms);
                if (transforms[0] == null)
                    Debug.LogError("Cannot find building for " + elementQualifiedName);
                elementHookTransform = transforms[0];
                elementHookBezierMultipler = 1;
            }
            else
            {
                gameObject.SetActive(false);
            }
        }

        void Update()
        {
            float distance = Vector3.Distance(connectionRoot.position, elementHookTransform.position);

            lineRenderer.widthCurve = AnimationCurve.EaseInOut(0, 1, 1, 1+distance);

            //float bezierHandleMultiplier = Mathf.Max(0.2f, Vector3.Distance(connectionRoot.position, elementHookTransform.position) / 2f);
            //float bezierHandleMultiplier = Vector3.Distance(connectionRoot.position, elementHookTransform.position) / 2f;
            float bezierHandleMultiplier = Mathf.Sqrt(distance);
            CurveSegment curve = new CurveSegment(
                connectionRoot.position,
                connectionRoot.position + connectionRoot.forward.normalized * bezierHandleMultiplier,
                elementHookTransform.position + elementHookTransform.up.normalized /* SolarSystemManager.INSTANCE.transform.lossyScale.x*/ * bezierHandleMultiplier * elementHookBezierMultipler,
                elementHookTransform.position,
                CurveType.BEZIER);

            for (int i = 0; i < BezierResolution; i++)
            {
                float u = (float)i / ((float)BezierResolution - 1f);
                lineRenderer.SetPosition(i, curve.Evaluate(u));
            }
        }



        public void ToggleAttachmentPoint()
        {
            if(!ContainingCanvasManager.transform.parent.Equals(SolarSystemManager.INSTANCE.InfoCanvasParentWorldSpace))
            {
                ContainingCanvasManager.transform.parent = SolarSystemManager.INSTANCE.InfoCanvasParentWorldSpace;
            }
            else
            {
                ContainingCanvasManager.transform.parent = SolarSystemManager.INSTANCE.InfoCanvasParentSecondaryHand;
                ContainingCanvasManager.PlaceAsChildOfHand();
            }
        }

        public void NotifyCityChangeTriggered()
        {
            //if (ContainingCanvasManager.transform.parent.Equals(SolarSystemManager.INSTANCE.InfoCanvasParentWorldSpace))
            //    ToggleAttachmentPoint();
        }

        public void NotifyCityChangeComplete()
        {
            //if (ContainingCanvasManager.transform.parent.Equals(SolarSystemManager.INSTANCE.InfoCanvasParentWorldSpace))
            //    ToggleAttachmentPoint();
        }

        public void OnPlayerEntersSpaceship()
        {
            establishConnection();
        }

        public void OnPlayerExitsSpaceship()
        {
            establishConnection();
        }
    }
}

