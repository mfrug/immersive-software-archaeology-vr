using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ISA.SolarOverview;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

namespace ISA.UI
{
    public class CityInfoCanvasManager : AbstractInfoCanvasManager
    {
        public TMP_Text BuildingsText;
        public Button CityVisitButton;

        public CityContinent3D city { private set; get; }



        public void Setup(CityContinent3D city)
        {
            this.city = city;
            this.ecoreElement = city.cityEcoreModel;
            //this.targetTransform = city.connectionHookObject.transform;

            NameText.text = ecoreElement.name;
            if(ecoreElement.tags != null && ecoreElement.tags.Length > 0)
                TagsText.text = string.Join(", ", ecoreElement.tags);
            else
                TagsText.text = "no tags available";
            BuildingsText.text = "" + city.size;

            CityVisitButton.onClick.AddListener(delegate {
                bool loadingNow = city.LoadCityLevel();
            });
            CityVisitButton.gameObject.SetActive(false);

            base.Setup();
            SceneHandler.INSTANCE.AddListener(this);
        }



        protected override void OnSwitchToActive(ISANamedVisualElement e)
        {
            base.OnSwitchToActive(e);
            CityVisitButton.gameObject.SetActive(true);
        }

        protected override void OnSwitchToHovered(ISANamedVisualElement e)
        {
            base.OnSwitchToHovered(e);
            CityVisitButton.gameObject.SetActive(false);
        }



        public override void NotifyCityChangeTriggered()
        {
            base.NotifyCityChangeTriggered();
            CityVisitButton.transform.GetChild(0).gameObject.SetActive(false);
            CityVisitButton.transform.GetChild(1).gameObject.SetActive(true);
        }

        public override void NotifyCityChangeComplete()
        {
            base.NotifyCityChangeComplete();
            CityVisitButton.transform.GetChild(0).gameObject.SetActive(true);
            CityVisitButton.transform.GetChild(1).gameObject.SetActive(false);
        }

    }
}
