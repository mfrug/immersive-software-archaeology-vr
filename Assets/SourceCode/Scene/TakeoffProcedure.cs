using AfGD;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeoffProcedure
{
    public static Vector3 spaceShipStartPosition;

    private bool finalizedFlying = false;
    private bool initializedFlying = false;

    public IEnumerator StartTakeoffProcedure(Transform spaceshipTransform, GameObject enlargedPlanet)
    {
        CurveSegment flyUpCurve;
        {
            Vector3 startPoint = spaceshipTransform.position;
            Vector3 endPoint = spaceshipTransform.position + spaceshipTransform.up * 20f + spaceshipTransform.forward * 5f;
            flyUpCurve = new CurveSegment(
                startPoint,
                startPoint + spaceshipTransform.up * 5f,
                endPoint - spaceshipTransform.up * 1f - spaceshipTransform.forward * 2f,
                endPoint,
                CurveType.BEZIER);

            for (int i = 1; i <= 64; i++)
            {
                Debug.DrawLine(flyUpCurve.Evaluate((float)(i - 1) / 64), flyUpCurve.Evaluate((float)(i) / 64), Color.green, float.PositiveInfinity);
            }
        }

        CurveSegment flyForwardCurve;
        {
            Vector3 startPoint = flyUpCurve.Evaluate(1);
            Vector3 endPoint = startPoint + spaceshipTransform.forward * 1000f;
            flyForwardCurve = new CurveSegment(
                startPoint,
                startPoint + spaceshipTransform.forward * 10f,
                endPoint - spaceshipTransform.forward * 10f,
                endPoint,
                CurveType.BEZIER);

            for (int i = 1; i <= 64; i++)
            {
                Debug.DrawLine(flyForwardCurve.Evaluate((float)(i - 1) / 64), flyForwardCurve.Evaluate((float)(i) / 64), Color.blue, float.PositiveInfinity);
            }
        }

        AthmosphereVisibilityHandler athmosphere = null;
        foreach (Transform childTransform in enlargedPlanet.transform)
        {
            if (childTransform.TryGetComponent<AthmosphereVisibilityHandler>(out AthmosphereVisibilityHandler athm))
            {
                athmosphere = athm;
                break;
            }
        }

        float[] AnimationStageDurations = SceneHandler.INSTANCE.TakeoffAnimationStageDurations;
        float animationStartStamp = Time.time;
        while (true)
        {
            if (Time.time > animationStartStamp + AnimationStageDurations[0] + AnimationStageDurations[1] + AnimationStageDurations[2])
            {
                // Animation finished
                spaceshipTransform.transform.position = spaceShipStartPosition;
                SceneHandler.INSTANCE.SpaceSkyBox.position = spaceshipTransform.position;
                SceneHandler.INSTANCE.OnTakeoffProcedureFinished();
                break;
            }

            else if (Time.time - animationStartStamp < AnimationStageDurations[0])
            // Close the door
            {
                float t01 = (Time.time - animationStartStamp) / AnimationStageDurations[0];
                float eased_t01 = EasingFunctions.Crossfade(EasingFunctions.SmoothStart2, EasingFunctions.SmoothStop3, 0.5f, t01);
                SceneHandler.INSTANCE.SpaceshipDoorTransform.localRotation = Quaternion.Euler(0f, 0f, -110f * (1-eased_t01));

                SceneHandler.INSTANCE.SpaceshipDoorTeleportArea.locked = true;
            }

            else if (Time.time - animationStartStamp <= AnimationStageDurations[0] + AnimationStageDurations[1])
            // Fly up
            {
                if (!initializedFlying)
                {
                    SceneHandler.INSTANCE.NotifyFlyingStarts();
                    initializedFlying = true;
                }

                float t01 = (Time.time - animationStartStamp - AnimationStageDurations[0]) / AnimationStageDurations[1];
                float eased_t01 = EasingFunctions.Crossfade(EasingFunctions.SmoothStart2, EasingFunctions.SmoothStop5, 0.5f, t01);

                spaceshipTransform.position = flyUpCurve.Evaluate(eased_t01);
            }

            else if (Time.time - animationStartStamp <= AnimationStageDurations[0] + AnimationStageDurations[1] + AnimationStageDurations[2])
            // Fly away
            {
                if (!finalizedFlying)
                {
                    SceneHandler.INSTANCE.NotifyFlyingEnds();
                    finalizedFlying = true;
                }

                float t01 = (Time.time - animationStartStamp - AnimationStageDurations[0] - AnimationStageDurations[1]) / AnimationStageDurations[2];
                float eased_t01 = EasingFunctions.Crossfade(EasingFunctions.SmoothStart5, EasingFunctions.Linear, 0.5f, t01);

                spaceshipTransform.position = flyForwardCurve.Evaluate(eased_t01);
                athmosphere.UpdateShaders(false);
            }

            SceneHandler.INSTANCE.SpaceSkyBox.position = spaceshipTransform.position;

            // Wait 1 frame
            yield return null;
        }
    }

}
