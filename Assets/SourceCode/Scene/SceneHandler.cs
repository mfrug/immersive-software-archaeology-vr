using AfGD;
using ISA.CityGeneration;
using ISA.SolarOverview;
using ISA.util;
using ISA.VR;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR;
using Valve.VR.InteractionSystem;
using static Valve.VR.InteractionSystem.Hand;

public class SceneHandler : MonoBehaviour
{
    public static SceneHandler INSTANCE { get; private set; }

    void Awake()
    {
        INSTANCE = this;
    }


    public LayerMask SpaceshipLandingPositionLayerMask;

    public Collider VRHeadCollider;
    public Pointer VRPointer;
    public Camera VRPointerCamera;

    public Transform SpaceSkyBox;
    public Light DirectionalSunLight;
    public Collider SpaceshipOuterShellCollider;
    public MeshRenderer SpaceshipOuterShellRenderer;
    public Transform SpaceshipDoorTransform;
    public TeleportArea SpaceshipDoorTeleportArea;

    public Transform WorlSpaceCanvasesParent;
    public GameObject[] ToDisableWhileFlying;

    // Animation steps: planet approaches (quickly), spaceship lands, door opens
    public float[] LandingAnimationStageDurations = new float[] { 2f, 8f, 1f };
    // Animation steps: door closes, spaceship gains height (vertically), spaceship flies off
    public float[] TakeoffAnimationStageDurations = new float[] { 1f, 1.5f, 2.5f };

    public GameObject spaceShipLocation3DIcon { get; private set; }

    public float PlanetSize = 100f;
    public CityGenerator lastCityGenerator { get; internal set; }
    public CityContinent3D VisitedCityOriginalData { get; private set; }
    public GameObject VisitedPlanetEnlargedGameObject { get; private set; }

    private bool currentlyInSceneTransition = false;

    private List<SceneListener> listenersToRemoveSafely = new List<SceneListener>();
    private List<SceneListener> listeners = new List<SceneListener>();

    public void AddListener(SceneListener newListener)
    {
        listeners.Add(newListener);
    }
    public void RemoveListener(SceneListener listener)
    {
        listeners.Remove(listener);
    }
    public void RemoveListenerSafely(SceneListener listener)
    {
        listenersToRemoveSafely.Remove(listener);
    }




    void Start()
    {
        spaceShipLocation3DIcon = Instantiate(Resources.Load("UI/3D/spaceship-icon") as GameObject);
        spaceShipLocation3DIcon.name += " (DeleteOnClone)";
        spaceShipLocation3DIcon.transform.parent = SolarSystemManager.INSTANCE.transform;
        spaceShipLocation3DIcon.SetActive(false);

        lastCityGenerator = null;
        VisitedCityOriginalData = null;
        VisitedPlanetEnlargedGameObject = null;
        currentlyInSceneTransition = false;

        TakeoffProcedure.spaceShipStartPosition = transform.position;
    }



    public bool LoadCityImmersively(CityContinent3D city3D)
    {
        if (!PlayerLocationTracker.INSTANCE.PlayerIsCurrentlyInSpaceship)
            return false;

        if(currentlyInSceneTransition)
            return false;

        currentlyInSceneTransition = true;
        VisitedCityOriginalData = city3D;

        foreach (SceneListener listener in listeners)
            listener.NotifyCityChangeTriggered();

        foreach (SceneListener listenerToRemove in listenersToRemoveSafely)
            listeners.Remove(listenerToRemove);

        if (VisitedPlanetEnlargedGameObject != null)
        {
            TakeoffProcedure takeoffManager = new TakeoffProcedure();
            StartCoroutine(takeoffManager.StartTakeoffProcedure(transform, VisitedPlanetEnlargedGameObject));
        }
        else
        {
            LandingProcedure landingManager = new LandingProcedure();
            StartCoroutine(landingManager.StartLandingProcedure(city3D, transform, PlanetSize));
        }

        return true;
    }

    public void NotifyFlyingStarts()
    {
        foreach (GameObject o in ToDisableWhileFlying)
        {
            o.SetActive(false);
        }
        foreach (Transform child in WorlSpaceCanvasesParent)
        {
            child.gameObject.SetActive(false);
        }
    }

    public void NotifyFlyingEnds()
    {
        foreach (GameObject o in ToDisableWhileFlying)
        {
            o.SetActive(true);
        }
        spaceShipLocation3DIcon.transform.parent = VisitedCityOriginalData.cityCenterHookObject.transform;
        spaceShipLocation3DIcon.transform.localPosition = Vector3.up * 0.5f;
        spaceShipLocation3DIcon.transform.localScale = Vector3.one / spaceShipLocation3DIcon.transform.parent.lossyScale.x / 200f;
        spaceShipLocation3DIcon.transform.localRotation = Quaternion.identity;
        spaceShipLocation3DIcon.SetActive(true);
    }

    public void OnLandingProcedureFinished(GameObject visitedPlanet)
    {
        VisitedPlanetEnlargedGameObject = visitedPlanet;
        currentlyInSceneTransition = false;

        foreach (SceneListener listener in listeners)
            listener.NotifyCityChangeComplete();
        foreach (SceneListener listenerToRemove in listenersToRemoveSafely)
            listeners.Remove(listenerToRemove);
    }

    public void OnTakeoffProcedureFinished()
    {
        GameObject.Destroy(VisitedPlanetEnlargedGameObject);
        LandingProcedure landingManager = new LandingProcedure();
        StartCoroutine(landingManager.StartLandingProcedure(VisitedCityOriginalData, transform, PlanetSize));
    }

    /*
    public static void LoadSolarSystemSceneAdditive()
    {
        SteamVR_LoadLevel loader = new GameObject("loader").AddComponent<SteamVR_LoadLevel>();
        loader.levelName = "SolarSystemScene";
        loader.loadAsync = true;
        loader.loadAdditive = true;
        loader.showGrid = false;
        loader.fadeOutTime = 0f;
        loader.fadeInTime = 0f;
        loader.backgroundColor = new Color(0, 0, 0, 0);
        loader.Trigger();
    }
    */



    [System.Obsolete("This method loads the city as a new Unity scene, causing a transition that interferes with users' immersion, use LoadCityImmersively(..) instead")]
    public void LoadCityInSeperateUnityScene(ISACity cityEcoreModel)
    {
        Player player = Player.instance;

        List<GameObject> attachedObjects = new List<GameObject>();
        foreach (Hand hand in player.hands)
            foreach (AttachedObject attachedObject in hand.AttachedObjects)
                attachedObjects.Add(attachedObject.attachedObject);
        foreach (Hand hand in player.hands)
            foreach (GameObject attachedObject in attachedObjects)
                hand.DetachObject(attachedObject, true);

        GameObject.Destroy(Teleport.instance.gameObject);

        // Unity's default scene manager, don't use, Valve made their VR version
        //SceneManager.LoadScene(1, LoadSceneMode.Single);

        SteamVR_LoadLevel loader = new GameObject("loader").AddComponent<SteamVR_LoadLevel>();
        loader.levelName = "CityScene";
        loader.showGrid = false;
        loader.fadeOutTime = 0.5f;
        loader.fadeInTime = 2f;
        loader.backgroundColor = new Color(0, 0, 0, 1);
        loader.loadingScreen = Resources.Load<Texture2D>("UI/Logos/logo-L-bright.png");
        loader.Trigger();
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}

public interface SceneListener
{
    public void NotifyCityChangeTriggered();
    public void NotifyCityChangeComplete();
}
