using AfGD;
using ISA.CityGeneration;
using ISA.SolarOverview;
using ISA.util;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class LandingProcedure
{

    private CurveSegment spaceShipPositionCurve;

    private Vector3 planetStartPosition;
    private Vector3 planetEndPosition;

    private Transform cityCenter;
    private Transform cityConnectionHook;

    private Vector3 spaceshipDestinationPoint;
    private Vector3 spaceshipDestinationDirection;

    private GameObject enlargedPlanetClone;

    private bool finalizedFlying = false;
    private bool initializedFlying = false;
    private ReflectionProbe cityReflectionProbe;

    private AthmosphereVisibilityHandler athmosphere;



    public IEnumerator StartLandingProcedure(CityContinent3D city3D, Transform spaceshipTransform, float PlanetSize)
    {
        //DirectionalSunLight.gameObject.SetActive(true);
        //DirectionalSunLight.intensity = 0f;
        //DirectionalSunLight.transform.rotation = Quaternion.LookRotation(city3D.ContainingPlanet3D.gameObject.transform.localPosition);

        planetStartPosition = Vector3.forward * 5000f;
        planetEndPosition = Vector3.forward * PlanetSize * 20f + Vector3.down * city3D.connectionHookObject.transform.localPosition.magnitude * PlanetSize * 2f;

        string originalName = city3D.gameObject.name;
        city3D.gameObject.name += " (VISITED_CITY_CONTINENT_MESH)";
        prepareNakedPlanetClone();
        city3D.gameObject.name = originalName;

        yield return null;
        yield return null;
        yield return null;

        finalizeLandMassMeshes(enlargedPlanetClone.transform.Find("Land Mass"));

        GameObject cityGameObject = new GameObject("City: " + city3D.cityEcoreModel.qualifiedName);
        SceneHandler.INSTANCE.lastCityGenerator = cityGameObject.AddComponent<CityGenerator>();
        yield return SceneHandler.INSTANCE.StartCoroutine(SceneHandler.INSTANCE.lastCityGenerator.doStart(cityCenter, 0.1f));

        yield return null;
        yield return null;
        yield return null;

        findPositionForSpaceshipToLand();

        spaceShipPositionCurve = new CurveSegment(
                Vector3.zero,
                Vector3.zero + spaceshipTransform.forward * PlanetSize * 10f,
                spaceshipDestinationPoint - spaceshipDestinationDirection * PlanetSize * 10f,
                spaceshipDestinationPoint,
                CurveType.BEZIER);

        for (int i = 1; i <= 64; i++)
        {
            Debug.DrawLine(spaceShipPositionCurve.Evaluate((float)(i - 1) / 64), spaceShipPositionCurve.Evaluate((float)(i) / 64), Color.red, float.PositiveInfinity);
        }

        cityReflectionProbe = ReflectionProbeGenerator.GenerateReflectionProbe(cityGameObject.transform.parent, Vector3.up * 10f / PlanetSize, Vector3.one * PlanetSize * 10f, "City Reflection Probe", 512);
        SceneHandler.INSTANCE.SpaceshipOuterShellRenderer.probeAnchor = cityReflectionProbe.transform;

        yield return StartLandingAnimation(spaceshipTransform, PlanetSize);
    }



    private void findPositionForSpaceshipToLand()
    {
        //spaceshipDestinationDirection = (cityCenter.forward - cityCenter.right * 2f).normalized;
        spaceshipDestinationDirection = cityCenter.position;
        spaceshipDestinationDirection.y = 0;
        spaceshipDestinationDirection.Normalize();

        Vector3 spaceshipDestinationDirectionNormalLeft = Vector3.Cross(spaceshipDestinationDirection, Vector3.up);

        float checkSphereSize = 15f;

        spaceshipDestinationPoint = cityCenter.position;
        for (int i = 0; i < 500; i++)
        {
            Collider[] cols = Physics.OverlapSphere(spaceshipDestinationPoint, checkSphereSize, SceneHandler.INSTANCE.SpaceshipLandingPositionLayerMask);

            //Debug.Log("i=" + i + " | hits=" + cols.Length);
            //string colStr = "";
            //foreach (Collider col in cols)
            //    colStr += col.name + " | ";
            //Debug.Log(colStr);

            if (cols.Length == 0 && Physics.Raycast(spaceshipDestinationPoint + 8f * Vector3.back + Vector3.up, Vector3.down, 1.1f))
            {
                // SUCCESS
                // Does not collide buildings etc.
                // Player can exit spaceship: there is ground nearby

                //VisualDebugger.DrawPoint(spaceshipDestinationPoint + 2.5f * spaceshipDestinationDirection, Vector3.one * checkSphereSize, Color.green);
                spaceshipDestinationPoint += planetEndPosition - planetStartPosition;
                return;
            }

            //VisualDebugger.DrawPoint(spaceshipDestinationPoint, Vector3.one * checkSphereSize, Color.red);
            spaceshipDestinationPoint = cityCenter.position - i * spaceshipDestinationDirection * 0.5f + i * ((i % 2) * 2f - 1f) * spaceshipDestinationDirectionNormalLeft * 0.5f;
        }
    }

    private IEnumerator StartLandingAnimation(Transform spaceshipTransform, float PlanetSize)
    {
        float[] AnimationStageDurations = SceneHandler.INSTANCE.LandingAnimationStageDurations;
        float animationStartStamp = Time.time;
        while (true)
        {
            if (Time.time > animationStartStamp + AnimationStageDurations[0] + AnimationStageDurations[1] + AnimationStageDurations[2])
            {
                // Animation finished
                SceneHandler.INSTANCE.OnLandingProcedureFinished(enlargedPlanetClone);
                break;
            }

            else if (Time.time - animationStartStamp < AnimationStageDurations[0])
            // Move planet towards spaceship
            {
                float t01 = (Time.time - animationStartStamp) / AnimationStageDurations[0];

                //DirectionalSunLight.intensity = t01 * 1f;
                enlargedPlanetClone.transform.position = Vector3.Lerp(planetStartPosition, planetEndPosition, EasingFunctions.SmoothStop3(t01));
            }

            else if (Time.time - animationStartStamp <= AnimationStageDurations[0] + AnimationStageDurations[1])
            // Move spaceship along curve to target position
            {
                if (!initializedFlying)
                {
                    SceneHandler.INSTANCE.NotifyFlyingStarts();
                    cityReflectionProbe.RenderProbe();
                    initializedFlying = true;
                }

                enlargedPlanetClone.transform.position = planetEndPosition;
                float t01 = (Time.time - animationStartStamp - AnimationStageDurations[0]) / AnimationStageDurations[1];
                float eased_t01 = EasingFunctions.Crossfade(EasingFunctions.SmoothStart2, EasingFunctions.SmoothStop5, 0.5f, t01);
                spaceshipTransform.position = spaceShipPositionCurve.Evaluate(eased_t01);
                //Debug.Log(t01 + " -> " + eased_t01 + ": x=" + transform.position.x + " y=" + transform.position.y + " z=" + transform.position.z);

                Vector3 pointToFocus;
                float lookAhead = 0.1f;
                if (eased_t01 <= 1f - lookAhead)
                {
                    //Quaternion curveRotation = Quaternion.LookRotation(spaceShipPositionCurve.Evaluate(eased_t01 + lookAhead), cityDestinationUpwards);
                    //transform.rotation = Quaternion.Slerp(Quaternion.identity, curveRotation, eased_t01 / lookAhead);
                    pointToFocus = spaceShipPositionCurve.Evaluate(eased_t01 + lookAhead);
                }
                else
                {
                    //Quaternion finalRotation = Quaternion.LookRotation(cityDestinationDirection, cityDestinationUpwards);
                    //Quaternion lastCurveRotation = Quaternion.LookRotation(spaceShipPositionCurve.Evaluate(1f - lookAhead), cityDestinationUpwards);
                    //Debug.Log((eased_t01 - 1f + lookAhead) / lookAhead);
                    //transform.rotation = Quaternion.Slerp(lastCurveRotation, finalRotation, (eased_t01-1f+lookAhead) / lookAhead);
                    pointToFocus = (Vector3)spaceShipPositionCurve.Evaluate(1f) + PlanetSize * spaceshipDestinationDirection * (eased_t01 - 1f + lookAhead) / lookAhead;
                }

                Quaternion followPointRotation = Quaternion.LookRotation(pointToFocus - spaceshipTransform.position, Vector3.Lerp(Vector3.up, cityCenter.up, (eased_t01) / (1f - 2f * lookAhead)));
                spaceshipTransform.rotation = Quaternion.Slerp(Quaternion.identity, followPointRotation, eased_t01 / lookAhead);
                SceneHandler.INSTANCE.SpaceSkyBox.position = spaceshipTransform.position;
                athmosphere.UpdateShaders(true);
            }

            else if (Time.time - animationStartStamp <= AnimationStageDurations[0] + AnimationStageDurations[1] + AnimationStageDurations[2])
            // Open spaceship door
            {
                if (!finalizedFlying)
                {
                    SceneHandler.INSTANCE.NotifyFlyingEnds();
                    finalizedFlying = true;
                }
                spaceshipTransform.rotation = Quaternion.LookRotation(spaceshipDestinationDirection, cityCenter.up);
                float t01 = (Time.time - animationStartStamp - AnimationStageDurations[0] - AnimationStageDurations[1]) / AnimationStageDurations[2];
                float eased_t01 = EasingFunctions.Crossfade(EasingFunctions.SmoothStart2, EasingFunctions.SmoothStop3, 0.5f, t01);
                SceneHandler.INSTANCE.SpaceshipDoorTransform.localRotation = Quaternion.Euler(0f, 0f, -110f * eased_t01);

                SceneHandler.INSTANCE.SpaceshipDoorTeleportArea.locked = false;
            }

            // Wait 1 frame
            yield return null;
        }
    }

    private void prepareNakedPlanetClone()
    {
        enlargedPlanetClone = GameObject.Instantiate<GameObject>(SceneHandler.INSTANCE.VisitedCityOriginalData.ContainingPlanet3D.gameObject);

        removeComponentsFromTransformHierarchy(enlargedPlanetClone.transform, "Rigidbody", "Interactable");
        removeComponentsFromTransformHierarchy(enlargedPlanetClone.transform);
        deleteMarkedGameObjects(enlargedPlanetClone.transform);

        enlargedPlanetClone.layer = 0;
        enlargedPlanetClone.transform.parent = null;
        enlargedPlanetClone.transform.position = planetStartPosition;
        enlargedPlanetClone.transform.localScale = Vector3.one * SceneHandler.INSTANCE.PlanetSize;
        enlargedPlanetClone.transform.rotation = Quaternion.identity;
        enlargedPlanetClone.transform.rotation *= Quaternion.FromToRotation(cityConnectionHook.transform.up, Vector3.up);

        foreach (Transform childTransform in enlargedPlanetClone.transform)
        {
            if (childTransform.TryGetComponent<AthmosphereVisibilityHandler>(out AthmosphereVisibilityHandler athm))
            {
                athmosphere = athm;
            }
        }
    }

    private void removeComponentsFromTransformHierarchy(Transform t, params String[] toIgnore)
    {
        // This loop iterates over all components and tries to delete them
        // Works fine, but leaves ugly error prints in logs...
        //for (int i = 0; i < t.GetComponents<Component>().Length; i++)
        {
            foreach (Component comp in t.GetComponents<Component>())
            {
                if (!(comp is Transform))
                {
                    try
                    {
                        bool ignore = false;
                        foreach(String toIgnoreComponentName in toIgnore)
                        {
                            if (comp.GetType().Name.Equals(toIgnoreComponentName))
                            {
                                ignore = true;
                                break;
                            }
                        }

                        if(!ignore)
                            GameObject.Destroy(comp);
                    }
                    catch (Exception)
                    {
                        // Could not be deleted because other components rely on it.
                    }
                }
            }
        }
    }

    private void deleteMarkedGameObjects(Transform current)
    {
        foreach (Transform childTransform in current)
        {
            if (childTransform.gameObject.name.Contains("VISITED_CITY_CONTINENT_MESH"))
            {
                cityCenter = childTransform.Find("City Center");
                cityConnectionHook = childTransform.Find("City Connection Hook");
            }

            if (childTransform.gameObject.name.Contains("DeleteOnClone"))
                GameObject.Destroy(childTransform.gameObject);
            else
                deleteMarkedGameObjects(childTransform);
        }
    }

    private void finalizeLandMassMeshes(Transform current)
    {
        if (current == null)
            return;

        List<GameObject> teleportAreaPieces = new List<GameObject>();
        Material texturedLandMat = Resources.Load("SolarOverview/Planets/materials/PlanetLandTexturedMat") as Material;
        //Material texturedCliffMat = Resources.Load("SolarOverview/Planets/materials/PlanetLandCliffMat") as Material;

        foreach (Transform childTransform in current)
        {
            if (childTransform.GetComponent<MeshRenderer>() != null && childTransform.GetComponent<Collider>() != null)
            {
                GameObject teleportAreaPiece = GameObject.Instantiate(childTransform.gameObject);
                teleportAreaPieces.Add(teleportAreaPiece);

                teleportAreaPiece.GetComponent<MeshRenderer>().enabled = false;
                teleportAreaPiece.AddComponent<TeleportArea>();

                childTransform.GetComponent<Collider>().enabled = false;
                Material[] mats = new Material[] { texturedLandMat };
                childTransform.GetComponent<MeshRenderer>().materials = mats;
                //childTransform.GetComponent<MeshRenderer>().lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.CustomProvided;
            }
            else
            {
                finalizeLandMassMeshes(childTransform);
            }
        }

        foreach (GameObject teleportAreaPiece in teleportAreaPieces)
        {
            teleportAreaPiece.transform.parent = current;
            teleportAreaPiece.transform.localPosition = Vector3.zero;
            teleportAreaPiece.transform.localRotation = Quaternion.identity;
            teleportAreaPiece.transform.localScale = Vector3.one;
        }
    }
}
