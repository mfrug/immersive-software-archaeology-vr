using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class PlayerLocationTracker : MonoBehaviour
{

    public static PlayerLocationTracker INSTANCE;

    private Collider col;
    private List<PlayerLocationListener> listeners = new List<PlayerLocationListener>();

    public bool PlayerIsCurrentlyInSpaceship { get; private set; }



    void Start()
    {
        INSTANCE = this;
        col = GetComponent<Collider>();
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.Equals(SceneHandler.INSTANCE.VRHeadCollider))
        {
            PlayerIsCurrentlyInSpaceship = true;
            foreach(PlayerLocationListener listener in listeners)
                listener.OnPlayerEntersSpaceship();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.Equals(SceneHandler.INSTANCE.VRHeadCollider))
        {
            PlayerIsCurrentlyInSpaceship = false;
            foreach (PlayerLocationListener listener in listeners)
                listener.OnPlayerExitsSpaceship();
        }
    }



    public void AddListener(PlayerLocationListener newListener)
    {
        listeners.Add(newListener);
    }

    public void RemoveListener(PlayerLocationListener listener)
    {
        listeners.Remove(listener);
    }

}



public interface PlayerLocationListener
{

    public void OnPlayerEntersSpaceship();
    public void OnPlayerExitsSpaceship();

}
