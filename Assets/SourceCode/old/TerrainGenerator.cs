﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    public int subdivision = 1;
    public int resolution = 200;

    private int subEdgeNumber;
    private int subPlaneNumber;
    private int verticesPerEdge;

    public Vector3 terrainSize = new Vector3(500, 20, 500);
    public Vector3 terrainPosition = new Vector3(0, 0, 0);



    public Texture2D heightMapTexture;

    [Serializable]
    public struct TerrainNoise
    {
        public float scale;
        public float height;
        public float exponentialFalloff;
    }
    //public TerrainNoise[] additionalNoise = new TerrainNoise[] { new TerrainNoise { scale = 1.5f, height = 50f, exponentialFalloff = 1.5f } };



    // Start is called before the first frame update
    void Start()
    {
        subdivision = Mathf.Max(0, subdivision);
        resolution = Mathf.Max(16, resolution);

        subEdgeNumber = (int)Mathf.Pow(2, subdivision);
        subPlaneNumber = (int)Mathf.Pow(subEdgeNumber, 2);

        verticesPerEdge = (int)Mathf.Floor((float)resolution / ((float)subEdgeNumber));

        int maxResolution = Mathf.FloorToInt(Mathf.Sqrt((float)Math.Pow(2, 16)) - 1);
        if (verticesPerEdge > maxResolution)
        {
            Debug.LogWarning("Maximum Terrain Resolution per subdivision succeeded, changing resolution down from " + resolution + " to " + maxResolution * subEdgeNumber);
            resolution = maxResolution * subEdgeNumber;
            verticesPerEdge = (int)Mathf.Floor((float)resolution / ((float)subEdgeNumber));
        }

        Debug.Log("The terrain will be split into " + subPlaneNumber + " (" + subEdgeNumber + "x" + subEdgeNumber + ") areas.");
        Debug.Log("Vertices per subdivision edge: " + verticesPerEdge);

        GenerateMeshes();

        transform.position = terrainPosition - new Vector3(terrainSize.x / 2, 0, terrainSize.z / 2);
    }

    private void GenerateMeshes()
    {
        GetComponent<MeshFilter>().mesh = null;

        //meshes = new Mesh[(int)Mathf.Pow(subdivision + 1, 2)];

        for (int z = 0; z < subEdgeNumber; z++)
        {
            for(int x = 0; x < subEdgeNumber; x++)
            {
                GameObject childObject = new GameObject("Terrain Tile ("+x+", "+z+")");
                childObject.transform.parent = transform;

                Mesh mesh = new Mesh();
                mesh.Clear();
                childObject.AddComponent<MeshFilter>().mesh = mesh;
                childObject.AddComponent<MeshRenderer>().material = GetComponent<MeshRenderer>().material;

                int startX = x * verticesPerEdge;
                int startZ = z * verticesPerEdge;

                GenerateVertices(mesh, startX, startZ);
                GenerateTriangles(mesh);

                mesh.RecalculateNormals();
            }
        }
    }

    void GenerateVertices(Mesh mesh, int startX, int startZ)
    {
        Vector3[] vertices = new Vector3[(int)Mathf.Pow(verticesPerEdge + 1, 2)];
        Vector2[] uvs = new Vector2[vertices.Length];

        //float maxDistanceFromCenter = Mathf.Sqrt((((float)resolution / 2) * ((float)resolution / 2)) + (((float)resolution / 2) * ((float)resolution / 2)));

        int i = 0;
        for (int z = 0; z <= verticesPerEdge; z ++)
        {
            for (int x = 0; x <= verticesPerEdge; x ++)
            {
                float u = ((float)x+ (float)startX) / (float)resolution;
                float v = ((float)z + (float)startZ) / (float)resolution;
                uvs[i] = new Vector2(u, v);

                float vertX = u * terrainSize.x;
                float vertZ = v * terrainSize.z;

                Color heightMapColor = heightMapTexture.GetPixelBilinear(u, v);
                float vertY = heightMapColor.r * terrainSize.y;

                /*
                float distanceFromCenterX = Mathf.Abs((float)x - (float)resolution / 2);
                float distanceFromCenterZ = Mathf.Abs((float)z - (float)resolution / 2);
                float distanceFromCenter = Mathf.Sqrt((distanceFromCenterX * distanceFromCenterX) + (distanceFromCenterZ * distanceFromCenterZ));

                foreach (TerrainNoise noise in additionalNoise)
                {
                    vertY += noise.height * Mathf.PerlinNoise(vertX * noise.scale / 100, vertZ * noise.scale / 100);
                    vertY *= Mathf.Pow(distanceFromCenter / maxDistanceFromCenter, noise.exponentialFalloff);
                }
                */

                //Debug.Log(vertX + ", " + vertY + ", " + vertZ);
                vertices[i] = new Vector3(vertX, vertY, vertZ);
                i++;
            }
        }

        mesh.vertices = vertices;
        mesh.uv = uvs;
    }



    void GenerateTriangles(Mesh mesh)
    {
        int[] triangles = new int[verticesPerEdge * verticesPerEdge * 6];

        int vertexIndex = 0;
        int triangleIndex = 0;

        for (int z = 0; z < verticesPerEdge; z++)
        {
            for (int x = 0; x < verticesPerEdge; x++)
            {
                triangles[triangleIndex + 0] = vertexIndex + 0;
                triangles[triangleIndex + 1] = vertexIndex + verticesPerEdge + 1;
                triangles[triangleIndex + 2] = vertexIndex + 1;
                triangles[triangleIndex + 3] = vertexIndex + 1;
                triangles[triangleIndex + 4] = vertexIndex + verticesPerEdge + 1;
                triangles[triangleIndex + 5] = vertexIndex + verticesPerEdge + 2;

                vertexIndex += 1;
                triangleIndex += 6;
            }

            vertexIndex += 1;
        }

        mesh.triangles = triangles;
    }
}
