# **Immersive Software Archaeology Virtual Reality**

The ISA VR application allows to inspect a subject software system in virtual reality.
In order for that to work, you need to install the [ISA Eclipse plugins](https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-eclipse), which serve the VR application with information on a software system's design and architecure.

The ISA VR application is built with **Unity 3D** and **SteamVR**.

**Important remark**: This project was developed and tested on a Windows machine!



![ISA VR Screenshots](readme-resources/screenshots.jpg?raw=true)



&nbsp;



## **Publications**

- **[FSE'21 Vision Paper](https://pure.itu.dk/ws/portalfiles/portal/86190732/fse21_21_07_07_camera_ready_authors_version.pdf)** - short paper on our vision for the project
- **[VISSOFT'22 Technical Paper](https://pure.itu.dk/ws/portalfiles/portal/92854114/preprint_vissoft22.pdf)** - paper on the solar system metaphor, highlighting concepts and methodology



&nbsp;



## **Installation**

There are **two alternative ways** of installing the ISA VR application on your machine.  
These are explained beneath.

#### Option A: Installation via a **Binary Build** (Windows only)

This way of installing the ISA Eclipse plugins is the fastest.  
Simply download and extract the ZIP-folder from the [release section](https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-vr/-/releases) of this repository and start the provided executable.

#### Option B: Installation via **Source Code**

Check out this repository and open it as Unity project using the specified Unity version.  
We recommend using the [Unity Hub](https://unity3d.com/get-unity/download).



&nbsp;



## **Usage**

The ISA VR application acts as visualization front-end in combination with a running Eclipse runtime hosting the [ISA Eclipse plugins](https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-eclipse).
The ISA VR application communicated with the ISA Eclipse plugins, thereby fetches information on a priorly analyzed subject software system, and visualizes these in immersive VR.

[![VISSOFT'22 Tool Demo](readme-resources/youtube-preview.jpg?raw=true)](https://www.youtube.com/watch?v=wmayYcpL7ZY "VISSOFT'22 Tool Demo")


#### **SteamVR**

The ISA VR application is developed on top of [SteamVR](https://store.steampowered.com/app/250820/SteamVR/).
This principally enables to use all headset currently supported by SteamVR.  
We use a Valve Index headset for testing our implementation.

Please note that you can change the binding of controller buttons to VR interaction via the SteamVR controller binding menu.


#### **Inspecting Source Code**

It is possible to inspect source code in VR.
In order for that to work, please make sure that the workspace of your back-end Eclipse runtime contains respective Java projects.  
**Please note**: the names of the Eclipse projects is important! These *must* match the names of the projects as they were set during the analysis of the system.



&nbsp;



## **Contact**

In case you have questions, please don't hesitate to contact me: [adho@itu.dk](adho@itu.dk).



&nbsp;
